# Command-line tools for FSL data sets


FSL includes some command-line tools for working with the FSL atlases and standard templates.


## `atlasq`


The `atlasq` tool allows command-line interrogation of the atlas images installed into `$FSLDIR/data/atlases`. `atlasq` can  be used to query the values contained in an atlas at specific coordinates, or within a mask. `atlasq` can also list all installed atlases, and to print information about a specified atlas.


### Query an atlas


The `atlasq query` sub-command allows you to interrogate region probabilities and labels from a FSL atlas with coordinates, or with a mask image.  The command-line interface is as follows:


```
$ atlasq query -h

usage: atlasq query <atlas> [options] -m mask  [-m mask ...]
usage: atlasq query <atlas> [options] -c X Y Z [-c X Y Z...]
usage: atlasq query <atlas> [options] -v X Y Z [-v X Y Z...]
usage: atlasq query <atlas> [options] -v X Y Z [-c X Y Z [-m mask]...]

positional arguments:
  atlas                 Name or ID of atlas to summarise.

options:
  -h, --help            show this help message and exit
  -r RESOLUTION, --resolution RESOLUTION
                        Desired atlas resolution (mm). Default is highest available resolution.
  -s, --short           Output in short (machine-friendly) format.
  -l, --label           Query label/maxprob version of atlas (for probabilistic atlases).
  -m MASK, --mask MASK  Mask to query with.
  -c X Y Z, --coord X Y Z
                        World coordinates to look up.
  -v X Y Z, --voxel X Y Z
                        Voxel coordinates to look up. Must be in terms of the atlas at the specified (or default) --resolution.
```


You can specify multiple sets of coordinates or mask images in a single call, for example:


```
$ atlasq query harvardoxford-cortical -c 38 -44 48 -c -52 -13 48
---------------------------------
| coordinate 38.00 -44.00 48.00 |
---------------------------------

name                                    | index | summary value | proportion
--------------------------------------- | ----- | ------------- | ----------
Superior Parietal Lobule                | 17    | 18            | 45.0000
Angular Gyrus                           | 20    | 21            | 12.0000
Supramarginal Gyrus, posterior division | 19    | 20            | 12.0000
Postcentral Gyrus                       | 16    | 17            | 2.0000

----------------------------------
| coordinate -52.00 -13.00 48.00 |
----------------------------------

name              | index | summary value | proportion
----------------- | ----- | ------------- | ----------
Postcentral Gyrus | 16    | 17            | 43.0000
Precentral Gyrus  | 6     | 7             | 41.0000
```


When you provide a mask image, `atlasq` will report the proportions of all regions within the mask:

```
$ atlasq query harvardoxford-cortical -m ./mask.nii.gz
----------------------
| mask ./mask.nii.gz |
----------------------

name                                      | index | summary value | proportion
----------------------------------------- | ----- | ------------- | ----------
Middle Frontal Gyrus                      | 3     | 4             | 29.4126
Inferior Frontal Gyrus, pars opercularis  | 5     | 6             | 14.0991
Inferior Frontal Gyrus, pars triangularis | 4     | 5             | 8.6138
Frontal Pole                              | 0     | 1             | 8.4548
Precentral Gyrus                          | 6     | 7             | 1.1291
```


The `--short`/`-s` option instructs `atlasq` to output its results in a more condensed, machine-friendly format - results for each coordinate/mask image are printed on one line, with each section separated by a tab character, (indicated with `\t` below)::

```
$ atlasq query harvardoxford-cortical -s -c 38 -44 48 -c -52 -13 48
coordinate\t38.00 -44.00 48.00\tSuperior Parietal Lobule 45.0000\tSupramarginal Gyrus, posterior division 12.0000\tAngular Gyrus 12.0000\tPostcentral Gyrus 2.0000
coordinate\t-52.00 -13.00 48.00\tPostcentral Gyrus 43.0000\tPrecentral Gyrus 41.0000
```

You can also interrogate an atlas with voxel coordinates, although when doing so you should also use the `-r` option to specify which resolution image you are querying:

```
$ atlasq query harvardoxford-cortical -s -r 2 -v 44 45 60
voxel\t44 45 60\tCingulate Gyrus, posterior division 37.0000\tPrecuneous Cortex 29.0000\tPrecentral Gyrus 2.0000\tPostcentral Gyrus 2.0000
```


### List all atlases


To list all available atlases, run `atlasq list`. This will print a table containing the names and IDs of all atlases:


```
$ atlasq list
ID                                | Full name
--------------------------------- | ---------------------------------------------------------------
cerebellum_mniflirt               | Cerebellar Atlas in MNI152 space after normalization with FLIRT
cerebellum_mnifnirt               | Cerebellar Atlas in MNI152 space after normalization with FNIRT
harvardoxford-cortical            | Harvard-Oxford Cortical Structural Atlas
harvardoxford-subcortical         | Harvard-Oxford Subcortical Structural Atlas
smatt                             | Human Sensorimotor Tracts Labels
jhu-labels                        | JHU ICBM-DTI-81 White-Matter Labels
jhu-tracts                        | JHU White-Matter Tractography Atlas
juelich                           | Juelich Histological Atlas
marsparietalparcellation          | Mars Parietal connectivity-based parcellation
marstpjparcellation               | Mars TPJ connectivity-based parcellation
mni                               | MNI Structural Atlas
neubertventralfrontalparcellation | Neubert Ventral Frontal connectivity-based parcellation
thalamus                          | Oxford Thalamic Connectivity Probability Atlas
striatum-connectivity-3sub        | Oxford-Imanova Striatal Connectivity Atlas 3 sub-regions
striatum-connectivity-7sub        | Oxford-Imanova Striatal Connectivity Atlas 7 sub-regions
striatum-structural               | Oxford-Imanova Striatal Structural Atlas
salletdorsalfrontalparcellation   | Sallet Dorsal Frontal connectivity-based parcellation
stn                               | Subthalamic Nucleus Atlas
talairach                         | Talairach Daemon Labels
xtract                            | XTRACT HCP Probabilistic Tract Atlases
xtract_mac                        | XTRACT Macaque Probabilistic Tract Atlases
```


You can also run `atlasq list --extended`, which will print additional informatiohn about each atlas.


### Describe one atlas


The `atlasq summary` command will print out information about one atlas. You can call it with the full name of an atlas name, or with an atlas ID:


```
$ atlasq summary  mni
MNI Structural Atlas [mni]
  Spec:          ${FSLDIR}/data/atlases/MNI.xml
  Type:          probabilistic
  Labels:        9
  Image:         ${FSLDIR}/data/atlases/MNI/MNI-prob-2mm
  Image:         ${FSLDIR}/data/atlases/MNI/MNI-prob-1mm
  Summary image: ${FSLDIR}/data/atlases/MNI/MNI-maxprob-thr25-2mm
  Summary image: ${FSLDIR}/data/atlases/MNI/MNI-maxprob-thr25-1mm
Index | Label          | X     | Y     | Z
----- | -------------- | ----- | ----- | -----
0     | Caudate        | 12.0  | 20.0  | 0.0
1     | Cerebellum     | 28.0  | -58.0 | -44.0
2     | Frontal Lobe   | 0.0   | 42.0  | -10.0
3     | Insula         | 40.0  | 16.0  | -8.0
4     | Occipital Lobe | -22.0 | -88.0 | -10.0
5     | Parietal Lobe  | 38.0  | -56.0 | 48.0
6     | Putamen        | -22.0 | 12.0  | -4.0
7     | Temporal Lobe  | 56.0  | -4.0  | -12.0
8     | Thalamus       | 10.0  | -14.0 | 6.0
```


## `atlasquery`

The `atlasquery` tool is a less flexible alternative to `atlasq`, also designed to allow command-line interrogation of the atlas images supplied with FSL. It takes as input the name of one of the FSL atlases together with either a coordinate of interest or a mask. The syntax is:

```

usage: atlasquery -h
       atlasquery --dumpatlases
       atlasquery -a atlas -c X,Y,Z
       atlasquery -a atlas -m mask

options:
  -h, --help              show this help message and exit
  -a ATLAS, --atlas ATLAS Name of atlas to use
  -V, --verbose           Switch on diagnostic messages
  -m MASK, --mask MASK    A mask image to use during structural lookups
  -c COORD, --coord COORD Coordinate to query
  --dumpatlases           Dump a list of the available atlases
```

Note that the name of the atlas being specified must be exactly as specified in the list created by `--dumpatlases` and requires double quotes to enable the spaces to be included in the name (see the examples below).

### Outputs

The output from atlasquery is text, sometimes with a small amount of formatting in html suitable for a browser. It gives the (average) probability of a voxel or mask being a member of the different labelled regions within the atlas. If a mask is used then the probability is averaged over all the voxels in the mask. All the probability numbers are scaled from 0 to 100, so that they are easily interpreted as percentages (e.g., 49.5 represents a probability of 0.495).

The probability values will not add up to 100 if the coordinate or mask is outside, or on the edge of, the ROIs featured in the atlas.


With a coordinate (in mm convention; i.e. MNI coordinates):

```
$ atlasquery -a "Harvard-Oxford Subcortical Structural Atlas" -c -22,10,0
<b>Harvard-Oxford Subcortical Structural Atlas</b><br>99% Left Putamen
```

With a mask (in MNI space):

```
$ atlasquery -a "Harvard-Oxford Subcortical Structural Atlas" -m mask.nii.gz
Right Cerebral White Matter:3.5
Right Cerebral Cortex :0.5
Right Putamen:49.5
Right Pallidum:46
```

## `fsl_get_standard`


The `fsl_get_standard` tool can be used to locate standard template images.

```
usage: fsl_get_standard [options] [image_type] [modality]

Generate paths to FSL standard space images

positional arguments:
  image_type             Image type
  modality               Desired modality

options:
  -h, --help             show this help message and exit
  -V, --version          Print version and exit.
  -r {2,1,0.5}
  --resolution {2,1,0.5}
                         Desired isotropic resolution in millimetres
```

The default standard template used by FSL tools is the MNI152 template:

```
$ fsl_get_standard
/usr/local/fsl/data/standard/MNI152_T1_2mm.nii.gz
$ fsl_get_standard whole_head
/usr/local/fsl/data/standard/MNI152_T1_2mm.nii.gz
$ fsl_get_standard brain_mask
/usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz
$ fsl_get_standard brain -r 1
/usr/local/fsl/data/standard/MNI152_T1_1mm_brain.nii.gz
```

If you have a different set of standard template images that you wish to use instead (e.g. installed in `${FSLDIR}/data/mouse/`), you can set the `${FSL_STANDARD}` environment variable, e.g.:


```
$ export FSL_STANDARD=mouse
$ fsl_get_standard brain
/usr/local/fsl/data/mouse/mouse_brain_T1_1mm.nii.gz
$ fsl_get_standard whole_head    -r 1
/usr/local/fsl/data/mouse/mouse_whole_head_T1_1mm.nii.gz
$ fsl_get_standard whole_head T2 -r 1
/usr/local/fsl/data/mouse/mouse_whole_head_T2_1mm.nii.gz
```

For more details, and for information on creating a standard template data set for use by FSL, refer to the [`fsl_get_standard` git repository](https://git.fmrib.ox.ac.uk/fsl/get_standard).
