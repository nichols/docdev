# FSLeyes

[![PyPi version](https://img.shields.io/pypi/v/fsleyes.svg)](https://pypi.org/project/fsleyes/)
[![conda-forge version](https://anaconda.org/conda-forge/fsleyes/badges/version.svg)](https://anaconda.org/conda-forge/fsleyes)
[![FSLeyes DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1470761.svg)](https://doi.org/10.5281/zenodo.1470761)


FSLeyes (pronounced *fossilise*) is the FSL image viewer, part of FSL since FSL 5.0.10.


Follow FSLeyes on [Twitter](https://twitter.com/FSLeyes) to be notified of new releases.

The FSLeyes) user guide can be found [here](https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/userdoc/), and the release history can be viewed [here](https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/userdoc/changelog.html).

FSLeyes can either be installed as part of FSL, installed into a conda environment, or installed into a Python environment (for advanced users).


## Install as part of FSL (recommended)


!> The terminal commands in these instructions may require administrative privileges, depending on where you have installed FSL. If you encounter permission errors, try repeating the command, but prefixing it with `sudo`.

FSLeyes comes bundled with all versions of FSL from 5.0.10 onwards. So if you have FSL, you already have FSLeyes. The FSLeyes version which comes bundled with FSL may be slightly out of date, but it is straightforward to update using `conda`.

If you have an older version of FSL, it is recommended to update to the latest available version - you can do this by downloading and running the `fslinstaller.py` script, available on the [installation](install/index.md) page.


### FSL 6.0.7

If you have FSL 6.0.7 or newer, you can update to the latest version of FSLeyes by running the following command:

```bash
update_fsl_package -u fsleyes
```


### FSL 6.0.6 or newer

!> Sometimes it is difficult to update packages within an existing conda environment, due to compatibility problems with packages that are already installed. If you have trouble updating FSLeyes, please get in touch via the [FSl mailing list](http://www.jiscmail.ac.uk/lists/fsl.html) or [twitter](https://twitter.com/FSLeyes).


If you have FSL 6.0.6 or newer, you can update to the latest version by running the following command:

```bash
${FSLDIR}/bin/conda update -p ${FSLDIR} fsleyes
```


### FSL 6.0.2 and newer

From FSL 6.0.2 to 6.0.5.2, FSLeyes is installed into the embedded `${FSLDIR}/fslpython` conda environment, which is a part of the FSL installation. You can update to the latest version of FSLeyes by running the following command:

```bash
${FSLDIR}/fslpython/bin/conda update -n fslpython -c conda-forge --update-all fsleyes
```


> On macOS, if you experience segmentation faults after updating FSLeyes, try running: `${FSLDIR}/fslpython/bin/conda install -n fslpython -c conda-forge libffi=3.3`


### FSL 6.0.1 and older


Versions of FSL prior to 6.0.2 come with a standalone version of FSLeyes. Before updating, you should remove the old standalone version of FSLeyes. If you are using macOS:


```bash
rm ${FSLDIR}/bin/fsleyes
rm -r ${FSLDIR}/bin/FSLeyes.app
```


Or, if you are using Linux:


```bash
rm ${FSLDIR}/bin/fsleyes
rm -r ${FSLDIR}/bin/FSLeyes
```

Now you can install FSLeyes with the following command:


```bash
${FSLDIR}/fslpython/bin/conda install -n fslpython -c conda-forge --update-all fsleyes
```

When you want to update FSLeyes again in the future, use this command instead:

```bash
${FSLDIR}/fslpython/bin/conda update -n fslpython -c conda-forge --update-all fsleyes
```


## Install from conda-forge (recommended)

If you want to install FSLeyes independently of FSL, `conda` is the recommended method.  FSLeyes is available on [conda-forge](https://conda-forge.org/) - if you use an [anaconda](https://www.anaconda.com/) or [miniconda](https://docs.conda.io/en/latest/miniconda.html) environment, you can install FSLeyes into it like so:

```bash
conda install -c conda-forge fsleyes
```


Or you can create a separate conda environment, specifically for FSLeyes, like so:

```bash
conda create -c conda-forge -p path/to/fsleyes/environment fsleyes
```


> If you are using Linux and are using a X11 (as opposed to Wayland) desktop environment, you may need to install an older version of wxPython, for example:
>
> ```conda create -c conda-forge -p path/to/fsleyes/environment fsleyes "wxpython=4.0.*"```

## Install from PyPi (advanced)

!> This is an advanced option, recommended only if you are comfortable working with Python environments, and installing packages using your OS package manager. The commands below are __suggestions__ - you will probably need to adapt them to suit your OS and environment.


FSLeyes is available on [PyPi](https://pypi.org/project/fsleyes/) and should work with Python 3.7 and newer. The best way to install FSLeyes from PyPi is to create an isolated python environment with a [virtual environment](https://docs.python.org/3/library/venv.html), and install FSLeyes into it. To get started:


```bash
python -m venv fsleyes-virtualenv
. fsleyes-virtualenv/bin/activate
```


### macOS users

Once you have activated your virtual environment, you should be able to install FSLeyes like so:

```
pip install fsleyes
```


### Linux users


Before installing FSLeyes, you first need to install wxPython. The easiest way to do this on Linux is to use the pre-release wxPython builds available at https://extras.wxpython.org/wxPython4/extras/linux/. For example, if you are using CentOS 7:


```bash
pip install -f https://extras.wxpython.org/wxPython4/extras/linux/gtk2/centos-7 wxpython
pip install fsleyes
```

You will also need to install the wxPython runtime dependencies. Under CentOS 7, you will need to run the following command:


```bash
sudo yum install freeglut SDL
```

Similarly, under Ubuntu 20.04:

```bash
sudo apt-get install freeglut3 libsdl1.2debian
```


Another option is to install wxPython directly from PyPi - if you do this, you will need to have C/C++ compilers installed, and all of the dependencies required to compile wxPython. Under CentOS 7, run the following commands:

```bash
sudo yum groupinstall "Development tools"
sudo yum install gtk2-devel gtk3-devel webkitgtk-devel webkitgtk3-devel
sudo yum install libjpeg-turbo-devel libtiff-devel SDL-devel gstreamer-plugins-base-devel libnotify-devel freeglut-devel
```

Under Ubuntu 20.04, run the following:

```bash
sudo apt-get install build-essential
sudo apt-get install libgtk2.0-dev libgtk-3-dev libwebkitgtk-dev libwebkitgtk-3.0-dev
sudo apt-get install libjpeg-turbo8-dev libtiff5-dev libsdl1.2-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libnotify-dev freeglut3-dev
```

Then you should be able to run:

```bash
pip install fsleyes
```

## Help and troubleshooting


User documentation for FSLeyes can be found [here](https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/userdoc/).

If you are having trouble getting FSLeyes to run, a solution may be documented on the [troubleshooting](https://open.win.ox.ac.uk/pages/fsl/fsleyes//fsleyes/userdoc/troubleshooting.html) page.

You may also find a solution to your problem in the [FSL mailing list archives](https://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=fsl), or on [Neurostars](https://neurostars.org/).

To ask for help, try the [FSL mailing list](https://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=fsl), [Neurostars](https://neurostars.org/), or [Twitter](https://twitter.com/FSLeyes).

If you have found a bug in FSLeyes, you can also open an issue on [github](https://github.com/pauldmccarthy/fsleyes). If you would like to fix a bug or add a new feature, you are also welcome to open a pull request through [github](https://github.com/pauldmccarthy/fsleyes) (although it is probably best to open an issue first to discuss the proposal).

## Development

The source code for FSLeyes and its main dependencies is hosted at:

- https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes
- https://git.fmrib.ox.ac.uk/fsl/fsleyes/widgets
- https://git.fmrib.ox.ac.uk/fsl/fsleyes/props
- https://git.fmrib.ox.ac.uk/fsl/fslpy


Mirrors for these projects are also on github - feel free to use the issue tracker, and open a PR if you have something to contribute:

- https://github.com/pauldmccarthy/fsleyes/
- https://github.com/pauldmccarthy/fsleyes-widgets/
- https://github.com/pauldmccarthy/fsleyes-props/
- https://github.com/pauldmccarthy/fslpy/

API/developer documentation can be found here:

- https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/apidoc/
- https://open.win.ox.ac.uk/pages/fsl/fsleyes/widgets/
- https://open.win.ox.ac.uk/pages/fsl/fsleyes/props/
- https://open.win.ox.ac.uk/pages/fsl/fsleyes/fslpy/


## Citation

If you would like to cite FSLeyes or fslpy in your research, refer to their respective [Zenodo](https://zenodo.org/) pages:

- [FSLeyes](https://doi.org/10.5281/zenodo.1470761)
- [fslpy](https://doi.org/10.5281/zenodo.1470750)
