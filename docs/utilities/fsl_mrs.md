# FSL MRS

## FSL-MRS - end-to-end Magnetic Resonance Spectroscopy Toolbox

FSL-MRS is a suite of tools for MR Spectroscopy, including single voxel (SVS), MRS imaging (MRSI), functional MRS (fMRS), diffusion MRS (dwMRS), edited spectroscopy, etc.

This page only briefly describes the toolbox. Please refer to the [FSL-MRS](https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/) page for access to the full documentation and code.

The toolbox includes tools for:

- Data conversion to NIfTI-MRS from multiple vendor-specific formats
- Pre-processing, including coil-combination, eddy-current correction, phase alignment, etc.
- Density matrix basis simulation
- Spectral fitting
- Quantification
- Visualisation

![FSL MRS pipeline](./fsl_mrs_pipeline.gif)

## Citation

If you use FSL-MRS in your research, please cite the main paper below:

>William T Clarke, Charlotte J Stagg, Saad Jbabdi. FSL-MRS: An end-to-end spectroscopy analysis package. Magnetic Resonance in Medicine, 85(6):2950-2964, 2021 ([Clarke 2021](https://pubmed.ncbi.nlm.nih.gov/33280161))