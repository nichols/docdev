# Cluster

The tool cluster can be used to form clusters, report information about clusters and/or perform cluster-based inference.

Cluster formation can be done using *any* type of image as it uses a supplied threshold, even though the default reporting and input options mention z-stats. Optionally, inference (turning cluster sizes into clusterwise p-values) is done using Gaussian Random Field (GRF) theory and requires smoothness estimates from `smoothest`.


!> Cluster name change\
\
In FSL 6.0.6, `cluster` was renamed to `fsl-cluster` to avoid conflict with the [Graphviz](https://graphviz.org/docs/cli/cluster/) `cluster` command. A wrapper script is available at `$FSLDIR/share/fsl/bin/cluster`, so when working at the command-line you should be able to continue using the `cluster` command (as long as the `$FSLDIR/share/fsl/bin/` directory is at the front of your `$PATH` - the `fslinstaller.py` script will update your shell profile accordingly for a typical installation).

However, when calling cluster from a script, the safest option is to call `$FSLDIR/bin/fsl-cluster`, or `$FSLDIR/share/fsl/bin/cluster` directly, to avoid any ambiguity.

The usage for `cluster` is:

```
cluster --in=<filename> --thresh=<value> [options]

Compulsory arguments (You MUST set one or more of):
        -i,--in,-z,--zstat      filename of input volume
        -t,--thresh,--zthresh   threshold for input volume

Optional arguments (You may optionally specify one or more of):
        -o,--oindex      filename for output of cluster index (in size order)
        --othresh        filename for output of thresholded image
        --olmax filename for output of local maxima text file
        --olmaxim        filename for output of local maxima volume
        --osize filename for output of size image
        --omax  filename for output of max image
        --omean filename for output of mean image
        --opvals         filename for image output of log pvals
        -p,--pthresh     p-threshold for clusters
        --peakdist       minimum distance between local maxima/minima, in mm (default 0)
        -c,--cope        filename of input cope volume
        --volume         number of voxels in the mask
        -d,--dlh         smoothness estimate = sqrt(det(Lambda))
        --fractional     interprets the threshold as a fraction of the robust range
        --connectivity   the connectivity of voxels (default 26)
        --mm             use mm, not voxel, coordinates
        --min            find minima instead of maxima
        --no_table       suppresses printing of the table info
        --minclustersize prints out minimum significant cluster size
        -x,--xfm         filename for Linear: input->standard-space transform. Non-linear: input->highres transform
        --stdvol         filename for standard-space volume
        --scalarname     give name of scalars (e.g. Z) - to be used in printing output tables
        -n,--num         no of local maxima to report
        -v,--verbose     switch on diagnostic messages
        -h,--help        display this message
        --warpvol        filename for warpfield
```

## Outputs

The main output from cluster is the table reporting the different clusters, their size, and information about their location and contents (e.g. maximum/mean values within the cluster). This table is written as plain text and is output to the screen, but can be redirected into a file using the standard unix method of using `> filename` after the command (which puts all the output that would have been written to the screen into the specified file instead) - see examples below.

The standard output table contains the following columns:

    Cluster Index   Voxels  MAX     MAX X (vox)     MAX Y (vox)     MAX Z (vox)     COG X (vox)     COG Y (vox)     COG Z (vox)

- **Cluster Index**: a unique number for each cluster from 1 to N (larger clusters have bigger numbers)
- **Voxels**: the number of voxels in the cluster
- **MAX**: the value of the maximum "intensity" within the cluster (e.g. the maximum z-statistic if the input was a z-statistic image)
- **MAX X/Y/Z (vox/mm)**: the location of the maximum intensity voxel, given as X/Y/Z coordinate values in either voxel coordinates (vox) or standard space coordinates (mm) - the latter requires either the input image to already be in standard space or a transformation to standard space to be specified via the options `-x`,`--xfm` or `--warpvol`
- **COG X/Y/Z (vox/mm)**: the location of the Centre Of Gravity for the cluster (that is, a weighted average of the coordinates by the intensities within the cluster, equivalent to the concept of Centre Of Gravity for a object with distributed mass, where intensity substitutes for mass in this case)

Note that the text formatting of the columns can make it difficult to see which header is attached to which column of numbers, so be careful to count the columns correctly.

If a COPE image is added (`-c`,`--cope`) then extra columns are added to the table, reporting the maximum COPE value, the location of this maximum and the mean COPE value over the *clusters defined from the input image* (i.e., clusters are not defined using the COPE image).

If GRF options are added (`-d`,`--dlh` and `--volume` and `-p`,`--pthresh`) then the table also reports the probability (p-value and -log10(p)) associated with each cluster.

Some particularly useful **image outputs** are:

- **cluster index** (`--oindex` or `-o`): this is an image where every cluster is assigned a unique number (from 1 to N) and the intensity of every voxel in a cluster is set to this unique number. For example, every voxel in cluster number 3 will have an intensity of 3. This is very useful for separating/extracting clusters - see the examples below.

- **size index** (`--osize`): every voxel in a cluster has its intensity set equal to the size of that cluster (where size is the number of voxels). This is useful for thresholding clusters based on size.

## Examples

The examples below represent a small set of possibilities, but include the most commonly used scenarios. Note that most of the commands below redirect the text output into a file, but the text output can be displayed directly on the screen by deleting everything from the `>` character and onwards.

### Defining Clusters

    cluster -i myinputimage -t 2.5 -o cluster_index --osize=cluster_size > cluster_info.txt
This takes any type of input values (contained in the image file `myinputimage`) and thresholds them at 2.5 (a totally arbitrary example value here). The output images specified here are two of the most useful outputs, as described above, although other outputs can also be requested.

### Extracting a Particular Cluster

Assuming that a cluster index file has been created (e.g., as above) then any particular cluster can be extracted using `fslmaths`. For example, to extract a mask that contained only cluster number 7 we would do:

    fslmaths -dt int cluster_index -thr 7 -uthr 7 -bin cluster_mask7

### Reporting Cluster Information

**A) Z-stat (without GRF statistics):**

    cluster -i zstat1 -t 2.3 > cluster_zstat1.txt
> Note: the threshold value of 2.3 is an arbitrary example only.

**B) Corrected 1-p values (as output from `randomise`):**

    cluster -i blah_blah_corrp_tstat1 -t 0.95 --scalarname="1-p" > cluster_corrp1.txt
> Note: the threshold is based on the 1-p value (i.e. 0.95 = 1 - 0.05); the use of `--scalarname` means that all the table headings report "1-p" rather than "Z".

**C) Corrected 1-p values with associated t-statistics (as output from `randomise`):**

    cluster -i blah_blah_corrp_tstat1 -t 0.95 -c blah_blah_tstat1 --scalarname="1-p" > cluster_corrp1.txt
> Note: the columns labelled `COPE` refer to the associated t-statistics within the respective clusters (where the clusters are still defined by the 1-p values in the input image, together with the threshold).

**D) Z-stat with GRF statistics:**

    cluster -i zstat1 -t 2.3 -p 0.05 -d 1.65786 --volume=19537 > cluster_zstat1.txt