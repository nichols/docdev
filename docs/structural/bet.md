# BET

## Contents

- [Research overview](#research-overview)
- [Citation](#citation)
- [User guide](#user-guide)
- [FAQ](#faq)

## Research overview

BET (Brain Extraction Tool) deletes non-brain tissue from an image of the whole head. It can also estimate the inner and outer skull surfaces, and outer scalp surface, if you have good quality T1 and T2 input images.

<div class="niivue-canvas" thumbnail="structural/bet_thumbnail.png">
<vol url="structural/bet_T1.nii.gz"/>
<vol url="structural/bet_T1_brain.nii.gz" colormap="red" opacity="0.5"/>
</div>


## Citation

If you use BET, please make sure to cite the following references in your publications:

- [S.M. Smith. Fast robust automated brain extraction. Human Brain Mapping, 17(3):143-155, November 2002](https://pubmed.ncbi.nlm.nih.gov/12391568/)

If you use the skull and scalp surface extraction functions, please also cite the following reference:

- M. Jenkinson, M. Pechaud, and S. Smith. BET2: MR-based estimation of brain, skull and scalp surfaces. In Eleventh Annual Meeting of the Organization for Human Brain Mapping, 2005

## User guide

### BET programs

- `bet` - the main command-line script which makes calling the core bet2 and betsurf programs easy
  - `bet2` - the main brain extraction program
  - `betsurf` - using a registered T1 and T2 image pair, estimates inner and outer skull surfaces and outer scalp surface
- `Bet` - the BET GUI (`bet_gui` on macOS)

Changes: The old C program called bet has now been discontinued; the `bet2` C++ program gives almost identical results. `betall` has been replaced by the `-A` and `-A2` options in the new bet script. `betfunc` has been replaced by the `-F` option in `bet`. `betpremask` has been replaced by the `-b` option in `standard_space_roi`. `betsmallz` has been replaced by the `-Z` option in `bet`.

### `bet` and `bet2`

By default `bet` just calls the `bet2` program for simple brain extraction. However, it also allows various additional behaviour. The basic usage is:

```bash
bet <input> <output> [options]
```

#### Main `bet2` options

- `-o` generate brain surface outline overlaid onto original image
- `-m` generate binary brain mask
- `-s` generate rough skull image (not as clean as what betsurf generates)
- `n` do not generate the default brain image output
- `f <number>` fractional intensity threshold (0..1); default=0.5; smaller values give larger brain outline estimates
- `-g <number>` vertical gradient in fractional intensity threshold (-1..1); default=0; positive values give larger brain outline at bottom, smaller at top
- `r <number>` head radius (mm not voxels); initial surface sphere is set to half of this
- `-c <x y z>` centre-of-gravity (voxels not mm) of initial mesh surface
- `-t` apply thresholding to segmented brain image and mask
- `-e` generates brain surface as mesh in .vtk format

#### Variations on default bet2 functionality (mutually exclusive options)

- `-R` This option runs more "robust" brain centre estimation; it repeatedly calls bet2, each time using the same input image and the same main options, except that the -c option (which sets the starting centre of the brain estimation) is set each time to the centre-of-gravity of the previously estimated brain extraction. The primary purpose is to improve the brain extraction when the input data contains a lot of non-brain matter - most likely when there is a lot of neck included in the input image. By iterating in this way the centre-of-gravity should move up each time towards the true centre, resulting in a better final estimate. The iterations stop when the centre-of-gravity stops moving, up to a maximum of 10 iterations.
- `-S` This attempts to cleanup residual eye and optic nerve voxels which bet2 can sometimes leave behind. This can be useful when running SIENA or SIENAX, for example. Various stages involving standard-space masking, morphpological operations and thresholdings are combined to produce a result which can often give better results than just running bet2.
- `-B` This attempts to reduce image bias, and residual neck voxels. This can be useful when running SIENA or SIENAX, for example. Various stages involving FAST segmentation-based bias field removal and standard-space masking are combined to produce a result which can often give better results than just running bet2.
- `-Z` This can improve the brain extraction if only a few slices are present in the data (i.e., a small field of view in the Z direction). This is achieved by padding the end slices in both directions, copying the end slices several times, running bet2 and then removing the added slices.
- `-F` This option uses bet2 to determine a brain mask on the basis of the first volume in a 4D data set, and applies this to the whole data set. This is principally intended for use on FMRI data, for example to remove eyeballs. Because it is normally important (in this application) that masking be liberal (ie that there be little risk of cutting out valid brain voxels) the -f threshold is reduced to 0.3, and also the brain mask is "dilated" slightly before being used.
- `-A` This runs both bet2 and betsurf programs in order to get the additional skull and scalp surfaces created by betsurf. This involves registering to standard space in order to allow betsurf to find the standard space masks it needs.
- `-A2 <T2w image>` This is the same as -A except that a T2 image is also input, to further improve the estimated skull and scalp surfaces. As well as carrying out the standard space registration this also registers the T2 to the T1 input image.

The old script betpremask has been replaced with the -b option in standard_space_roi. This uses flirt to register the input image to a standard space whole-head image; the resulting transform is then inverted and a standard-space brain mask is transformed into the space of the input image, and then applied to this to create the output.

## Bet GUI

### Main options

First set the Input image filename and the Output image filename. The output name is also used to generate the filenames for any optional outputs that are turned on in Advanced options.

Changing Fractional intensity threshold from its default value of 0.5 will cause the overall segmented brain to become larger (<0.5) or smaller (>0.5). This threshold must lie between 0 and 1.

If you change the options menu from the default setting of Run standard brain extraction using bet2 then you will get one of the Variations on default bet2 functionality described above.

![BET GUI](./bet_gui.png)

### Advanced options

By default the only output from BET is an image with all non-brain matter removed - this is the Output brain-extracted image option.

The Output binary brain mask image option tells BET to output a binary brain mask (0 outside of the brain and 1 inside). This can then be used in later processing, to mask other images derived from the original.

The Apply thresholding to brain and mask image option causes BET to apply thresholding to the segmented brain image (and also the brain mask if selected). Thus, inside the estimated brain, some voxels can be "zeroed" after segmentation, if their intensity falls below an automatically estimated threshold.

The Output exterior skull surface image option tells BET to produce an image which is an estimate of the exterior surface of the skull. All non-skull-surface points are 0 in this image, and skull-surface points are 100.

The Output brain surface overlaid onto original image option creates an output image which is a copy of the original image, with additionally an outline of the brain's surface drawn on top.

Changing Threshold gradient from its default value of 0 causes a gradient to be apply to the previous threshold. Thus setting a positive value here causes the primary threshold to be reduced at the bottom of the brain, giving a larger brain estimate there, and a smaller estimate of the brain towards the top of the image. This value must lie between -1 and 1.

## betsurf

`betsurf` is the program that produces the three additional surfaces (inner & outer skull, outer scalp). It can output these surfaces as filled-in binary mask images, surface-only binary mask images and mesh format files.

`betsurf` requires ideally good resolution T1- and T2-weighted input images. It can work only from just a T1 image, but the results will generally not be as good. The images should ideally be better than 2x2x2mm - the smaller the voxel size the better. `betsurf` requires that bet2 has already been run on the T1 image, with the -e mesh output option for the brain surface. It also requires that the T2 image already be aligned (e.g., using `flirt`) to the T1. Finally, it requires that you supply a transformation from the T1 to standard space (e.g., generated by `flirt`) as it uses standard-space masks in order to constraint some of the mesh fitting.

The .vtk mesh files contain mm co-ordinates. If the image has radiological ordering ( see `fslorient` ) then the mm co-ordinates are the voxel co-ordinates scaled by the mm voxel sizes. If the image has neurological ordering the y and z co-ordinates are the same as for radiological ordering, but the x co-ordinates will be inverted ( x_mm = ( n - 1 - x ) * x_dim ) where n is the number of voxels in the x-direction, x is the voxel co-ordinate and x_dim is the voxel size in mm.

As an example of how to use `bet2` and `betsurf` together, see the `-A` and `-A2` options in the bet script. The `-A2` option:

Registers the T2 to the T1 image. Registers the T1 image to standard space (not to transform the image but just to get the transform matrix itself). Runs `bet2` to get the initial brain extraction from the T1 image. Runs `betsurf` to get the other head surfaces.

## FAQ

### BET does not give me good results - what can I do?

Firstly, check that the input image is OK. This can be done using slices as described in the first part of the [flirt checklist](structural/flirt/faq.md).

If the image contains a lot of neck (large superior-inferior FOV), then the default centre used by BET is often poor. This can be fixed by using the `-c `option to select good centre coordinates. Note that these coordinates are in voxels, not mm, so if you are using voxel coordinates (starting at 0 in the corner) then you must multiply these coordinates by the voxel dimensions - as the (0,0,0)mm point is in the corner, not in the middle of the image.

If all of the above appears to be OK, then the main parameters to alter in BET are the `-f` and `-g` parameters which can cause general expansion or contraction of the brain segmentation. Note that very local errors are unlikely to be fixed by varying these parameters. For more information about these parameters see: What do the `-f` and `-g` options in BET do?

### What do the -f and -g options in BET do?

The `-f` option in BET is used to set a fractional intensity threshold which determines where the edge of the final segmented brain is located. The default value is 0.5 and the valid range is 0 to 1. A smaller value for this threshold will cause the segmented brain to be larger and should be used when the overall result from BET is too small (inside the brain boundary). Obviously, larger values for this threshold have the opposite effect (making the segmented brain smaller). This parameter does not normally need to be used, but sometimes requires tuning for specific scanners/sequences to get the best results. It is not advisable to tune it for each individual image in general.

The `-g` option in BET causes a gradient change to be applied to the previous threshold value. That is, the value of the `-f` intensity threshold will vary from the top to the bottom of the image, centred around the value specified with the `-f` option. The default value for this gradient option is 0, and the valid range is -1 to +1. A positive value will cause the intensity threshold to be smaller at the bottom of the image and larger at the top of the image. This will have the effect of increasing the estimated brain size in the bottom slices and reducing it in the top slices.

### Can I use BET with animal brains?

From FSL 6.0.6.6 onwards, the `bet4animal` script is available and tuned for the animals: macaque, marmoset, night monkey, rat and mouse. It is necessary 1) to reorient the input image so that AC-PC line is parallel to the y axis (e.g., using `fslreorient2std`), 2) find the co-ordinates of the centre using `fsleyes` to set the voxel co-ordinate values via `-c` option, and (3) set the species (1. macaque, 2. marmoset, 3. night monkey, 4. rat, 5. mouse) via `-z` option OR set scaling values in x, y, z direction via `-x` option so that brain is scaled in each direction to simulate the size and shape of human brain before bet. You may also want to use `-w` option with a value larger than 1 to obtain the smoothed brain surface like in lissencephalic brain like rat and mouse.
