# VERBENA

<img title="VERBENA example image"
     src="structural/verbena_image.jpg"
     align="right" width="30%"/>

Vascular Model Based Perfusion Quantification for DSC-MRI (VERBENA) is a Bayesian Inference tool for quantification of perfusion and other haemodynamic parameters from Dynamic Susceptibility Contrast perfusion MRI of the brain. `VERBENA` complements the `BASIL` tools for the quantification of perfusion using Arterial Spin Labelling MRI and is built on the same core inference algorithm (`FABBER`). `VERBENA` uses a specific physiological model for capillary transit of contrast within the blood generally termed the 'vascular model' that was first described by [Ostergaard et al. (1999)](https://doi.org/10.1097/00004647-199906000-00013). In `VERBENA` the model has been extended to explicitly infer the mean transit time and also to optionally include correction for macro vascular contamination - contrast agent within arterial vessels - more information on the model can be found in the theory section.

`VERBENA` takes a model-based approach to the analysis of DSC-MRI data in contrast to alternative 'non-parametric' approaches, that often use a Singular Value based Deconvolution to quantify perfusion. An alternative Bayesian Deconvolution approach is also available, but not currently distributed as part of FSL. For more information see the reference below and contact the senior author.

`VERBENA` is scheduled for a future release of FSL (it is not to be found in the current release). However, if you are interested in using `VERBENA`, it is possible to provide a pre-release copy that is compatible with more recent FSL releases.


## Usage

For the full usage of `VERBENA` type `verbena` at the command line. A typical usage of `VERBENA` would be:

	verbena -i data.nii.gz -a aif.nii.gz -o output_directory -m mask.nii.gz

This would process the 4D DSC data in `data.nii.gz` using the AIFs supplied in `aif.nii.gz` using the modified Vascular Model to estimate (relative) perfusion, commonly referred to as cerebral blood flow (rCBF), along with the mean transit time (MTT) and the transit time distribution parameter lambda; maps of which are placed in the output directory. Analysis is only performed within the mask supplied (`mask.nii.gz`) which will normally have been derived from a brain extraction using `BET` or other equivalent tool.

### AIFs

`VERBENA` takes as an input a 4D Nifti file containing the Arterial Input Functions (AIFs) this should have identical dimensions to the data and thus should have a single AIF time course for every single voxel (within the mask), often this will be a single global AIF replicated for every single voxel. However, `VERBENA` allows for different AIFs to be specified for individual brain regions should a local AIF be available. We do not currently include a tool for the selection or identification of the AIF. Often the AIF time course will be manually selected from the DSC data by the identification of a major artery, various automated methods have been developed in the literature and it may be possible to find tools that implement them online.

### Macro vascular contamination

By adding the `--mv` option an additional component will be added to the model (based on the AIF) to account for macro vascular contamination contrast in large arteries, see Theory. When this option is included a further image will be produced in the output directory that maps the Arterial Blood Volume (rABV) in relative units. By default the additional macro vascular component is added when the concentration time course of the voxel is calculated, optionally addition of the tissue and macro vascular component can be done as signal time courses using the `--sigadd` option.

### 'Model-Free' Analysis

`VERBENA` takes a model-based approach to perfusion quantification. It is possible to use a more conventional Singular Value Decomposition deconvolution method by choosing the `--modelfree` option. This 'model-free' quantification can also be used to create initial estimates for the main model-based `VERBENA` analysis using the `--modelfreeinit`option, which may lead to more robust results in some cases.

---

## Theory

### The Vascular Model

The Vascular Model was originally proposed by Ostergaard et al. and was used for the analysis of DSC data (within a Bayesian like algorithm) by Mouridsen et al. in 2006. The basic principle follows all tracer kinetic studies and treats the concentration of contrast agent in the tissue as the convolution of an arterial input function (AIF) and a residue function. The AIF describes the delivery of agent to the tissue region by in the blood, the residue function describes what happens to it once it has arrived - for example how long a unit of contrast agent remains before it is removed to the venous vasculature. 

In the context of DSC-MRI the convolution model is applied to each voxel in turn and the residue function represents the residence of the agent within the tissue volume described by the voxel. In the healthy brain the Gadolinium tracer that is used in DSC-MRI does not leave the vasculature and thus the residue function encapsulates the transit of the contrast agent through the capillary bed. In fact the residue function is the integral of the distribution of transit times for blood passing through the voxel - a key parameter of which is the mean transit time (MTT), which is routinely used in DSC perfusion as a surrogate measure of perfusion (although it is often calculated without finding the transit distribution itself). 

The Vascular Model assumes that the transit time distribution can be modelled as series of parallel pathways of differing lengths that can be summered by a gamma distribution of transit times. This can be converted to the equivalent residue function by integration. Once this has been convolved with the residue function the concentration time curve in the tissue can be calculated. In practice DSC measures the effect that this concentration of contrast agent has on the T2* of the voxel which is described by a non-linear transformation. In `VERBENA` it is this final estimated signal that is compared to the data and used to find the optimal parameters using a Bayesian inference algorithm. Additionally the potential for a time delay between the supplied AIF (often measured at a remote location from the tissue) and the tissue signal is included in the model.

### The Modified Vascular Model

`VERBENA` implements a modified version of the Vascular Model whereby the MTT is not pre-calcualted from the data, but instead is a further parameter to be estimated as part of the inference applied to the data, see Chappell et al.. This removes the risk of bias from the separate MTT calculation and also allows for a separate macro vascular component to be implemented within the model.

### Macro Vascular Contamination

`VERBENA` has the option to include a macro vascular component to the model. This combines the estimated concentration time curve from the (modified) vascular model with a scaled version of the AIF, where the AIF is representative of contrast that is still within the large arteries during imaging and the scaling is a (relative) measure of arterial blood volume. The component is subject to a 'shrinkage prior' that aims to provide a conservative estimate - so that this component is only included in voxels where the data supports its inclusion, recognising that macro vascular contamination will be be universally present within the brain, but only occur in voxels that contain large arteries. The combination of tissue and macro vascular contributions could be done in terms of the concentrations of contrast in the voxel. However, since in DSC it is the T2* effect of the concentration that is measured, the summation might be better done with the signals once their effect on T2* has been accounted for. `VERBENA` offers the option to do either, there is currently no clear evidence as to which is most physically accurate and it is likely that both are an incomplete representation of the reality, see Chappell et al..

---

## References

If you use `VERBENA` in your research, please make sure that you reference the first article listed below.


> Chappell, M.A., Mehndiratta, A., Calamante F., *Correcting for large vessel contamination in DSC perfusion MRI by extension to a physiological model of the vasculature*, [Magn Reson Med., 2015 74(1):280-290](https://doi.org/10.1002/mrm.25390).

The following articles provide more background on the original vascular model from which the `VERBENA` model is derived:

> Mouridsen K, Friston K, Hjort N, Gyldensted L, Østergaard L, Kiebel S. *Bayesian estimation of cerebral perfusion using a physiological model of microvasculature*. [NeuroImage 2006;33:570–579](https://doi.org/10.1016/j.neuroimage.2006.06.015).

> Ostergaard L, Chesler D, Weisskoff R, Sorensen A, Rosen B. *Modeling Cerebral Blood Flow and Flow Heterogeneity From Magnetic Resonance Residue Data*. [J Cereb Blood Flow Metab 1999;19:690–699](https://doi.org/10.1097/00004647-199906000-00013).

An alternative Bayesian 'non-parametric' deconvolution approach has been published in:

> Mehndiratta A, MacIntosh BJ, Crane DE, Payne SJ, Chappell MA. A control point interpolation method for the non-parametric quantification of cerebral haemodynamics from dynamic susceptibility contrast MRI. [NeuroImage 2013;64:560–570](https://doi.org/10.1016/j.neuroimage.2012.08.083).