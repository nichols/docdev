# fsl_anat

## Anatomical Processing Script: fsl_anat

This tool provides a general pipeline for processing anatomical images (e.g. T1-weighted scans).

Most of the pipeline involves standard use of FSL tools, but the bias-field correction has been substantially improved, especially for strong bias-fields typical of multi-coil arrays and high-field scanners.

The stages in the pipeline (in order) are:

- reorient the images to the standard (MNI) orientation using `fslreorient2std`
- automatically crop the image using `robustfov`
- bias-field correction (RF/B1-inhomogeneity-correction) using `fast`
- registration to standard space (linear and non-linear) using `flirt` and `fnirt`
- brain-extraction using `fnirt` (via registration) or `bet`
- tissue-type segmentation using `fast`
- subcortical structure segmentation using `first`

The overall run-time is heavily dependent on the resolution of the image but anything between 30 and 90 minutes would be typical.

## Usage
Various stages in the pipeline can be turned off. Currently not all combinations have been tested and so users are advised to only turn off the first few or last couple of stages currently.

```bash
Usage: fsl_anat [options] -i <structural image>
       fsl_anat [options] -d <existing anat directory>
 
Arguments (You may specify one or more of):
  -i <strucural image>         filename of input image (for one image only)
  -d <anat dir>                directory name for existing .anat directory where this script will be run in place
  -o <output directory>        basename of directory for output (default is input image basename followed by .anat)
  --clobber                    if .anat directory exist (as specified by -o or default from -i) then delete it and make a new one
  --strongbias                 used for images with very strong bias fields
  --weakbias                   used for images with smoother, more typical, bias fields (default setting)
  --noreorient                 turn off step that does reorientation 2 standard (fslreorient2std)
  --nocrop                     turn off step that does automated cropping (robustfov)
  --nobias                     turn off steps that do bias field correction (via FAST)
  --noreg                      turn off steps that do registration to standard (FLIRT and FNIRT)
  --nononlinreg                turn off step that does non-linear registration (FNIRT)
  --noseg                      turn off step that does tissue-type segmentation (FAST)
  --nosubcortseg               turn off step that does sub-cortical segmentation (FIRST)
  -s <value>                   specify the value for bias field smoothing (the -l option in FAST)
  -t <type>                    specify the type of image (choose one of T1 T2 PD - default is T1)
  --nosearch                   specify that linear registration uses the -nosearch option (FLIRT)
  --betfparam                  specify f parameter for BET (only used if not running non-linear reg and also wanting brain extraction done)
  --nocleanup                  do not remove intermediate files
```

By default:

- the bias-field correction assumes that the field is "strong", typical of that arising from a multi-coil array or a high-field scanner. For images acquired using birdcage coils or on 1.5T scanners, the `--weakbias` option will be faster and may produce equally good results.
- the brain extraction is based on transforming a standard-space mask to the input image using the FNIRT (non-linear) registration, and does not use the BET tool for this (and consequently the `--betfparam` setting does not change the brain extraction in this FNIRT-based mode of operation)

Using the `-d` option the script can be run again (with a subset of stages) to update an existing result.

## Outputs

This section describes the main output files - it is not a complete list, but highlights the most important outputs.

### Directory

The output directory will end with `.anat` and by default will have the same basename as the input image (and be in the same directory). If the `-o` option is used the directory name will use the specified name, followed by `.anat`

### Original image

The specified input image is copied into the output directory and named `T1`, `T2` or `PD`, depending on the setting of the `-t` option (default is T1).

### Reorientation and Cropping

If run, the original image (we shall call it `T1` from here on in as an example), will be replaced by the reoriented and/or cropped version. The original versions are saved as files: `T1_orig` and `T1_fullfov`.

In addition, transformation files are provided to allow images to be moved between spaces: i.e. `T1_orig2std.mat` and `T1_nonroi2roi.mat` and their inverses and combinations.

### Bias-correction

The bias-corrected version of the image is called `T1_biascorr`.

### Registration and Brain-Extraction

The registration (to standard space) produces the following images that are in MNI space with a 2mm resolution:

- `T1_to_MNI_lin` (linear registration output)
- `T1_to_MNI_nonlin` (non-linear registration output)
- `T1_to_MNI_nonlin_field` (non-linear warp field)
- `T1_to_MNI_nonlin_jac` (Jacobian of the non-linear warp field)
- `T1_vols.txt` - a file containing a scaling factor and brain volumes, based on skull-contrained registration, suitable for head-size normalisation (as the scaling is based on the skull size, not the brain size)

The brain-extraction produces:

- `T1_biascorr_brain`
- `T1_biascorr_brain_mask`

### Segmentation

Tissue-type segmentation (done with FAST) produces:

- `T1_biascorr` - refined again in this stage
- `T1_fast_pve_0`, `T1_fast_pve_1`, `T1_fast_pve_2` - partial volume segmentations (CSF, GM, WM respectively)
- `T1_fast_pveseg` - a summary image showing the tissue with the greatest partial volume fraction per voxel

Subcortical segmentation (done with FIRST) produces:

- `T1_subcort_seg` - summary image of all subcortical segmentations
- all other outputs in the first_results subdirectory
- `T1_first_all_fast_firstseg` - same as `T1_subcort_seg`
- a host of other images relating to individual segmentations
- `T1_biascorr_to_std_sub.mat` (in the main anat directory) - a transformation matrix of the subcortical optimised MNI registration

