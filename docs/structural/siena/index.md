# SIENA

## Research Overview

SIENA is a package for both single-time-point ("cross-sectional") and two-time-point ("longitudinal") analysis of brain change, in particular, the estimation of atrophy (volumetric loss of brain tissue). SIENA has been used in many clinical studies.

Siena estimates percentage brain volume change (PBVC) betweem two input images, taken of the same subject, at different points in time. It calls a series of FSL programs to strip the non-brain tissue from the two images, register the two brains (under the constraint that the skulls are used to hold the scaling constant during the registration) and analyse the brain change between the two time points. It is also possible to project the voxelwise atrophy measures into standard space in a way that allows for multi-subject voxelwise statistical testing. An extension for ventricular analysis is provided in FSL5.

Sienax estimates total brain tissue volume, from a single image, normalised for skull size. It calls a series of FSL programs: It first strips non-brain tissue, and then uses the brain and skull images to estimate the scaling between the subject's image and standard space. It then runs tissue segmentation to estimate the volume of brain tissue, and multiplies this by the estimated scaling factor, to reduce head-size-related variability between subjects.

- A paper on SIENA has been published in [JCAT](http://www.fmrib.ox.ac.uk/analysis/fmribindex/fmribindex.html#Smith01c); also see a related technical report ([PDF](http://www2.fmrib.ox.ac.uk/analysis/research/siena/siena.pdf)).
-A second paper, on SIENAX and improvements to SIENA has been published in [NeuroImage](http://www.fmrib.ox.ac.uk/analysis/fmribindex/fmribindex.html#Smith02); also see a related technical report ([PDF](http://www2.fmrib.ox.ac.uk/analysis/research/siena/siena2.pdf)).
- We have recently extended SIENA ("SIENAr") to allow voxelwise statistical analysis of atrophy across subjects; see [ISMRM04](http://www.fmrib.ox.ac.uk/analysis/fmribindex/fmribindex.html#DeStefano03b) and [HBM04](http://www.fmrib.ox.ac.uk/analysis/fmribindex/fmribindex.html#Bartsch04).

If you use SIENA in your research, please make sure that you reference the following articles. You may alternatively wish to use the brief descriptive methods text and expanded list of references given below.

## Referencing SIENA/X (minimal version)

Two-timepoint percentage brain volume change was estimated with SIENA [Smith 2002], part of FSL [Smith 2004].

or
Brain tissue volume, normalised for subject head size, was estimated with SIENAX [Smith 2002], part of FSL [Smith 2004].

>[Smith 2002] S.M. Smith, Y. Zhang, M. Jenkinson, J. Chen, P.M. Matthews, A. Federico, and N. De Stefano. Accurate, robust and automated longitudinal and cross-sectional brain change analysis. NeuroImage, 17(1):479-489, 2002.

>[Smith 2004] S.M. Smith, M. Jenkinson, M.W. Woolrich, C.F. Beckmann, T.E.J. Behrens, H. Johansen-Berg, P.R. Bannister, M. De Luca, I. Drobnjak, D.E. Flitney, R. Niazy, J. Saunders, J. Vickers, Y. Zhang, N. De Stefano, J.M. Brady, and P.M. Matthews. Advances in functional and structural MR image analysis and implementation as FSL. NeuroImage, 23(S1):208-219, 2004.

## Referencing SIENA/X (more detailed text and references)

**SIENA**. Two-timepoint percentage brain volume change was estimated with SIENA [Smith 2001, Smith 2002], part of FSL [Smith 2004]. SIENA starts by extracting brain and skull images from the two-timepoint whole-head input data [Smith 2002b]. The two brain images are then aligned to each other [Jenkinson 2001, Jenkinson 2002] (using the skull images to constrain the registration scaling); both brain images are resampled into the space halfway between the two. Next, tissue-type segmentation is carried out [Zhang 2001] in order to find brain/non-brain edge points, and then perpendicular edge displacement (between the two timepoints) is estimated at these edge points. Finally, the mean edge displacement is converted into a (global) estimate of percentage brain volume change between the two timepoints.

**SIENAX**. Brain tissue volume, normalised for subject head size, was estimated with SIENAX [Smith 2001, Smith 2002], part of FSL [Smith 2004]. SIENAX starts by extracting brain and skull images from the single whole-head input data [Smith 2002b]. The brain image is then affine-registered to MNI152 space [Jenkinson 2001, Jenkinson 2002] (using the skull image to determine the registration scaling); this is primarily in order to obtain the volumetric scaling factor, to be used as a normalisation for head size. Next, tissue-type segmentation with partial volume estimation is carried out [Zhang 2001] in order to calculate total volume of brain tissue (including separate estimates of volumes of grey matter, white matter, peripheral grey matter and ventricular CSF).

**Voxelwise multi-subject SIENA statistics**. First, SIENA was run separately for each subject. Next, for each subject, the edge displacement image (encoding, at brain/non-brain edge points, the outwards or inwards edge change between the two timepoints) was dilated, transformed into MNI152 space, and masked by a standard MNI152-space brain edge image. In this way the edge displacement values were warped onto the standard brain edge [Bartsch 2004]. Next, the resulting images from all subjects were fed into voxelwise statistical analysis to test for.....

>[Smith 2001] S.M. Smith, N. De Stefano, M. Jenkinson, and P.M. Matthews. Normalised accurate measurement of longitudinal brain change. Journal of Computer Assisted Tomography, 25(3):466-475, May/June 2001.

>[Smith 2002] S.M. Smith, Y. Zhang, M. Jenkinson, J. Chen, P.M. Matthews, A. Federico, and N. De Stefano. Accurate, robust and automated longitudinal and cross-sectional brain change analysis. NeuroImage, 17(1):479-489, 2002.

>[Smith 2004] S.M. Smith, M. Jenkinson, M.W. Woolrich, C.F. Beckmann, T.E.J. Behrens, H. Johansen-Berg, P.R. Bannister, M. De Luca, I. Drobnjak, D.E. Flitney, R. Niazy, J. Saunders, J. Vickers, Y. Zhang, N. De Stefano, J.M. Brady, and P.M. Matthews. Advances in functional and structural MR image analysis and implementation as FSL. NeuroImage, 23(S1):208-219, 2004.

>[Smith 2002b] S.M. Smith. Fast robust automated brain extraction. Human Brain Mapping, 17(3):143-155, November 2002.

>[Jenkinson 2001] M. Jenkinson and S.M. Smith. A global optimisation method for robust affine registration of brain images. Medical Image Analysis, 5(2):143-156, June 2001.

>[Jenkinson 2002] M. Jenkinson, P.R. Bannister, J.M. Brady, and S.M. Smith. Improved optimisation for the robust and accurate linear registration and motion correction of brain images. NeuroImage, 17(2):825-841, 2002.

>[Zhang 2001] Y. Zhang, M. Brady, and S. Smith. Segmentation of brain MR images through a hidden Markov random field model and the expectation maximization algorithm. IEEE Trans. on Medical Imaging, 20(1):45-57, 2001.

>[Bartsch 2004] A.J. Bartsch, N. Bendszus, N. De Stefano, G. Homola, and S. Smith. Extending SIENA for a multi-subject statistical analysis of sample-specific cerebral edge shifts: Substantiation of early brain regeneration through abstinence from alcoholism. In Tenth Int. Conf. on Functional Mapping of the Human Brain, 2004.