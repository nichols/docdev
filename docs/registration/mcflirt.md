# MCFLIRT

MCFLIRT is an intra-modal motion correction tool designed for use on fMRI time series and based on optimization and registration techniques used in [FLIRT](registration/flirt), a fully automated robust and accurate tool for linear (affine) inter- and inter-modal brain image registration.

If you use MCFLIRT in your research, please quote the journal reference:

> Jenkinson, M., Bannister, P., Brady, J. M. and Smith, S. M. Improved Optimisation for the Robust and Accurate Linear Registration and Motion Correction of Brain Images. NeuroImage, 17(2), 825-841, 2002.

## How it works

MCFLIRT loads the time-series in its entirity and will default to the middle volume as an initial template image. A coarse 8mm search for the motion parameters is then carried out using the cost function specified followed by two subsequent searches at 4mm using increasingly tighter tolerances. All optimizations use trilinear interpolation.

As part of the initial 8mm search, an identity transformation is assumed between the middle volume and the adjacent volume. The transformation found in this first search is then used as the estimate for the transformation between the middle volume and the volume beyond the adjacent one. This scheme should lead to much faster optimization and greater accuracy for the majority of studies where subject motion is minimal. In the pathalogical cases, this scheme does not penalise the quality of the final correction.

If mean registration is used, the curent motion correction parameters are applied to the time-series, the volumes are averaged to create a new template image and the same 3-stage correction is carried out using this new mean image as a template.

Finally, if a 4-stage correction has been specified, a further optimization pass is carried out using sinc interpolation (internally) for greater accuracy. This step is significantly more time-consuming than the previous part of the correction, which should take in the order of 10 minutes for a 100 volume timeseries. This internal interpolation is independent of the final resampling interpolation (as specified by `sinc_final` or `spline_final`).

The scheme has the added advantage of `sensible' handling of the end slices in a volume. In other schemes, voxels which move out of the Field of View (FOV) due to (often slight) movement of the head are either excluded from further calculations or treated as zero-value. In order to retain as much useful information in the data as possible, the volumes are padded by doubling the first and last slices in the z-plane so that we can interpolate from locations outside the FOV using appropriate values.


## Usage

The `mcflirt` command-line tool is used as follows:

    mcflirt -in <infile> [options]

Run `mcflirt` without any arguments for details on all of the options.


## Output file formats

When run with the `-mats` option, MCFLIRT saves a FLIRT-compatible transformation matrix for every 3D volume in the input image.

When run with the `-plots` option, MCFLIRT saves a `.par` file, which is a plain-text file that contains the estimated rotations and translations for each 3D volume. The first three columns contain the rotations for the X, Y, and Z voxel axes, in radians. The remaining three columns contain the estimated X, Y, and Z translations. These translation parameters are defined in the scaled mm coordinate system used by FLIRT, and have been rotated by the rotation parameters with respect to the centre-of-mass of the reference image volume (which can be calculated with the `fslstats -C` option, multiplying the results by the image pixdims).

## General Advice

- For rapid registration, high-quality corrections can be performed using only 3-stage optimization with the mean template
