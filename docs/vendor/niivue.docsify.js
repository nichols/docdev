/*
 * Docsify plugin for embedding Niivue canvases in Markdown. To use,
 * add  the following to your markdown file:
 *
 *   <div class="niivue-canvas" thumbnail="thumbnail.png">
 *     <vol url="image1.nii.gz"/>
 *     <vol url="image2.nii.gz" colormap="red" opacity="0.5"/>
 *   </div>
 *
 * The <vol> elements will be replaced with a <canvas> element, and Niivue
 * will be used to render the specified NIFTI images to the canvas,
 */
function install(hook) {

  hook.doneEach(_ => {

    // Load niivue for every <div> with class "niivue-canvas"
    let elems = Array.from(document.querySelectorAll(
      ".markdown-section div.niivue-canvas"));

    for (var i = 0; i < elems.length; i++) {

      var canvasId = "gl-" + i;
      var elem     = elems[i];

      // Turn <vol> elements into niivue
      // volume object specifications
      var volElems = Array.from(elem.querySelectorAll('vol'));
      var volSpecs = volElems.map(function(volElem) {
        var volSpec = {};
        volElem.getAttributeNames().forEach(function(attr) {
          // supported attributes are:
          // colormap(string), opacity(Number)
          // for attributes that should be numbers, make sure we force that
          if (attr == "opacity") {
            volSpec[attr] = Number(volElem.getAttribute(attr));
          } else {
            volSpec[attr] = volElem.getAttribute(attr);
          }
        });
        return volSpec;
      });

      // load them into niivue, and
      // add a <canvas> to the div
      var nv = new niivue.Niivue({
        // disable thumbnail for now until aspect ratio issue is fixed
        // see: https://github.com/niivue/niivue/issues/723
        // thumbnail   : elem.getAttribute("thumbnail"),
        loadingText : 'loading...'
      });

      var canvas = document.createElement("canvas");

      canvas.setAttribute("id",     canvasId);
      canvas.setAttribute("height", "300");
      canvas.setAttribute("width",  "640");

      elem.replaceChildren(canvas);
      nv.attachTo(canvasId);
      nv.loadVolumes(volSpecs);
    }
  });
}

$docsify.plugins = [].concat(install, $docsify.plugins);
