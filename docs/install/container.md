# Install FSL into a Docker/Singularity container


FSL can easily be installed into a Docker or Singularity container. If you would like to have a complete FSL installation, follow the standard instructions for a [Linux installation](install/linux.md). Here is an example `Dockerfile` which installs FSL into an Ubuntu 20.04 image:


```
FROM ubuntu:20.04

ENV FSLDIR          "/usr/local/fsl"
ENV DEBIAN_FRONTEND "noninteractive"
ENV LANG            "en_GB.UTF-8"

RUN apt update  -y && \
    apt upgrade -y && \
    apt install -y    \
      python          \
      wget            \
      file            \
      dc              \
      mesa-utils      \
      pulseaudio      \
      libquadmath0    \
      libgtk2.0-0     \
      firefox         \
      libgomp1

RUN wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fslinstaller.py && \
    python ./fslinstaller.py -d /usr/local/fsl/

ENTRYPOINT [ "sh", "-c", ". /usr/local/fsl/etc/fslconf/fsl.sh && /bin/bash" ]
```

If you only need specific FSL commands, you can use [conda](https://conda.io/) to install only the FSL components that you need - [see here](install/conda.md) for more details.
