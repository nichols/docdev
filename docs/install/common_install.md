## Install FSL


1. Use a web browser to download the `fslinstaller.py` script from
   [here](https://fsl.fmrib.ox.ac.uk/fsldownloads) - download it to your
   `Downloads` directory.

2. Open a terminal.
   - On Linux, you should be able to find a `Terminal` application in your system/application menu.
   - On macOS, you can find the `Terminal` app via Finder, by navigating to  *Applications > Utilities*.
   - On Windows, open the WSL distribution, (e.g. the *Ubuntu* app, if you chose to install Ubuntu).

3. Run the `fslinstaller.py` script in the Terminal:
   - On Linux and macOS, the `fslinstaller.py` script will probably have been saved to your *Downloads* directory:
         python ~/Downloads/fslinstaller.py
   - On Windows, the `fslinstaller.py` script will probably have been saved to the *Downloads* directory **in your Windows file system** - replace `<WindowsUserName>` with your Windows user account name:
         python "/mnt/c/Users/<WindowsUserName>/Downloads/fslinstaller.py"


!> Some systems do not have a command called `python`, and will print errors like `python: command not found`. If this happens try calling `python3 fslinstaller.py` or `python2 fslinstaller.py`.\
\
Some versions of macOS do not have Python installed by default - if you do not have any Python command available, you can download an installer from https://www.python.org. Alternately, Python will be installed as part of Xcode, which you can install by running `sudo xcode-select --install`. \
\
If you experience SSL certificate errors on macOS, try running the installer with `/usr/bin/python fslinstaller.py` or `/usr/bin/python3 fslinstaller.py`. If you have installed Python separately from https://www.python.org, make sure you have run the *Install certificates* script, as described [here](https://bugs.python.org/issue43404#msg388144).


The `fslinstaller.py` script will print out a number of messages, reporting its progress, and any errors that may arise. **Please read the ``fslinstaller.py`` output carefully** - if something goes wrong, save a copy of the output, as it will be useful in diagnosing the problem.


Be sure to [verify your installation](install/verify_install.md) to make sure that the installation has succeeded.


If the installation fails for any reason, check the [troubleshooting](install/troubleshooting.md) page for some potential solutions.



### Installing FSL into a different location


By default, FSL is installed into your user account home directory (at `~/fsl/`), and so does not require administrative privileges. If you wish to install FSL to a different location (e.g. `/usr/local/fsl/`), or are installing FSL on a multi-user system, you may need to enter your administrator password before the installation can proceed. You can run the `fslinstaller.py` script as described above - the script will prompt you for your password when necessary.


If you do not have permission to use `sudo` then the installer will fail and you will need to either install into a folder belonging to your user or to run the installer as the root user. Should you install as root, see the [configuring your account](install/configuration.md) for FSL section for details on how to setup your user account for FSL.



!> **Important note for Windows users:** FSL must be installed into the **WSL** file system - do not try and install FSL into a location in your Windows file system. The safest option is to accept the default location, which is in your WSL home directory at `/home/<wsl-user>/fsl/` (where `<wsl-user>` is the WSL user account that you created when you set up WSL).
