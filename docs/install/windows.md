# Installation on Windows


We recommend using Linux or macOS for using FSL. However FSL can be used on Windows via the Windows Subsystem for Linux (WSL, recommended) or by running the Linux version of FSL inside a virtual machine (VM).


We have created an installation video which gives an overview of the installation procedure for the Windows Subsystem for Linux using Windows 10:


<iframe width="560" height="315" src="https://www.youtube.com/embed/QNp3lyP57rw?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## System requirements

- Windows 10 or newer. You need to have Windows 10 version 2004 (Build 19041) or higher. You can find out your Windows version / build by opening the *Settings* app, and navigating to *System* > *About*.

- Dual core CPU (recommended minmum)
- 4GB RAM minimum (8GB+ recommended)
- Minimum 10GB, up to 75GB, of free disk space


## Install WSL / Ubuntu


Follow these steps to install a WSL Ubuntu Linux system within Windows. We recommend installing Ubuntu, but advanced users may wish to install a different flavour of Linux:


1. Install WSL by running the **Windows PowerShell (as administrator)** application. Copy+paste this command into the PowerShell window, press enter, and follow any prompts given:

        wsl --install -d Ubuntu-20.04

  If you experience any problems, consult the [Microsoft WSL documentation](https://learn.microsoft.com/en-us/windows/wsl/install).

2. Restart your computer.

3. Start the **Ubuntu** application to open an Ubuntu terminal (also known as a *shell*).

4. Set up a username and password for Ubuntu - this only needs to be done the first time you start Ubuntu. This can be different from your Windows username/password. **Make sure you remember your password**, as you will need it later on.


## Install FSL dependencies

Some system dependencies need to be installed into WSL before FSL can be installed. Copy+paste these commands into the WSL shell to install these dependencies. You may be prompted for a password - enter the Ubuntu password that you set up earlier:

    sudo apt update
    sudo apt -y upgrade
    sudo apt-get install dc python3 mesa-utils gedit pulseaudio libquadmath0 libgtk2.0-0 firefox libgomp1


[filename](common_install.md ':include')


## Additional configuration (Windows 11 only)

For Windows 11 you will need to add run an additional command to configure graphical rendering:

     echo "export LIBGL_ALWAYS_SOFTWARE=1" >> ~/.bashrc


## Install a X11 server (Windows 10 only)


On Windows 10, you need to install a separate application, called a *X11 server*, in order to use graphical FSL programs. We currently recommend installing [VcXsrv](https://sourceforge.net/projects/vcxsrv/), although several alternatives are available, including [MobaXterm](https://mobaxterm.mobatek.net/) and [Xming](https://sourceforge.net/projects/xming/).

To install VcXsrv:

1. Download and run the installer from https://sourceforge.net/projects/vcxsrv/. This will install an application called **XLaunch**.

2. In order for VcXsrv to work, you will need to add these lines to your Ubuntu configuration profile. Copy+paste the following lines into the Ubuntu shell:

        echo "export DISPLAY=\$(grep nameserver /etc/resolv.conf  | awk '{print \$2; exit}'):0" >> ~/.bashrc
        echo "export LIBGL_ALWAYS_INDIRECT=1" >> ~/.bashrc


3. Start the **XLaunch** app. You need to make sure that *Native OpenGL* is **deselected**, and *Disable access control* is **selected**, in the *Extra > Settings* panel.

4. Type `glxgears` into the Ubuntu shell, and press enter . A window with three spinning gears should open.

If `glxgears` doesn't work, it is possible that Windows Defender Firewall has blocked the VcXsrv process. Go to *Control panel* > *System and security* > *Windows defender firewall* > *Advanced settings* > *Inbound rules* and make sure that the VcXsrv rules are *not* set to *block* - if they are you will need to edit the VcXsrv rule and change it from *block* to *allow*.

## Installing FSL into a virtual machine (VM) instead of WSL


If you are unable to use the Windows Subsystem for Linux (WSL) as outlined above, another option for using FSL on a Windows computer is to use a virtual machine (VM). You can use the freely available [VirtualBox](https://www.virtualbox.org/), or [VMWare Workstation](https://www.vmware.com/uk/products/workstation-pro.html) if you have a license.

There are many online tutorials which describe how to install and set up a virtual machine, e.g.:
 * https://www.howtogeek.com/796988/how-to-install-linux-in-virtualbox/
 * https://www.arcserve.com/blog/dead-simple-guide-installing-linux-virtual-machine-windows

FSL can be installed into most modern Linux distributions - we recommend an Ubuntu LTS release, such as 20.04 or 22.04.

Once you have installed a Linux VM, you can install FSL in the same way as for other platforms - when downloading the `fslinstaller.py` script, be sure to use a web browser **inside the VM**.
