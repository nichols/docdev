# Installation on macOS


We have created an installation video which gives an overview of the installation procedure on macOS:


<iframe width="560" height="315" src="https://www.youtube.com/embed/LgRZLev1LRw?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


[filename](common_install.md ':include')


## Running the FSL GUIs


The file system that macOS uses can't distinguish between upper and lower case filenames, so `Feat` and `feat` are treated the same. Consequently, the FSL GUIs on the macOS release are called `<Toolname>_gui`, for example `Feat_gui`.


To run a GUI version of a program you can either type its capitalised name with an additional `_gui` appended in the terminal (e.g. `Bet_gui`), or you can start the main FSL GUI by just typing `fsl`.


## Migrating to zsh


Recent versions of macOS use `zsh` as the default terminal shell, whereas older versions used `bash`. If you installed FSL, and then upgraded macOS or changed your default shell to `zsh`, you may need to migrate your FSL configuration commands. See the [configuration](install/configuration.md) page for more details.
