# FSL configuration


If you have used the FSL installer your shell should have already been setup for running the FSL tools. However, there are situations where you may need to carry out this configuration by hand, for example when installing FSL on a multi-user computer. How you do this will depend on what shell you are using - type `echo ${SHELL}` to find out what shell you are using.


?> A shell is the command line you use to enter commands for your computer to run. There are several different shells available for UNIX-like OSes which use different command syntaxes to carry out the same tasks.


The instructions below will ask you to add lines of code to shell initialisation files. When doing this, make sure you are using a plain text editor, such as `nano`, `gedit`, or `emacs`. For example, if you are using the `bash` shell, you can edit the configuration file by typing `nano ~/.bash_profile` into a terminal.


!> **Warning:** Be careful when modifying your shell configuration/profile, as making a mistake could cause your terminal to become unusable.


## Where is my shell configuration file?

Depending on the shell you are using, your shell configuration will be stored in a different file:

| Shell  | Configuration file |
| ------ | ------------------ |
| `bash` | `~/.bash_profile`  |
| `dash` | `~/.bash_profile`  |
| `zsh`  | `~/.zprofile`      |
| `sh`   | `~/.profile`       |
| `ksh`  | `~/.profile`       |
| `csh`  | `~/.cshrc`         |
| `tcsh` | `~/.tcshrc`        |


## FSL configuration - `bash`/`dash`/`zsh`/`sh`/`ksh`

If you are using one of  `bash`, `dash`, `zsh`, `sh`, or `ksh`, add the following lines to the bottom of your shell configuration file - change the value of `FSLDIR` if you have installed FSL to a different location:

    # FSL Setup
    FSLDIR=~/fsl
    PATH=${FSLDIR}/share/fsl/bin:${PATH}
    export FSLDIR PATH
    . ${FSLDIR}/etc/fslconf/fsl.sh


## FSL configuration - `csh`/`tcsh`

If you are using `csh` or `tcsh`, add the following lines to the bottom of your shell configuration file - change the value of `FSLDIR` if you have installed FSL to a different location:

    # FSL Setup
    setenv FSLDIR ~/fsl
    setenv PATH ${FSLDIR}/share/fsl/bin:${PATH}
    source ${FSLDIR}/etc/fslconf/fsl.csh


## Configuring FSL on multi-user systems

You have several options for automatically setting up FSL for all users on a system, all beyond the scope of this document, but here are some suggestions on how you might go about this.

 - Change the template used for new user accounts (e.g. `/etc/skel` on Linux). New users will then get a suitably modified settings file.

 - Store the settings in an NFS shared folder. You could ensure that the default settings file looks for and sources a settings file on an NFS server. This is useful for a compute cluster as you only have to edit one file to have it change everywhere.

 - Modify the system-wide `.profile` or `.cscrc` file. On Linux you can create files `fsl.sh` and `fsl.csh` in `/etc/profile.d` containing the settings.


## Calling FSL from MATLAB

By default, the `fslinstaller.py` script will add a few lines to `~/Documents/MATLAB/startup.m` to allow you to call FSL commands from MATLAB.  If you need to perform this step manually, the MATLAB commands are as follows (change the value of `FSLDIR` to the location that you installed FSL into):

    % FSL Setup
    setenv( 'FSLDIR', '/Users/steve/fsl' );
    setenv('FSLOUTPUTTYPE', 'NIFTI_GZ');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;


## Customising FSL - environment variables


You can change various aspects of FSL behaviour by using environment variables. You can find a list of all of the environment variables that FSL understands below.

When the FSL configuration commands in your shell profile are run, default settings are applied. You can override these default settings simply by specifying different values for the relevant environment variable.

Perhaps the easiest method for customising FSL is to add your custom variable values to your shell configurtaion file (see above).

For example, if you are using the `bash` shell, and you wish to change the output file format of the FSL tools to uncompressed NIFTI images (`.nii` images, instead of the default `.nii.gz`), open `~/.bash_profile` in a plain text editor, and change the FSL configuration lines to this (making sure that the value for `FSLDIR` is correct for your system):

    # FSL Setup
    FSLDIR=~/fsl
    . ${FSLDIR}/etc/fslconf/fsl.sh
    PATH=${FSLDIR}/share/fsl/bin:${PATH}
    export FSLDIR PATH

    # My custom values for FSL environment variables
    export FSLOUTPUTTYPE=NIFTI


There are other places you could store your custom values for controlling FSL - look inside the `$FSLDIR/etc/fslconf/fsl.sh` file for more details.


|Variable name                | Default value           | Possible values  | Purpose                                                                                                                                                                              |
| --------------------------- | ----------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `FSLDIR`                    | -                       | -                | Points to the folder you installed FSL into, for example `/Users/steve/fsl`                                                                                                          |
| `FSLOUTPUTTYPE`             | `NIFTI_GZ`              | `NIFTI_GZ`       | Output results as a single gzip compressed NIFTI format file                                                                                                                         |
|                             |                         | `NIFTI`          | Output as a single NIFTI format file                                                                                                                                                 |
|                             |                         | `NIFTI2_GZ`      | Output results as a single gzip compressed NIFTI2 format                                                                                                                             |
|                             |                         | `NIFTI2`         | Output as a single NIFTI2 format file                                                                                                                                                |
|                             |                         | `NIFTI_PAIR`     | Output as NIFTI header/image pair                                                                                                                                                    |
|                             |                         | `NIFTI_PAIR_GZ`  | Output as gzip compressed NIFTI header/image pair                                                                                                                                    |
|                             |                         | `NIFTI2_PAIR_GZ` | Output as gzip compressed NIFTI2 header/image pair                                                                                                                                   |
|                             |                         | `NIFTI2_PAIR`    | Output as NIFTI2 header/image pair                                                                                                                                                   |
| `FSL_LOAD_NIFTI_EXTENSIONS` | `0`                     | `0` or `1`       | Controls whether FSL tools will load any NIfTI extensions stored in input files, and pass them through to output files. The default behaviour is to '''not''' load NIfTI extensions. |
| `FSL_SKIP_GLOBAL`           | `0`                     | `0` or `1`       | Controls whether BLAS threading is disabled. The default behaviour (`0` or unset) is to disable BLAS threading, as it does not give performance benefits  for most FSL tools.        |
| `FSLTCLSH`                  | `$FSLDIR/bin/fsltclsh`  |-                 | Path to the location of the TCL interpreter you wish to use, defaults to the interpreter installed in `$FSLDIR`                                                                      |
| `FSLWISH`                   | `$FSLDIR/bin/fslwish`   |-                 | Path to the location of the TCL/TK interpreter you wish to use, defaults to the interpreter installed in `$FSLDIR`                                                                   |
| `FSLCONFDIR`                | `$FSLDIR/config`        |-                 | Path to the location of the configuration files used to compile FSL - only necessary for compiling the source code                                                                   |
| `FSLDEVDIR`                 | -                       |-                 | Defines the location you are building FSL into when compiling the source code. This has to be set if you want to build a single project without running the full compilation script  |


## Editing text files


Whilst using FSL you are likely to need to use a text editor at some point, for example to create a `fsl.sh` settings file, a `~/.bash_profile` or `~/.zprofile` shell configuration, or an FEAT explanatory variable (EV) timing file.


There are lots of plain text editors available - some suggestions can be found below. Or, you may already be familiar with an advanced editor such as [Emacs](http://www.gnu.org/software/emacs/ ), [VS Code](https://code.visualstudio.com/), or [Atom](https://atom.io/).


### `nano`


One of the simplest options is a terminal-based program called `nano`.  This is installed on nearly all macOS / Linux computers, and is old-fashioned, but easy to use. Simply type `nano <file_to_edit>` into a terminal.


### TextEdit (macOS)



<img title="TextEdit New Document"
     src="install/TextEditND.png"
     align="right" width="30%"/>

<img title="TextEdit Open and Save"
     src="install/TextEditOS.png"
     align="right" width="30%"/>


macOS comes with a built in editor called `TextEdit` (look in `/Applications`). By default, `TextEdit` creates *Rich Text* documents which FSL cannot use, so you need to change the settings so that it will create *Plain Text* files.  Do this by opening the preferences window (`TextEdit` -> `Preferences`) and in the `New Document` tab choose `Plain Text` from the `Format` section. Then in the `Open and Save` tab deselect `Add ".txt" extension to plain text files`. You can now close this window and open a new file with `File` -> `New`.


### Linux


The Gnome desktop ships with an editor called [Gedit](http://projects.gnome.org/gedit/) which is straightforward to use. To start it type `gedit`, or `gedit <file_to_edit>` in a terminal.


### Windows

If you have installed FSL on Windows, using the Windows Subsystem for Linux, you can use `nano` or `gedit` to edit plain text files (although you may need to install `gedit` first).

The [Windows Notepad](https://www.microsoft.com/en-gb/p/windows-notepad/9msmlrh6lzf3?activetab=pivot:overviewtab) program is also a good choice for editing plain-text files on Windows, although you may need to make sure that Notepad saves files with [UNIX-compatible line-endings](https://devblogs.microsoft.com/commandline/extended-eol-in-notepad/). If you have trouble with text files created with Notepad, or another Windows editor, you could try converting it with the `fslFixText` tool.
