## Verify your installation <!-- {docsify-ignore} -->

To check that FSL has been installed, **close and re-open your shell/terminal**, and then follow the steps below. If any of the steps don't work, check the [troubleshooting](install/troubleshooting.md) page for some potential solutions.


1. Type `echo $FSLDIR` into the terminal, and press enter. This should print out the location that FSL has been installed to, e.g. `/Users/steve/fsl/`.


2. Type `fslmaths` into the terminal, and press enter. You should be presented
   with the help text for the `fslmaths` command:

        $ fslmaths

        Usage: fslmaths [-dt <datatype>] <first_input> [operations and inputs] <output> [-odt <datatype>]

        Datatype information:
         -dt sets the datatype used internally for calculations (default float for all except double images)
         -odt sets the output datatype ( default is float )
         Possible datatypes are: char short int float double input
         "input" will set the datatype to that of the original image

        ...

3. Type `imcp` into the terminal, and press enter. You should be presented
   with the help text for the `imcp` command:

        $ imcp

        Usage:
          imcp <file1> <file2>
          imcp <file1> <directory>
          imcp <file1> <file2> ... <fileN> <directory>

        Copy images from <file1> to <file2>, or copy all <file>s to <directory>

        NB: filenames can be basenames or include an extension.

        Recognised file extensions: .nii.gz, .nii, .img, .hdr, .img.gz, .hdr.gz


4. Type `fsl &` into the terminal, and press enter. The FSL GUI should appear:

   <div align=center>
   <img height="200" src="install/fsl.png"/>
   </div>


5. Type `fsleyes -std &` into the terminal, and press enter. The FSLeyes image viewer should start up, and display the MNI152 T1 standard template:

   <div align=center>
   <img height="200" src="install/fsleyes.png"/>
   </div>


If all of the above steps work, you have successfully installed FSL. If any of the steps do not work, refer to the [troubleshooting](install/troubleshooting.md) page for some suggestions.
