# Install FSL with conda

[Conda](https://conda.io/) is a platform-agnostic environment and package manager - conda can be used to create isolated environments on your file system, with each environment containing a different collection of software packages. All of the components that make up FSL are published as conda packages, so it is possible to install FSL software using conda, instead of using the [`fslinstaller.py`](install/index.md) script. This page contains an overview of conda and associated terminology, and some examples on how to use conda to install parts of FSL.


Conda is a very powerful tool with many options, and extensive documentation - if you want to learn more, the [_Getting started_ section](https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html) of the conda documentation is a good starting point.


To create a conda environment containing `fslmaths`, you can use a command such as:


```bash
conda create                                                  \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  -n fslmaths-env                                             \
  fsl-avwutils
```

where:
 - `-c <name-or-url>` tells conda where to download packages from
 - `-n fslmaths-env` tells conda to create an environment named `fslmaths-env`
 - `fsl-avwutils` is the name of the conda package that provides the `fslmaths` command.


If you have installed FSL 6.0.6 or newer, then you can use its `conda` command to create an environment - for example:


```
$FSLDIR/bin/conda create                                      \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  -n fslmaths-env                                             \
  fsl-avwutils
```

>The above command assumes that you have permission to write to the `$FSLDIR` directory - recent FSL versions will be installed to `~/fsl/` by default, so this will hopefully not be a problem.
>
>If you do not have permission to write to your `$FSLDIR` directory (or more generally, if you experience permission errors when trying to create a conda environment), you may need to configure conda to use a different location for its [environment and package cache](https://docs.conda.io/projects/conda/en/latest/user-guide/configuration/use-condarc.html#specify-env-directories). You can do this by creating a file called `~/.condarc` which contains (for example):
>```
>envs_dirs:
>  - ~/.conda/envs
>pkgs_dirs:
>  - ~/.conda/pkgs
>```


## Installing `conda`


FSL 6.0.6 and newer are based on conda, so if you have FSL installed, you can simply use `$FSLDIR/bin/conda` or `$FSLDIR/bin/mamba` to create environments and install packages.


!> While it is possible to install additional packages into your FSL installation, we recommend using separate conda environemnts - installing packages into an existing conda environment can cause already-installed packages to be updated, which could cause some FSL tools to break.


If you don't have FSL, or are working in an environment with limited resources (e.g. a cloud instance or Docker/Singularity container), there are a number of methods of installing conda:


 - [**Anaconda distribution**](https://www.anaconda.com/download/): This is a conda installation which comes with a wide range of software packages pre-installed
 - [**Miniconda**](https://docs.conda.io/projects/miniconda/en/latest/): This is a minimal command-line-based conda installation
 - [**Miniforge**](https://github.com/conda-forge/miniforge): This is version of Miniconda which is customised to source packages from conda-forge instead of the main anaconda channels (see below for some more details on conda channels).


There is an alternative to conda called [_mamba_](https://mamba.readthedocs.io/en/latest/), which is a faster C++ re-implementation of conda. If you have mamba available, you can usually use it in place of conda, simply by substituting the `conda` command for `mamba`. You can install mamba instead of conda with one of the following options:


 - [**Mambaforge**](https://github.com/conda-forge/miniforge) is a version of Miniforge which includes `mamba`
 - [**Micromamba**](https://mamba.readthedocs.io/en/latest/micromamba-installation.html) is a minimal mamba implementation (a single file binary). It is particularly useful for acquiring a conda implementation within automated cloud or CI environments.


After you have installed conda through one of the above methods, you need to make sure that the `conda` (or `mamba` or `micromamba`) executable is available via your `$PATH` environment variable.  For example, if you have installed Miniconda, and accepted the default installation location, you can make conda available by running this command, or adding it to your shell profile:


```
export PATH=~/miniconda3/bin::$PATH
```


Alternatively, the conda installer may offer to modify your shell profile, so that it is always available. Refer to the documentation for the installer that you have selected.


## Conda environments


A conda environment is a directory on your file system which contains a collection of installed conda packages. You can create an environment with the `conda create` sub-command. For example, to create a conda environment containing `fslmaths`, you can use this command:


```bash
conda create                                                  \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  -n fslmaths-env                                             \
  fsl-avwutils
```


This will create a conda environment named `fslmaths-env`, which contains the `fsl-avwutils` package (this is the name of the package that  provides the `fslmaths` executable).


Using the `-n <environment-name>` option will cause your environment to be created in a centralised location on your file system, typically `<conda-installation-location>/envs/<environment-name>`. For example, if you have installed conda to `~/miniconda3/`, the command above will create your environment at `~/miniconda3/envs/fslmaths-env/`.


It is also possible to create a conda environment in a specific location on your file system by using the `-p` option. For example, this command will create an environment at `~/fslmaths-env/`:


```bash
conda create                                                  \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  -p ~/fslmaths-env                                           \
  fsl-avwutils
```


In order to use the software installed in a conda environment, you need to _activate_ it. Activating an environment simply modifies your shell environment (e.g. your `$PATH` variable) so that the executables and libraries contained in the environment are available in your terminal session. When you activate a conda environment, you need to refer to it either by its name (if you created it with the `-n` option), or by its location on the file system (if you created it with the `-p` option).


Depending on how you have installed conda, you can activate an environment using one of the following methods:


 - If you have allowed conda to modify your shell profile, you can activate an environment with the `conda activate` sub-command - if you created your environment with `-n fslmaths-env`:
   ```bash
   conda activate fslmaths-env
   ```
   or if you created your environment with `-p ~/fslmaths-env`:
   ```bash
   conda activate ~/fslmaths-env
   ```
 - If you have instead added the `<conda-installation>/bin/` directory to your `$PATH`, you need to use a slightly different command to activate your environment:
   ```bash
   source activate fslmaths-env
   ```
   or:
   ```bash
   source activate ~/fslmaths-env
   ```


Once you have activated your environment, you will be able to call the commands that are installed in that environment - if you have been following the above example, you will now be able to run the `fslmaths` command.


## Conda channels


All packages that can be installed with conda are hosted on _channels_. A conda channel is simply a web site containing an index of all available packages, and links to download them. All FSL packages and their dependencies can be downloaded from two conda channels:


 - [The FSL conda channel](https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/) is where all FSL software and datasets are hosted
 - The [_conda forge_ channel](https://anaconda.org/conda-forge/) contains some auxillary software and also a range of third party packages on top of which FSL is built, such as `boost-cpp`, `OpenBLAS` and `nibabel`.


Channel order is very important, to ensure that packages are downloaded from the correct channel. When using conda to install FSL packages, you *must* specify both the FSL conda channel, and the conda-forge channel, in this order, i.e.:


 - `https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/`
 - `conda-forge`


or when calling conda at the command-line:


```
conda <command>                                               \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
   <other-options>
```


### The FSL development channel


Occasionally the FSL developers will publish unstable versions of FSL software to the _FSL development channel_, which is located at https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/. If you are a collaborator, or want to try out the latest features of a FSL project, you can install packages from the development channel by specifying it before the public channel, e.g.:


```
conda <command>                                                    \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/ \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/      \
  -c conda-forge                                                   \
   <other-options>
```


Note that if you are using `$FSLDIR/bin/conda` or `$FSLDIR/bin/mamba`, and wish to install packages from the development channel (or any other conda channel), you will need to make some changes to `$FSLDIR/bin/.condarc` to "unlock" the channel list - by default it is set to the following, which restricts the available channels to the public FSL channel and conda-forge:

```
channels: #!final
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ #!top
 - conda-forge #!bottom
```


If you open this file in a text editor and change the channel list to the following, you will then be able to use other channels when calling `conda`/`mamba` on the command-line:

```
channels:
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/
 - conda-forge
```
