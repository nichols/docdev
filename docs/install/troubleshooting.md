# FSL installation troublshooting


## Problems running `fslinstaller.py`

If the `fslinstaller.py` script fails, please save a copy of the messages that it printed in the terminal, and also check for a log file in your home directory called `fsl_installation_<date>.log` - if you [send us a message](support.md), include this information, as it will help us to diagnose the problem.


### `python: command not found` errors


Some systems do not have a command called `python`, and will print errors like `python: command not found`. If this happens try calling `python3 fslinstaller.py` or `python2 fslinstaller.py`.

Some versions of macOS do not have Python installed by default - if you do not have any Python command available, you can download an installer from https://www.python.org. Alternately, Python will be installed as part of Xcode, which you can install by running `sudo xcode-select --install`.


### `SSL: CERTIFICATE_VERIFY_FAILED` errors


If you experience SSL certificate errors on macOS, try running the installer with `/usr/bin/python fslinstaller.py` or `/usr/bin/python3 fslinstaller.py`. If you have installed Python separately from https://www.python.org, make sure you have run the *Install certificates* script, as described [here](https://bugs.python.org/issue43404#msg388144).

If you are still experiencing SSL errors, or you are using Linux or WSL, try running the `fslinstaller.py` script like this:

    python fslinstaller.py --skip_ssl_verify --conda


## `fsl: command not found` errors after installing

If you succesfully installed FSL, but are unable to run any FSL commands:

 - Make sure you have closed and re-opened your terminal / WSL shell. Another option is to reboot your computer.
 - Check your [shell configuration](install/configuration.md) to make sure that the FSL initialisation commands were correctly added.


## Localisation problems

FSL programs generally require a period to be used as the decimal separator ( e.g. `10.4`, `0.235` etc ). If your environment is configured to use a different character such as a comma then this may cause problems. Two potential fixes are to change the `LC_NUMERIC` variable value by:
 - typing the following lines into a terminal locally each time you want to run FSL, or
 - globally modifying all terminals by adding the following lines to your shell configuration file (see the page on [configuration](install/configuration.md)).

```
export LC_NUMERIC=en_GB.UTF-8
```
