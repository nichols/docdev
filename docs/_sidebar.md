* [FSL](/)
  * [Installation](install/index.md)
  * [Support](support.md)
  * [Task fMRI](task_fmri/index.md)
  * [Resting state fMRI](resting_state/index.md)
  * [Structural MRI](structural/index.md)
  * [Diffusion MRI](diffusion/index.md)
  * [Registration](registration/index.md)
  * [Statistics](statistics/index.md)
  * [Utilities and other tools](utilities/index.md)
  * [Other topics](other/index.md)
  * [FSL development](development/index.md)
  * [License](license.md)
