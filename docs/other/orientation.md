# Notes on image orientation


Orientation, labelling and getting left and right correct are important but often confusing issues. You should always look at your data and check if the labels in FSLeyes are correct. **No further analysis should ever be done if the labels in FSLeyes are incorrect or missing.** If your labels are fine then you are very unlikely to ever need any of these tools, except `fslreorient2std`, which is more generally useful and harmless.

Please note that there is a risk of left-right swaps occuring if you make any mistakes when using `fslswapdim` or `fslorient`, and so we **strongly recommend against using these tools unless you have incorrect labels**. It is *never* necessary to use these tools otherwise, since FSL works perfectly well with LAS, RAS or any other storage order for images. All the visualisation and analysis tools take care of everything behind the scenes and work with any image orientation, so please don't try and change these things unless the labels are wrong, as that is the only case where we recommend such changes.

If your labels *are* wrong then you will need to read on, otherwise you don't have to worry about the information below. We have tried to simplify the usage of the tools for common tasks involving orientation issues, although still providing flexible and powerful tools for the expert user.


## The labels in FSLeyes are correct but the image is oriented "weirdly"


This is not a problem. By default FSLeyes shows the image in the way that the scanner acquired it - the orientation of an image depends on the scanner configuration, and may not be the same as "standard" images such as the MNI152. As long as the anatomical labels for your image are correct when viewed in FSLeyes, this should not cause any problems.

However, you want your images to be in the same orientation as the standard templates (e.g. MNI152), or you are having problems with registration or other processing steps,  then you can run the tool `fslreorient2std`, which rotates (but does not modify) your image to have a standard orientation. Running `fslreorient2std` on all of your images, before any preprocessing, is good practice, as it is harmless, and can improve the robustness of some processing steps (e.g. registration using FLIRT).

However, make sure that the labels are correct in FSLeyes to start with, as this tool only works if they are correct.


## The labels in FSLeyes are wrong or missing


This is very serious and needs to be fixed before doing any analysis at all. Ideally it should be fixed by using a tool that creates a correct NIfTI file from the original DICOM files, such as [`dcm2niix`](https://github.com/rordenlab/dcm2niix). A correct converter should store the correct information in the NIfTI file so that the labels are correct when viewed in FSLeyes. If you have tried several DICOM to NIfTI converters and still cannot get the images converted correctly then you will need to use the advanced tools `fslswapdim` and `fslorient`, to fix the problem - these tools are described [here](utilities/fslutils.md), and steps for using them to correct a mis-oriented image is given below.

However, these steps should only to be attempted after reading and understanding the `fslorient` and `fslswapdim` tools, and the section below on background information on NIfTI orientation. Furthermore, unless you have some way of telling left from right in the image (e.g. by a vitamin capsule or clearly known structural asymmetry) this method can easily result in an incorrect left-right labelling and so should not be attempted unless you are very confident.

You may be able to fix a mis-oriented image with the following commands, where `<a> <b> <c>` need to be chosen, usually by trial and error, so that the image after this is in the same orientation (as viewed in FSLeyes) as the MNI152 images.

!> Make sure to back up your images, or work with a copy, before attempting these steps.

1. Clear the `sform`/`qform` affines - this completely resets any stored information about the image orientation.

   ```bash
   fslorient -deleteorient <image>
   ```

2. Rotate the image data into RAS orientation - the exact command (the values of `<a>`, `<b>`, and `<c>`) depends on the original data orientation - these need to be chosen by trial and error. After this step, the image data array (i.e. the voxel storage order) should be in RAS orientation.

   ```bash
   fslswapdim <image> <a> <b> <c> <image>
   ```

3. Reset the `qform.` We can use an identity matrix, as the scaling parameters used in the `qform` are ultimately taken frgom the image pixdims. After this step, the `qform` will be a diagonal matrix, with the image pixdims along the diagonal.

   ```bash
   fslorient -setqform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 <image>
   ```

4. Reset the `qform` code. This sets a flag stating that the `qform` is valid.

   ```bash
   fslorient -setqformcode 1 <image>
   ```

5. (Optional) Copy the `qform` to the `sform` so that they both contain the same transformation:
   ```bash
   fslorient -copyqform2sform <image>
   ```


## Background information on NIfTI Orientation

This section is purely optional but is extremely useful to understand if you are working with any of the advanced tools, have unusual problems, or are just plain interested.

The NIfTI standard is confusing for many people on the issue of orientation, largely because it is intrinsically a confusing issue. It is complicated because the intensity data is stored on disk as a list of numbers - there is no concept of spatial or anatomical location with these numbers, and so information and standard conventions are used to associate spatial and anatomical location information with these numbers.

To start with, voxel coordinates are associated with the numbers (intensity values), such that the first number is at coordinate (0,0,0) the second at (1,0,0), the third at (2,0,0), etc. So, for example, say the image was 64 by 64 by 20 in size, then the coordinates would go as:

 - (0,0,0)
 - (1,0,0)
 - ...
 - (63,0,0)
 - (0,1,0)
 - (1,1,0)
 - (2,1,0)
 - ...
 - (63,1,0)
 - (0,2,0)
 - ...
 - (63,63,0)
 - (0,0,1)
 - (1,0,1)
 - ...
 - (63,63,19)

Note that all coordinates start at 0, not 1, and that the first coordinate (i) here is the one that increases the fastest as you go through the list, followed by second (j) and then the third (k). These voxel coordinates - (i,j,k) - give spatial information (using information about how many voxels are in the three spatial directions) but do not attribute any anatomical information at all.

To go from voxel coordinates to more anatomically meaningful coordinates is the job of the `qform` and `sform` matrices that are stored in the NIfTI file. Note that these matrices only provide useful information if they have a non-zero code, since a zero code indicates that the matrix is _Unknown_ and hence cannot be used. Having two matrices here is a common source of confusion. They were originally proposed so that two different coordinates could be kept track of - one representing the scanner coordinate system (the real space inside the scanner bore) and the other one relating to standard space coordinates (e.g. MNI152). However, in practice, it is rare that both contain different information as they are often set to be the same, or one of them is _Unknown_ in which case the other contains all the useful information.

Either matrix (`qform` or `sform`) is used to convert the voxel coordinates into _real world_ coordinates (also called continuous coordinates, or mm coordinates, or other names). These new coordinates - called (x,y,z) here - have units of mm, and can take any values, not just positive whole numbers like the (i,j,k) voxel coordinates. Note that FSLeyes displays both voxel and mm coordinates.

> Aside: the conversion is done by multiplying the matrix with the voxel coordinate, reshaped as a 4x1 vector with the last element equal to 1 - although this detail is not important for any of the discussion or understanding.

If the `qform` (or `sform`) is set so that it gives information about scanner coordinates (as indicated by the code), then a standard convention is used to relate the coordinate values to the anatomy. The convention is:

 - +x = Right
 - +y = Anterior
 - +z = Superior

That is, moving in the positive x-direction (so that the x-coordinate increases) will be moving to the right, etc. This now allows left and right to be distinguished, which is usually not possible just by looking at brain images. Hence this information is absolutely crucial and getting this right for the conversion of the image to NIfTI format is extremely important. If it is incorrect then it would be easy for left and right to be assigned incorrectly and all analysis results to be on the wrong side. The information from the `qform` and `sform` is what is used by FSLeyes to determine the labels that are shown on the image.

If the `sform` is set so that it gives information about standard space coordinates then the situation is the same as above, since the standard convention used above was chosen to be the same as the convention chosen for the standard space coordinate systems (MNI152 and Talairach). The main difference between the two situations is that the transformation to standard space (represented by the `sform` matrix) also involves scaling and shearing of the coordinate axes to make the brain "fit" the standard templates, whereas the transformation to scanner space coordinates is normally just a rotation and translation.

**An aside on left-right conventions** It is possible to store an image in many different ways that are totally and completely equivalent in terms of the intensities and the (x,y,z) coordinates. What can be different is the voxel coordinates (and hence the order of the intensities stored in the file) but if only the images are being used in the analysis then these conventions are unimportant for all analysis purposes - they are purely related to the storage format, in this case NIfTI - only the (x,y,z) coordinates and the intensity values count with respect to the analysis. The one exception to this is when voxel coordinates are required for the commands (either on the command-line or in the GUI - for example `fslroi`, FDT GUI, etc.) and only in these cases is it important that the voxel coordinates are known. That being said, one common confusion arises when the left-right order of the storage is changed (but the mm coordinates and intensities are unchanged). For instance, imagine taking an image where the first voxel is on the left and then swapping the left-right order so that the new image is stored such that the first voxel is now the rightmost one, but modifying the `qform` and `sform` matrices appropriately so that this rightmost voxel still has the same (x,y,z) coordinate as it had before - just a different voxel coordinate. This kind of change will not affect any analysis in FSL or any programs that are fully NIfTI compliant. However, some programs are only happy with one sort of ordering (sometimes called left-handed/right-handed, or radiological/neurological, or RAS/LAS, etc.) This used to be the case for old versions of FSL, but is no longer true. But if only the order of the intensities in the file is swapped, without compensating for this in the `sform` and `qform` matrices, then the swapped version is no longer equivalent to the original. This is why both the data order and header information need to be kept in sync, and why running either `fslswapdim` or `fslorient` alone to change the orientation information is dangerous and usually incorrect.

**An aside on display orientation** Everything that has been said above about the NIfTI format refers to coordinates and data storage and has nothing to do with what way the images can be displayed in different viewing programs. Although the storage can be referred to as _radiological_ or _neurological_ this does not mean anything with respect to the displayed orientation. A valid NIfTI image can be displayed in any orientation that the viewing tool chooses, with the left side of the brain shown on the right side of the screen (so called radiological convention) or the other way around (neurological convention). It is also possible to display the inferior side of the brain at the top of the image, or any other configuration. It is entirely up to the display software how it chooses to show the image on the screen. What the NIfTI format does do is provide information to determine how to label the different parts of the image (with mm coordinates or anatomical labels such as left, right, anterior, posterior, superior and inferior). As long as the display tool correctly interprets this information and keeps it consistent with its choice of display orientation, there is no conflict between any type of NIfTI image and any displayed orientation.
