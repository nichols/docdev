# Related Software

Here we list other related software, including plugins, extensions, toolboxes, templates and atlases that have been created by other groups and are relevant to users of FSL.

# [FreeSurfer - Cortical surface and subcortical modelling](http://surfer.nmr.mgh.harvard.edu/)

FreeSurfer is a set of tools for reconstruction/flattening of the brain's cortical surface and subcortical segmentation/registration, produced at MGH, Boston. It can, for example, be used to overlay functional data onto the inflated/flattened cortical surface, or carry out multi-subject FMRI statistics on the cortical surface. We have worked together with MGH to make [interoperation of FSL and FreeSurfer](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FreeSurfer) as easy as possible.

# [FIX](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX)

FIX attempts to auto-classify ICA components into "good" vs "bad" components, so that the bad components can be removed from the 4D FMRI data. It is particularly suited for data-driven automatic artefact removal from resting-state fMRI data. This beta-version package requires you to have various other software than just FSL, such as MATLAB and R, and for now is not bundled as part of FSL.

Reza Salimi-Khorshidi & [Steve Smith](http://www.fmrib.ox.ac.uk/~steve)

# [ICA-AROMA: ICA-based Automatic Removal Of Motion Artifacts](https://github.com/rhr-pruim/ICA-AROMA)

ICA-AROMA is a data-driven method to identify and remove motion-related ICA components from FMRI data. It exploits a small, but robust set of theoretically motivated features specifically aimed at selecting motion-related components from FSL-MELODIC output. Unlike [FSL-FIX](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX) it is not aimed at more generic ICA_based denoising but due to the robustness of the feature set does not require classifier re-training. This beta-version package requires installation of Python2.7 and FSL. Read the provided 'Manual.pdf' for a description on how to run ICA-AROMA.

Raimon Pruim, Maarten Mennes & [Christian F. Beckmann](http://www.ru.nl/donders/research/theme-4-brain/statistical-imaging/)

# [Congrads: Connectivity Gradients in ROIs including trend Surface Modelling](https://github.com/koenhaak/congrads)

Congrads is a data-driven method for mapping the spatial organisation of connectivity within a pre-defined region-of-interest (i.e. connectopic mapping) and fitting the results with a spatial statisical model that summarises the map in terms of a limited number of trend surface model coefficients.

Koen V. Haak, Andre F. Marquand & [Christian F. Beckmann](http://www.ru.nl/donders/research/theme-4-brain/statistical-imaging/)

# [FSLNets](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLNets)

FSLNets is a set of simple MATLAB scripts for carrying out basic network modelling from (typically FMRI) timeseries data. This beta-version package requires you to have various other software than just FSL, such as MATLAB, and for now is not bundled as part of FSL.

[Steve Smith](http://www.fmrib.ox.ac.uk/~steve)

# [PALM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PALM)

PALM (Permutation Analysis of Linear Models) is a MATLAB/Octave tool that allows permutation inference using experimental methods not yet available in randomise, including permutation of data with complex dependence structure (multi-level) between observations, non-parametric combination (NPC), classical multivariate tests (MANOVA/MANCOVA/CCA), correction for multiple inputs, for multiple designs and contrasts, permutations together with sign flippings, ability to read various file formats, including non-imaging data, among several other features.

[Anderson Winkler](http://www.ndcn.ox.ac.uk/team/anderson-winkler)

# [FRIEND: Functional Real-time Interactive Endogenous Neuromodulation and Decoding](http://idor.org/neuroinformatics/friend)

FRIEND is GUI-based user-friendly software for real-time fMRI processing, multivoxel pattern decoding and neurofeedback. The package integrates routines for image preprocessing in real-time, ROI-based feedback (single-ROI percent signal change and functional connectivity) and brain decoding-based feedback using the FSL and libSVM libraries. FRIEND enables users to create or employ pre-specified visual stimuli for neurofeedback experiments.

FRIEND currently comes in two flavours: FRIEND for Windows and FRIEND Engine. The standalone Windows version runs embedded FSL and libSVM functions and provides a simple, straightforward solution. The FRIEND Engine is a multiplatform (Mac/Linux) toolbox that incorporates the same processing capabilities, but in which the front-end is separated from the processing module. This allows users to develop their own front-end GUIs while employing FRIEND processing capabilities through TCP/IP communication services. Advanced users can develop plugins to extend the original pipeline provided by the engine (e.g., Matlab functions for data preprocessing and multivariate classification, or stimulus presentation features from standard packages such as E-Prime, for customized feedback). A fully functional frontend is also provided along with the FRIEND Engine distribution.

[Jorge Moll](mailto:jorge.moll@idor.org)

# [featquery percent change calculation](http://mumford.bol.ucla.edu/perchange_guide.pdf)

This guide describes how featquery calculates percent change and how for certain types of studies a different procedure is necessary to calculate interpretable percent changes.

[Jeanette Mumford](http://mumford.bol.ucla.edu/)

# [Tom Nichols' FSL scripts](http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/nichols/scripts/fsl/)

A miscellaneous set of useful FSL shell scripts.

[Tom Nichols](http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/nichols/)

# [fslr package](http://cran.r-project.org/web/packages/fslr/index.html)

An R package that interfaces with FSL, giving the R user access to tools from FSL, such fslmaths and fslstats. The goal is to be able to interface with FSL completely in R, where you pass R nifti objects and the function executes an FSL command and returns an R nifti object if desired. The package can be installed in R using the command: `install.packages("fslr")`

The development version is hosted on https://github.com/muschellij2/fslr

[John Muschelli](mailto:muschellij2@gmail.com)

# [FLICA](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLICA)

FLICA (FMRIB's Linked Independent Component Analysis) is an exploratory analysis tool designed to automatically find structured components that cross multiple modalities of group data (e.g. TBSS, VBM, Freesurfer, etc).

[Adrian Groves](mailto:adriang@fmrib.ox.ac.uk)

# [Spinal Cord Toolbox](http://sourceforge.net/projects/spinalcordtoolbox/)

The Spinal Cord Toolbox is a comprehensive and open-source library of analysis tools for multi-parametric MRI of the spinal cord, such as fMRI, DTI and magnetization transfer. The toolbox notably includes state-of-the-art templates/atlases and robust methods for segmentation, registration and metric quantification.

# [melview](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/melview)

Melview is a Melodic Results Viewer. Coming Soon!

# [AutoPtx](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/AutoPtx)

A set of simple scripts and mask images to run probabilistic tractography in subject-native space using pre-defined protocols.

[Marius de Groot](http://www.bigr.nl/people/mariusdegroot)

# [optiBET](http://montilab.psych.ucla.edu/fmri-wiki/optibet)

Optimized brain extraction script for patients' brains.

[Evan Lutkenhoff](mailto:lutkenhoff@ucla.edu)

# [fmripower](http://fmripower.org/)

A matlab based power calculation tool that has been developed for the calculation of power for future group fMRI studies utilizing the analysis results from previous fMRI studies.

[Jeanette Mumford](http://mumford.bol.ucla.edu/)

# [DM Cluster](http://github.com/yarikoptic/dmcluster)

This approach to spatial clustering has some important properties (independence of cluster shape and total variance), carries out density estimation in 3D and shrinks the set of voxels to their densest regions.

[Stephen Hanson, RUMBA](http://sites.google.com/site/rumbalab)

# [DTI-TK](http://dti-tk.sourceforge.net/)

DTI-TK is a spatial normalization & atlas construction toolkit, designed from ground up to support the manipulation of diffusion-tensor images (DTI) with special cares taken to respect the tensorial nature of the data. It implements a state-of-the-art registration algorithm that drives the alignment of white matter (WM) tracts by matching the orientation of the underlying fiber bundle at each voxel. The algorithm has been shown to both improve WM tract alignment and to enhance the power of statistical inference in clinical settings.

Interoperability with FSL is straighforward using the fsl_to_dtitk script that converts a set of V[1-3] and L[1-3] dtifit files into the NIfTI tensor format that DTI-TK uses.

[Gary Hui Zhang](http://cmic.cs.ucl.ac.uk/mig//index.php?n=People.GHZhang)

# [dMRI Registration](http://www.nitrc.org/projects/dwiregistration/)

This FLIRT/FNIRT extension applies angular q-space interpolation when registering 4D diffusion-weighted MRI volumes. The scripts can simply switch to single volume registration and to default FLIRT/FNIRT functionality.

[Julio Duarte](mailto:duart022@umn.edu)

# [fast eigenvector centrality mapping (fECM)](https://code.google.com/p/bias/downloads/detail?name=fastECM.zip)

Eigenvector centrality mapping (ECM) determines network node centralities, providing a spatial distribution of network prominence. The fECM ZIP file contains a GUI, a NIFTI toolbox and example data, mask and atlas files. Interoperability with FSL is straighforward: the maps can be tested in between-group analyses using randomise .

[Alle Meije Wink](http://stackoverflow.com/users/1793968)

# [BundleFEAT](https://github.com/eduff/BundleFEAT)

Takes a FEAT directory and bundles all essential data of all levels of analysis into a single functioning directory, for easy sharing of results files.

[Eugene Duff](http://www.fmrib.ox.ac.uk/~eduff)

# [AxonDeepSeg](https://axondeepseg.readthedocs.io/en/latest/)

AxonDeepSeg uses convolutional neural networks for segmentation of microscopy data of nerve fibers. It includes a FSLeyes plugin which allows users to interactively segment microscopy images.

[NeuroImaging Research Laboratory at Polytechnique (NeuroPoly)](https://www.neuro.polymtl.ca/)

# [PCNtoolkit](https://github.com/amarquand/PCNtoolkit)

Predictive Clinical Neuroscience software toolkit (formerly nispat). Methods for normative modelling, spatial statistics and pattern recognition.

[Andre Marquand](mailto:a.marquand@donders.ru.nl)

# [COFFEE](https://www.biorxiv.org/content/10.1101/2023.09.19.558290)

Connectome Operations For FSL ExEcution [Benjamin Philip](mailto:bphilip@wustl.edu)
