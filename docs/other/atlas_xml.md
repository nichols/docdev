# FSL XML atlas description file format


Each atlas dataset included in FSL is represented by a set of image files (e.g. 3/4D NIfTI files) along with an XML-based _atlas description file_. The image files contain voxel classification values while the XML file contains some metadata about the atlas, for example label and summary image information.

There are two primary types of atlas images in FSL:

 - A *probabilistic* atlas is a 4D NIFTI image, where each 3D volume contains a probability map for one region in the atlas.  Each 3D volum contains values between 0 and 100, indicating the probability that a given voxel is classified as belonging to the structure represented by that volume.
 - A *label* (or *summary*, or *hard segementation*) atlas is a 3D NIfTI image which contains different discrete integer values for each region defined in the atlas.


Multiple versions of these images, at different resolutions, may exist (e.g. 1mm and 2mm versions of the same image may be present).


Each atlas dataset is accompanied by an XML-based atlas description file which describes the atlas, contains paths to the atlas image(s), and contains a description of every region in the atlas.


If you have an atlas image or dataset which you would like to add to FSL (or e.g. use within FSLeyes), you must write an atlas description file which describes your atlas.


The best way to create one of these files is to look at the atlas files that exist in ``$FSLDIR/data/atlases``. Create a copy of one of these files - select one which describes an atlas that is similar to your own atlas (i.e. probabilistic or label) - and then modify the atlas name, file paths, and label descriptions to suit your atlas.  Your `xml` atlas file should end up looking something like the following:


```xml
<atlas>

  <!-- The header defines the atlas name, type,
       and paths to the atlas image files. -->
  <header>

    <!-- Human-readable atlas name -->
    <name>Harvard-Oxford Cortical Structural Atlas</name>

    <!-- Abbreviated atlas name -->
    <shortname>HOCPA</shortname>

    <!-- Atlas type - "Probabilistic" or "Label" -->
    <type>Probabilistic</type>

    <!-- Paths (defined relative to the location
         of this XML file) to the atlas images.
         Multiple <images> elements may be present
         - one for each resolution in which the
         atlas is available. -->
    <images>

      <!-- If the atlas type is "Probabilistic", the
           <imagefile> must be a path to a 4D image
           which contains one volume per region.
           Otherwise, if the atlas type is "Label",
           the <imagefile> must be a path to 3D
           label image. -->
      <imagefile>/HarvardOxford/HarvardOxford-cort-prob-2mm</imagefile>

      <!-- If the atlas type is "Probabilistic", the
           <summaryimagefile> must be a path to a 3D
           label image which 'summarises' the
           probabilistic image. If the atlas type is
           "Label", the <summaryimagefile> is identical
           to the <imagefile>. There must be a
           <summaryimagefile> for every <imagefile>. -->
      <summaryimagefile>/HarvardOxford/HarvardOxford-cort-maxprob-thr25-2mm</summaryimagefile>
    </images>

    <!-- A 1mm version of the same atlas images. -->
    <images>
      <imagefile>/HarvardOxford/HarvardOxford-cort-prob-1mm</imagefile>
      <summaryimagefile>/HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm</summaryimagefile>
    </images>
  </header>

  <!-- The <data> element contains descriptions
       of all regions in the atlas. -->
  <data>

    <!-- Every region in the atlas has a <label> element which defines:

         - The "index". For probabilistic atlases, this is the index
           of the corresponding volume in the 4D image file. For
           label images, the index is the value of voxels which
           are in the corresponding region. For 3D summary images of
           probabilistic atlases, add 1 to the index to get the
           label value.

         - The "x", "y", and "z" coordinates of a pre-
           calculated "centre-of-gravity" for this region.
           These are specified as voxel coordinates,
           relative to the *first* image in the <images>
           list, above.

         - The name of the region. -->

    <label index="0" x="48" y="94" z="35">Frontal Pole</label>
    <label index="1" x="25" y="70" z="32">Insular Cortex</label>
    <label index="2" x="33" y="73" z="63">Superior Frontal Gyrus</label>

    <!-- ... -->

    <label index="45" x="74" y="53" z="40">Planum Temporale</label>
    <label index="46" x="44" y="21" z="42">Supracalcarine Cortex</label>
    <label index="47" x="37" y="15" z="34">Occipital Pole</label>
  </data>
</atlas>
```
