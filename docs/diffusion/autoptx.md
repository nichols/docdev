# Overview

!> We recommend most users to use [XTRACT](diffusion/xtract) instead.

<img src="diffusion/autoPTX_example.jpg" align="right" width="30%"/>

AutoPtx v0.1.1 is a set of simple scripts and mask images to run probabilistic tractography in subject-native space using pre-defined protocols. AutoPtx requires a working version of FSL and has been tested on Linux and Mac. The protocols (i.e., seed, target, exclusion and stop masks) were initially developed to allow measuring registration performance for diffusion MRI [1] and have been defined in standard space based on the protocols described in [2-5]. 

The process consists of two stages, a preprocessing stage (`autoPtx_1_preproc`) and a tractography stage (`autoPtx_2_launchTractography`). In short, the preprocessing stage runs a tensor fit, performs a registration and launches the bedpostx probabilistic model fit for each subject. The tractography stage runs probtrackx for each subject and for each white matter structure. 

AutoPtx is available under a licence similar to the FSL license, which is distributed with the plugin. If you use the plugin in your research, please reference [1].

---

# Running autoPtx

First, download the Plugin [autoPtx_0_1_1.tgz](diffusion/autoPtx_0_1_1.tgz ':ignore') and unpack with `tar -xvfz autoPtx_0_1_1.tgz`.

Both the [probabilistic model fit](diffusion/FDT#BEDPOSTX), started by `autoPtx_1_preproc`, and the [probabilistic tractography](diffusion/FDT#PROBTRACKX_-_probabilistic_tracking_with_crossing_fibres), started by `autoPtx_2_launchTractography`, use ‘fsl_sub’ to process the computionally heaviest tasks on a cluster (if available). Both autoPtx scripts, preproc and launchTractography, should be run consecutively in an interactive shell. 

To determine for which tracts the tractography is going to be performed, the launchTractography script reads the ‘structureList’ textfile. This file defines for each individual tract (left and right separate) the maximum run time and the seed count multiplier (listed in the table below). The seed count multiplier is based on a a tradeoff between run time and the observed number of particles in each tract, and can easily be customised if desired. 

---

# White matter tracts available

White matter tracts for which protocols are available are listed in the following table. Protocols with left/right homologues are indicated in column ‘l/r’. If a protocol contains a stop mask, the anatomical position of this mask relative to the tract is listed under the ‘stop’ column. The number of seed points per voxel is listed in the ‘seed #’ column. Tracts that are generated twice with inverted target-seed regions and then summed are listed under ‘Inv’. 

|   |   |   |   |   |
|---|---|---|---|---|
||l/r|stop|Seed # (*1000)|Inv|
|**Brainstem tracts**|||||
|Middle cerebellar peduncle|-||4.4|+|
|Medial lemniscus|+|superior|1.2|-|
|**Projection fibers**|||||
|Corticospinal tract|+||4|-|
|Acoustic radiation|+|medial|10|+|
|Anterior thalamic radiation|+|posterior|1|-|
|Superior thalamic radiation|+|inferior|0.8|-|
|Posterior thalamic radiation|+||20|-|
|**Association fibers**|||||
|Superior longitudinal fasciculus|+||0.4|+|
|Inferior longitudinal fasciculus|+|anterior|1.2|-|
|Inferior fronto-occipital fasciculus|+||4.4|-|
|Uncinate fasciculus|+||1.2|-|
|**Limbic system fibers**|||||
|Cingulate gyrus part of cingulum|+|anterior & posterior|20|-|
|Parahippocampal part of cingulum|+|superior & inferior|3|-|
|**Callosal fibers**|||||
|Forceps minor|-||0.6|+|
|Forceps major|-||0.6|+|

---

# Adding tracts

To add a protocol for automated tractography, create a folder in the ‘protocols’ folder, with the name of the tract and specify the following: 

|Name|Required|Content|Description|
|---|---|---|---|
|seed|*|Mask image|Seed mask in FMRIB-58 1mm space|
|target|*|Mask image|Target mask in FMRIB-58 1mm space|
|stop||Mask image|Stop mask in FMRIB-58 1mm space|
|exclude|* (may be empty image)|Mask image|Exclude mask in FMRIB-58 1mm space|
|invert||Empty text file|Indicates a 2nd run with inverted seed and target masks|

Then add the new protocol name to the ‘structureList’ textfile, and run the launchTractography script. 

---

# Visualizing tracts

To visualize the tracts in FSLView, the ‘autoPtx_prepareForDisplay’ script can be used. This script applies a (user defined) threshold on the tract density images and generates a script for viewing the subjects in FSLView. The tract image shown above is generated with this approach. 

---

# References

[1] De Groot, M., Vernooij, M.W., Klein, S., Ikram, M.A., Vos, F.M., Smith, S.M., Niessen, W.J., Andersson, J.L.R., 2013. Improving alignment in Tract-based spatial statistics: Evaluation and optimization of image registration. NeuroImage, 76, 400-411. 

[2] Mori, Susumu, Kaufmann, Walter E, Davatzikos, C., Stieltjes, Bram, Amodei, L., Fredericksen, Kim, Pearlson, Godfrey D, Melhem, E.R., Solaiyappan, Meiyappan, Raymond, G. V, Moser, H.W., Van Zijl, P.C.M., 2002. Imaging cortical association tracts in the human brain using diffusion-tensor-based axonal tracking. Magnetic Resonance in Medicine 47, 215–223. 

[3] Stieltjes, B, Kaufmann, W E, Van Zijl, P.C., Fredericksen, K, Pearlson, G D, Solaiyappan, M, Mori, S, 2001. Diffusion tensor imaging and axonal tracking in the human brainstem. NeuroImage 14, 723–735. 

[4] Wakana, S., Caprihan, A., Panzenboeck, M.M., Fallon, J.H., Perry, M., Gollub, R.L., Hua, K., Zhang, J., Jiang, H., Dubey, P., Blitz, A., Van Zijl, P., Mori, Susumu, 2007. Reproducibility of quantitative tractography methods applied to cerebral white matter. NeuroImage 36, 630–644. 

[5] Wakana, S., Jiang, H., Nagae-Poetscher, L.M., Van Zijl, P.C.M., Mori, Susumu, 2004. Fiber Tract–based Atlas of human white matter anatomy. Radiology 230, 77–87
