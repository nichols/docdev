# The diffusion processing pipeline

To call the FDT GUI, either run `Fdt` (`Fdt_gui` on a Mac), or run `fsl` and press the FDT button.

## Steps
A typical processing pipeline (and approximate time required for each stage, based on an Intel 2.66GHz processor, and a 60-direction whole-brain dataset of dimensions 128x128x70, at 2 mm isotropic resolution) would consist of:

1. Any study or scanner-specific pre-processing (e.g., conversion from DICOM to NIFTI using for example `dcm2niix`, removal of images affected by large artifacts). This would be done manually by the user.

2. Check your diffusion data (see below)

3. `topup` - Susceptibility-induced distortion correction (fieldmap estimation) using [topup](diffusion/topup.md).

4. `bet` - Brain extraction using [BET](structural/bet.md) on the distortion-corrected b0 (output of [topup](diffusion/topup.md)).

5. `eddy` - Distortion correction using eddy. `eddy` simultaneously corrects for eddy currents and subject motion. It can also use a fieldmap estimated by [topup](diffusion/topup.md) and correct for susceptibility-induced distortions. (~hours-days on CPU or less if multi-threaded or few hours on GPU).

After running eddy, all the diffusion-weighted images should be in alignment with each other and be undistorted. We can now either run a microstructural analysis or tractography. For a microstructural analysis the further pipeline would be:

- `dtifit` - Fitting of diffusion tensors on eddy-corrected data (~1 minute)

- `TBSS` - Comparison of the fractional anisotropy maps (or others) between subjects (see [TBSS](diffusion/tbss.md))

Or run tractography we need to:

- `bedpostx` - Fitting of the probabilistic diffusion model on corrected data (~15 hours single-threaded on CPU or less if multi-threaded or ~30 minutes run on GPU)

- Registration to structural reference image and/or standard space - (3-6 minutes)
- `probtrackx` - Probabilistic tractography run on the output of `bedpostx` (execution time depends very much on what the user wishes to do. Generating a connectivity distribution from a single voxel of interest takes about 1 second)
- `xtract` - An automated tractography tool, built on `probtrackx`, that allows reconstruction of a set of major pathways in humans and macaques.

## Diffusion data in FSL
Diffusion data is usually stored in a 4D NIFTI file, where each volume is acquired with a different b-value and/or gradient orientation. To run FDT tools we need:

- Diffusion weighted data (`data.nii.gz`): A 4D series of data volumes. This will include diffusion-weighted volumes and volume(s) with no diffusion weighting.

- Gradient directions (`bvecs`): An ASCII text file containing a list of gradient directions applied during diffusion weighted volumes. The order of entries in this file must match the order of volumes in the input data series.

The format is
```text
x_1 x_2 x_3 ... x_n
y_1 y_2 y_3 ... y_n
z_1 z_2 z_3 ... z_n
```

Vectors are normalised to unit length within the `dtifit` code. For volumes in which there was no diffusion weighting, the entry should still be present, although the direction of the vector does not matter! For technical details see this [FAQ entry](diffusion/faq.md).

- b values (`bvals`): An ASCII text file containing a list of b values applied during each volume acquisition. The b values are assumed to be in s/mm^2 units. The order of entries in this file must match the order of volumes in the input data and entries in the gradient directions text file.

The format is

```text 
b_1 b_2 b_3 ... b_n
```

## Check your data
To check whether your bvecs and bvals are correct, it is recommended to run a quick check on the raw data. This can be run after preprocessing or even before if the subject did not move too much:

1. Brain extraction using BET.

2. Run `dtifit`

3. View the principal eigenvector (V1) to check the vectors are correctly oriented with respect to the anatomy. If there is a problem then the bvecs need to be modified (change signs and/or permute order of components) in order to get the V1 vectors correctly oriented (repeat `dtifit` after modifying bvecs and view V1 vectors again until they are correct) before proceeding with any further processing.

Another useful tool for checking data quality is `eddy_qc`, which requires you to first run `eddy`.

