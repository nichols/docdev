# FAQ
- [How many diffusion encoding directions do I need for FDT tractography?](#Q1)
- [What resolution do I need for tractography?](#Q2)
- [How many reference (b=0) scans should I take?](#Q3)
- [I've checked my data carefully and I've run bedpost_datacheck. Something still isn't working and I'm really baffled. What can I do now?](#Qx)

## How many diffusion encoding directions do I need for FDT tractography? <a id='Q1'></a>
We would not recommend using fewer than 25 directions.

## What resolution do I need for tractography? <a id='Q2'></a>
It depends on what structures you're interested in, but everything we have published so far works well on data which is 2.5x2.5x2.5 mm^3 or better for the adult human brain. Imaging infants or non-human species would ideally require higher resolution.

## Do I need isotropic voxel resolution for tractography? <a id='Q3'></a>
It is not strictly necessary to have isotropic voxels, however we do strongly recommend it. If you do have anisotropic voxels, though, do not resample the data to make the voxels isotropic. Just use the data as it is.

## How many reference (b=0) scans should I take?
It depends on what b-factor you use, but for middle of the range b-factors (b=1000) you should take something like 1 b=0 image for every 8 or 9 diffusion-weighted image. (see Jones et al. 1999)

## What do I need before I can run dtifit or bedpostx?
You need raw diffusion-weighted data in a 4D volume (the 4th dimension indexes gradient dimensions). You also need to know the orientation of the diffusion-encoding gradient and the b-value applied during each diffusion weighted volume. Lastly, you need a binary mask of brain/non-brain.

## How do I know the gradient information?
Most modern scanners will output gradient information. If yours does not, you should ask your local physics team. NB, gradient information should be stored in bvecs and bvals files as described in the FDT documentation. NB2 - every volume in data requires an element in both bvals and bvecs.

## What conventions do the bvecs use?
The bvecs use a radiological voxel convention, which is the voxel convention that FSL uses internally and originated before NIFTI (i.e., it is the old Analyze convention). If the image has a radiological storage orientation (negative determinant of qform/sform) then the NIFTI voxels and the radiological voxels are the same. If the image has a neurological storage orientation (positive determinant of qform/sform) then the NIFTI voxels need to be flipped in the x-axis (not the y-axis or z-axis) to obtain radiological voxels. Tools such as dcm2niix create bvecs files and images that have consistent conventions and are suitable for FSL. Applying fslreorient2std requires permuting and/or changing signs of the bvecs components as appropriate, since it changes the voxel axes.

## Do I have to put an entry in my gradient files even for the reference (b=0) scans?
Yes - you can put any direction in the bvecs file, and you must put a zero in the correct location in the bvals file.

## If something's worth saying, should I say it three times?
Yes - every volume in data requires an element in bvecs and bvals. If your input directory is in FDT standard form you can do a quick check for the correct dimensions of all your files using bedpostx_datacheck.

## Do I need to normalise my bvecs?
No. All FDT programs that use bvecs (i.e. dtifit, bedpostx and qboot) will normalise the bvecs before fitting the models.

## Before I run dtifit, qboot or bedpostx, I need a brain mask - where do I get that from?
The brain mask needs to be in the same space as the diffusion data. We tend to run BET on one of our reference (b=0) scans.

## dtifit runs, but my outputs are all zero - what is wrong?
In most cases this is because your bvecs and bvals do not have carriage returns at the end. Make sure these files end in a new line, and rerun.

## dtifit crashes - what is happening?
In most cases, dtifit is crashing because your bvectors or bvalues are stored wrong, or don't match your data. If you have run dtifit from a standard directory, you check the basics with

```bash
bedpostx_datacheck <dirname>
```

## Bedpostx is taking ages and ages and ages and ages to finish, and I can't write may paper because my computer has seized up. Is this normal?
Yep. (On a 2.4GHz pentium, bedpostx will take about 20 cpu hours to process a 2.5x2.5x2.5 mm^3 dataset - however, at any one time, the process is v. small, so it shouldn't overburden your machine (hopefully!!) )

## Can I parallelise bedpostx?
Bedpostx is sge-capable, see here for information on setting up sge on your local system.

## When I've got my bedposted directory, how long will tractography take?
This depends on a few things, but the main ones are:

- Number of seed voxels: Apart from lots of data loading at the beginning, and data saving at the end, the actually tractography will take about 10-12 seconds per seed voxel on standard settings on a 2.4GHz Pentium IV.

- Number of samples: The time taken for tractography scales linearly with the number of samples drawn from the distribution at each seed voxel. In our experience, the distribution is well sampled by after 5000 samples are drawn, hence this is the default number. However, when you are doing quick testing, you can reduce this number. e.g. you could test with 1000 samples (which would take 1/5 of the time) and then when it came to doing detailed quantitative analysis, you could go back to 5000 samples.

## How should I threshold the path distribution estimates from ProbTrackx?
There is no right or wrong way to threshold these values. They form a continuous distribution which has been discretised in an arbitrary fashion (for example, if you double the voxel size, the probability of passing through each voxel will roughly double!). Note that this is NOT true for the "connectivity-based seed-voxel classification" mode where the discretisation is generally anatomically meaningful (the target masks are prespecified anatomical regions). In practice, thresholding at very low values (e.g., 10 out of 5000 samples) often tidies up path distribution estimates considerably. You will have to choose where to threshold based on your own specific question.

<a id="Qx"></a>
## I've checked my data carefully and I've run bedpost_datacheck. Something still isn't working and I'm really baffled. What can I do now? 

Send an email to the FSL list (see [How do I best report problems to the FSL mailing list?](support#fsl-mailing-list))

