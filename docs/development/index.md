# FSL development


FSL is a collection of MRI processing and analysis tools written in a range of programming languages (C++, BASH, TCL/TK, Python and Javascript to name a few), and published for use on macOS and Linux. FSL has historically been tightly coupled to UNIX-like operating systems, so using it on Windows is via the Windows Subsystem for Linux (or by installing FSL within a virtual machine).


FSL is published and installed using the [**conda**](https://conda.io/) package and environment management system. Each FSL project is built as a conda package, and published to an internally hosted (but publicly available) conda channel.


- The [FSL release history page](development/history/index.md) contains an overview of every version of FSL that has ever been released, starting from version 1.0 in June 2000.
- The [Local development page](development/local.md) contains notes on compiling FSL C++ tools on your local system.
- The [Management section](development/management/index.md) contains information on the procedures used by the FSL development team for developing FSL projects, and managing FSL releases.


## Older FSL releases

Prior to FSL 6.0.6, FSL was released and installed as a monolithic archive file, containing most FSL tools, and a separate conda environment called `fslpython`, configured after the archive file was installed.

Some of the reasons for transitioning to a purely conda-based release model were as follows:

 - Simplify FSL installation procedure
 - Reduce the size of `${FSLDIR}`
 - Allow "minimal" FSL installations, with only the utilities that are required
 - Improve build reproducibility
 - Improve support for container-based FSL installations
 - Simplify cross-platform management of external dependencies (e.g. VTK, OpenBLAS)
