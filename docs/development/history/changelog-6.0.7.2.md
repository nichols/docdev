# FSL 6.0.7.2, 22nd Aug 2023

- **New** fsleyes-plugin-mrs - for working with MRS data from within FSLeyes
- **New** spec2nii - command-line tool - for generating MRS NIfTI images

## fsleyes 1.7.0 -> 1.8.*

- *RF: Edit transform panel (nudge) now applies scaling factors such that the centre of volume/current cursor location is preserved*

  Requires fslpy 3.13.2

- *ENH: New ligthbox features*

  * Ability to draw slices overlapping
  * Ability to reverse the slice order

- *RF: Cluster panel continues to display info about last analysis if a non-FEAT overlay is selected.*

  Also remembers which contrast was selected if more than one analysis is loaded, and user switches between them

- *MNT: Replace setup.py with pyproject.toml*

  Because that's what all the cool kids are doing. Also refresh docker images, and remove various bits of administrative cruft.

- *MNT: Updates to work with notebook 7.x*

  The [Jupyter Notebook](https://github.com/jupyter/notebook/) project underwent a major refactor in version 7.x which broke FSLeyes notebook integration.

- *BF: Need to clear primary framebuffer, otherwise remnants of previous frame linger when drawing overlays with transparency*

  Example - zoom in, then click the Reset zoom button (the magnifying glass)

  ![image](/uploads/4b777fe860021d2921c0561e199dba59/image.png)

- *MNT: Migrate fsleyes.plugins from pkg_resources to importlib*

  Still TODO:

  - [x] Fix tests
  - [x] Update user docs
  - [x] Update example plugins in `fsleyes/tests/testdata/` to use `pyproject.toml`

- *ENH,RF: Support for layouts as plugins. Hide third-party plugins by default*

   - FSLeyes plugin libraries/files can now provide custom FSLeyes layouts.

   - FSLeyes plugins provided by installed third-party libraries are now hidden by default. Plugins provided by single-file plugin modules are still shown by default.

   - Third-party plugins can be shown via the `--showAllPlugins` command-line option, which can be permanently enabled by adding it as a default argument.

   - Third-party plugins are also automatically shown if a layout from the same library is loaded.

- Rel/1.8.0
- MNT: 1.8.0
- MNT: Python 3.8 requires that MetaPathFinder.find_distributions returns an iterator, and dict_values objects are not iterators
- Rel/1.8.0
- REL: 1.8.1
- CI: Typo in doc build scripts. Option to skip tests via environment variable
- REL: 1.8.2

### Detailed history

- RF: Edit transform panel (nudge) now applies scaling factors such that the centre of volume/current cursor location is preserved
- MNT: Need latest fslpy
- DOC: Changelog
- ENH: Skeleton for lightbox slice overlap option
- ENH: lightbox slice overlap. broken
- BF: fixes to worldToCanvas, drawCursor
- MNT: Update grid line routine
- MNT: --sliceOverlap command-line option
- BF: Bugfix in canvasToWorld, in mapping from world coordinates to grid coords
- ENH: Option to invert slice draw order, so that user can control which slices are drawn on top, when overlapping
- ENH: New option to reverse the order in which lightbox slices are displayed
- RF: Change sliceOverlapOrder symbols for consistency with sliceOrder
- MNT: cli options for sliceOrder/sliceOverlapOrder
- DOC: Changelog
- RF: Change new options to booleans, as it makes everything cleaner/easier. Add to canvas settings panel
- MNT: render support for new options
- DOC: Changelog
- TEST: Test new lightbox options
- BF: Oversights in widget generation - props has become stricter w.r.t. accepting invalid arguments
- RF: Cluster panel continues to display info about last analysis if a non-FEAT overlay is selected. Also remembers which contrast was selected if more than one analysis is loaded, and user switches between them
- DOC: Changelog
- MNT: Replace setup.py with pyproject.toml
- CI: update, remove cruft
- CI: Update docker images to ubuntu 20.04, and bump wxpython to 4.2.1
- TEST: add src dir to python path, as test benchmarks don't get installed into env
- DOC: No longer mocking things when building docs
- BF: Need to clear primary framebuffer, otherwise remnants of previous frame linger when drawing overlays with transparency
- DOC: Changelog
- MNT: Adapt notebook integration to work with notebook 7.x
- MNT: Make sure packaging is installed
- CI: Don't use setup.py install
- ENH: Ability to shut down notebook server and kernel. Not entirely sure about process for shutting down kernel, Will use for testing, but may have other uses in the future.
- BF,RF: Catch TimeoutExpired from Popen.communicate. Move kernel manager from jupyter config dir into notebook module so it can be used in testing.
- TEST: Unit test for notebook integration
- DOC: Changelog
- RF: Simplify things and use nbclassic exclusively
- MNT: nbclassic instead of notebook
- MNT: Update deprecated notebook config settings. Add a hook to allow additional settings to be injected - will be used in CI tests
- TEST: Inject allow_root setting into nb config
- MNT: open_browser still on NotebookApp
- MNT: MappingKernelManager is deprecated
- TEST: Fix a couple of errors in tests
- MNT: Ignore spurious errors from kernel after shutdown
- MNT: Migrate fsleyes.plugins from pkg_resources to importlib. Still in progrress
- TEST: update plugin examples
- MNT: Plugin libraries no longer need to be called "fsleyes-plugin-<name>". Removed the listPlugins function, as it is not used by anything.
- DOC: Update plugins userdoc page. Update conf.py files
- RF: Register Finder when it is created, rather than in initialise
- TEST: update plugin tests
- MNT: Small refactors and comments
- DOC: Changelog
- DOC: Small API doc fixes
- MNT: importlib.metadata.entry_points has different behaviour depending on python version
- MNT: allow notebook module to be imported without deps installed
- MNT: Don't assume any FSLeyes entry points have been registered
- TEST: listPlugins no longer exists
- RF: No need to differentiate between [non-]builtin plugins - what we want to avoid is loading plugins that have been imported from another module to be sub-classed. So here we add a constraint that plugin classes are only considered if they are defined in the module they are being loaded from
- MNT: Hide third-party plugins by default. They can be shown via the --showAllPlugins option
- DOC: note on --showAllPlugins
- DOC: Changelog
- TEST: Update plugin tests
- MNT: Remove unnecesssary checks
- MNT: f-string/log strings
- RF,ENH: FSLeyes plugin files/libraries can now provide custom layouts
- ENH: Layouts module can load layouts providedby plugins
- BF: Turns out those checks were necessary, so that bad plugins are ignored. Fix lookupLayout.
- TEST: update plugin tests
- RF: Change SHOW_THIRD_PARTY_PLUGINS so it can be a toggle or a list of plugins to show.
- TEST: Test filtering third party plugins
- MNT: Enable all plugins from third party library when a layout is loaded
- TEST: Test layoutModule function
- DOC: Changelog
- DOC: Improve layout format docs
- DOC: plugin userdocs
- DOC: Tweaks to plugin docs
- DOC: CHangelog
- MNT: version
- DOC: Changelog
- DOC: typo in docs
- MNT: Hook to debug notebook server
- TEST: Run notebook tests with additional debug
- CI: Hook to pass extra arguments to pytest
- CI: Allow running tests from web interface; extra args were not being passed to first batch of tests
- CI: abiility to trigger pipeline from web interface on upstream repo
- MNT: Python 3.8 requires that MetaPathFinder.find_distributions returns an iterator, and dict_values objects are not iterators
- MNT: Jesus, lists are not iterators either. Python 3.8 compatibility fix
- DOC: Changelog
- MNT: Filter some more useless messages from underlying libraries
- CI: Fix zenodo deposit script - it was using setup.py to get the FSLeyes version
- DOC: Changelog
- CI: API/user documentation generation now needs dependencies to be installed
- MNT: Version
- CI: Typo in doc build scripts. Option to skip tests via env var
- BF: Recurse into packages when loading builtin plugins, otherwise the "if item.__module__ != mod.__name__" check will cause some plugins to be missed (e.g. the AtlasPanel is defined in a submodule of fsleyes.plugins.controls.atlaspanel)
- DOC: Changelog
- DOC: Explain atlas panel value discrepancy
- TEST: Regression test for loading builtin plugins in submodules
- MNT: Add log statement back in
- DOC: Fix super script formatting
- MNT: version

## fslpy 3.13.2 -> 3.13.3

- Rel/3.13.3

### Detailed history

- MNT: Notifier callback functions can now be no-args
- DOC: Changelog
- TEST: Test noargs callback function
- DOC: changelog
- MNT: version

## fsl-add_module 0.5.0 -> 0.5.1

- *BF: Use original un-expanded destination path, when generating a new manifest from an existing one.*

  Update the `fsl_generate_module_manifest` command to use the original un-expanded destination paths, when generating a new manifest from an existing one. Otherwise the new manifest will have `~` and environment variables expanded with respect to the local environment.

  Also replace `setup.py` with `pyproject.toml`


### Detailed history

- BF: Use original un-expanded destination path, when generating a new manifest from an existing one. Otherwise the new manifest will have ~, and env vars expanded.
- DOC: Changelog
- MNT: version
- MNT: Replace setup.py with pyproject.toml
- CI: Update build/install commands

## fsl-avwutils 2209.0 -> 2209.1

- *add --json option and associated logic*

  closes #6


### Detailed history

- add --json option and associated logic

## fsl-deface 1.3.0 -> 1.3.1

- *BF: Only need to use imtest*

  This script is potentially called with an extension-stripped filename by `fsl_deface`, which will cause the first file-existence test to erroneously fail. The `imtest` call will perform all necessary checks so the first test can be safely removed.


### Detailed history

- BF: Only need to use imtest

## fsl-fabber_core v4.0.10 -> 4.0.11


### Detailed history


## fsl-installer 3.5.3 -> 3.5.5

- *Mnt/mamba timeout*

  `mamba` often crashes on slow connections, and/or when running behind a proxy, with an error like:
  ```
  19:56:49 fslinstaller.py: 933:  [stderr]: # >>>>>>>>>>>>>>>>>>>>>> ERROR REPORT <<<<<<<<<<<<<<<<<<<<<<
  19:56:49 fslinstaller.py: 933:  [stderr]:
  19:56:49 fslinstaller.py: 933:  [stderr]:     Traceback (most recent call last):
  19:56:49 fslinstaller.py: 933:  [stderr]:       File "/home/peilingwong/fsl/lib/python3.10/site-packages/conda/exceptions.py", line 1124, in __call__
  19:56:49 fslinstaller.py: 933:  [stderr]:         return func(*args, **kwargs)
  19:56:49 fslinstaller.py: 933:  [stderr]:       File "/home/peilingwong/fsl/lib/python3.10/site-packages/conda_env/cli/main.py", line 78, in do_call
  19:56:49 fslinstaller.py: 933:  [stderr]:         exit_code = getattr(module, func_name)(args, parser)
  19:56:49 fslinstaller.py: 933:  [stderr]:       File "/home/peilingwong/fsl/lib/python3.10/site-packages/conda/notices/core.py", line 109, in wrapper
  19:56:49 fslinstaller.py: 933:  [stderr]:         return func(*args, **kwargs)
  19:56:49 fslinstaller.py: 933:  [stderr]:       File "/home/peilingwong/fsl/lib/python3.10/site-packages/conda_env/cli/main_update.py", line 132, in execute
  19:56:49 fslinstaller.py: 933:  [stderr]:         result[installer_type] = installer.install(prefix, specs, args, env)
  19:56:49 fslinstaller.py: 933:  [stderr]:       File "/home/peilingwong/fsl/lib/python3.10/site-packages/mamba/mamba_env.py", line 153, in mamba_install
  19:56:49 fslinstaller.py: 933:  [stderr]:         transaction.fetch_extract_packages()
  19:56:49 fslinstaller.py: 933:  [stderr]:     RuntimeError: Download error (28) Timeout was reached [https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/noarch/fsl-data_first_models_336_bin-2006.0-0.tar.bz2]
  19:56:49 fslinstaller.py: 933:  [stderr]:     Operation too slow. Less than 30 bytes/sec transferred the last 60 seconds
  ```

  We can tell `mamba` not to enforce a timeout by setting an environment variable `MAMBA_NO_LOW_SPEED_LIMIT=1`

  https://github.com/mamba-org/mamba/issues/1941

- *Ci/manifest branch*

  Don't assume that default branch is `master`


### Detailed history

- MNT: Tell mamba not to abort on slow downloads. Also recognise ksh shell
- DOC: Changelog
- BF: env vars need to be strings
- CI: Don't assume branch name is master
- DOC: Changelog
- MNT: version

## fsl_mrs 2.1.6 -> 2.1.12

- *Assorted dynamic processing and MRSI processing updates.*

  - Added `fsl_mrs_proc mrsi-align` which can perform frequency and phase alignment across voxels using cross correlation.
  - Added `fsl_mrs_proc mrsi-lipid` which can perform lipid removal using Bilgic et al's L2-regularised method.
  - `fsl_mrs_proc fshift` can now take NIfTI images of matched shape witht he `--shifthz` anf `--shiftppm` to apply per-voxel shifts.
  - Fixed bug with incorrectly calculated zero order phase when given in degrees.
  - Improved interface and options for dynamic fitting driven preprocessing.
  - Fixed minor bug in `fsl_mrs_summarise`
  - Fixed bug where quantification information wasn't generated if no tissue fractions were given.
  - Added warnings when quantification reference or water has zero integral.

- *MRSI documentation and 2.1.7 release prep*

  Also contains fixes for compatibility with modified nifti-mrs 1.0.0 API

- Enh: Dynamic MRSI fitting
- *BF: mrsi + dynamic tweaks*

  Tweaks, bug fixes, and improvements to the dynamic + MRSI pipeline
  - `mrsi-align` now deals with higher dimensions properly
  - Fixed a bug in `mrsi-align`
  - Added more options to fsl_dynmrs for dealing with mrsi
  - Improved help text of fsl_dynmrs.
  - `fsl_dynmrs` is now dependant on the `fsl_sub` package when processing MRSI.

- ENH: Custom init functions for dynamic fitting
- *ENH: Better covariance estimation for svs coil combination.*

  Coil covariance estimation can now use multiple spectra held in higher NIfTI-MRS dimensions.

- *BF: Single coil covariance estimation for preproc pathways.*

  - Coil covariance estimation is now common across all inputs to `fsl_mrs_preproc` and `fsl_mrs_preproc_edit`.
  - Fallback option to disable coil pre-whitening when it isn't possible to calculate.

- *BF:fsl_mrs_proc unlike*

  Fixed bug in `fsl_mrs_proc unlike` calling the wrong processing function.

- *BF: fsl_mrs_proc unlike and complete fsl_mrs_proc tests*

  Further fixes to `fsl_mrs_proc unlike` and testing for all `fsl_mrs_proc` subcommands.

- *BF: Fix example_usage notebooks.*

  Fixes minor issues in the example_usage notebooks.


### Detailed history

- Fix small bug introduced in summarise
- Minor upgrades to dynamic preprocessing. Fixed bug in phase units calculation.
- Fix missing quantifiaction table in report when no fractions.
- Remove duplicate argument metavar
- Add user firendly errors when quantification fails.
- Add mrsi alignment functions.
- Add tests for mrsi processing.
- Add fsl_mrs interface for mrsi-align. Deal with higher dims. Tests added.
- Add b0 map shifts.
- Add lipid regularisation for mrsi and tests.
- Add l2 lipid pathway for proc
- Clean up tests etc for nifti-mrs 1.0.0 api
- Add documentation.
- Prepare for release.
- First pass at mrsi + dynamic
- Working dynamic + mrsi pipeline
- [skip-tests] Update changelog
- Ensure scaling consistent across voxels.
- Tweaks for mrsi alignment and dynamic fitting handling.
- Further tweaks, add fsl_sub queue option
- Add fsl_sub queue option to merge job
- [skip-tests] tweaks to the dynmrs arguments
- [skip-tests] add job hold for merge job
- [skip-tests] move to fsl_sub dependency
- [skip-tests] no string jids
- [skip-tests] don't start with number or have spaces in job name
- [skip-tests] Add _ to job names. Add save fits to options.
- [skip-tests] Fix call to create_nmrs.
- [skip-tests] Fit scaling.
- Update changelog.
- Add fsl_sub plugins to requirements.
- [skip-tests] First pass at custom init functions
- [skip-tests] Fix variablemapping bug
- [skip-tests] Add init results to report.
- Add tests and documentation for _init functions
- Better covariance estimation for svs.
- Single coil covariance estimation for preproc pathways.
- Fixed bug in fsl_mrs_proc unlike directing to wrong function
- Update todo.
- More detailed testing of unlike in proc. Fixes bugs with failed output.
- More through testing of fsl_mrs_proc.
- Fix notebooks.

## fsl-newimage 2203.9 -> 2203.10

- *Bf/extensions*

  This allows fully qualified filenames with extension '.img' to be recognised as an image by `newimage`. In this case the returned "valid" filename is the '.hdr' paired image - as that is used by NewNifti to begin the load process.


### Detailed history

- BF: Handle .img extensions as pre-FSL6. Improved logic
- MNT: Removed debug text
- ENH: Doubled extensions to standardise format

## fsl-nets 0.7.1 -> 0.8.0

- *MNT: Change nets.web server logic*

  Change nets.web server logic so that server is kept alive until the expected number of successful HTTP requests are made. Naively shutting down
  the server after 5 seconds can be problematic on slow systems.

- *Mnt/server crash*

  Make `fsl.nets.web` exit early if the HTTP server falls over for any reason

- *BF,RF: Fix/adjust plot_spectra*


   - [x] Fix bug - was using a hard-coded value for the number of samples in the call to `rfftfreq`
   - [x] ~Change spectrum estimation to use `scipy.signal.welch`, so that it now supports varying numbers of timepoints across subjects~
   - [x] Change spectrum estimation to use a fixed window length across all data sets, so that it can support varying numbers of timepoints across subjects
   - [x] New `nets.plot_timeseries` function, useful for QC
   - [x] Add some basic unit tests


### Detailed history

- MNT: Change nets.web server logic so that server is kept alive until the expected number of successful HTTP requests are made. Naively shutting down the server after 5 seconds can be problematic on slow systems.
- MNT: version
- MNT: Exit early if server dies for any reason
- MNT: version
- MNT: Set shutdown flag if absolutely anything goes wrong in server process
- BF: Comparing wrong dimension
- BF,RF: Was using hard-coded nsamples in call to rfftfreq. Change spectrum estimatiobn to use welch method, so that it now supports varying numbers of timepoints across subjects
- MNT: testing configuration
- ENH,RF: New plot_timeseries function, useful for QC. Option to pass bgimage when generating thumbnails. Consider all .txt files when loading from a directory.
- RF: Back to using raw fft, as welch method suppresses too much noise, which is undesirable when looking at spectra for noise components. Use same window length for all time series to handle varying #timepoints
- MNT: Change default nnodes for plot_timeseries
- MNT: version
- DOC: readme
- STY: line width
- TEST: A couple of basic tests
- MNT: test dependencies
- CI: run unit tests on MRs

## fsl-pipe 0.6.1 -> 1.0.0


### Detailed history


## fsl-pyfeeds 0.12.1 -> 0.12.2

- *Mnt/mkexec*

  Try to make `feedsRun` scripts executable if they are not already


### Detailed history

- MNT: Try nd make feedsRun script executable if not
- DOC: Changelog
- MNT: Version

## fsl-utils 2203.2 -> 2203.4

- *MNT: Ignore messages from dlerror, as it can return a message even when dlopen successfully loaded a library*

  Also expand the list of candidate library file names

- *BF: Clean up includes in threading.h, ensure that cstdint is included*

  Encountering compilation errors in some environments along the lines of:
  ```
  threading.h:60:6: error: variable or field 'fsl_init_blas_threading' declared void
  60 | void fsl_init_blas_threading(uint16_t flags=0);
  |      ^~~~~~~~~~~~~~~~~~~~~~~
  threading.h:60:30: error: 'uint16_t' was not declared in this scope
  60 | void fsl_init_blas_threading(uint16_t flags=0);
  |                              ^~~~~~~~
  threading.h:27:1: note: 'uint16_t' is defined in header '<cstdint>'; did you forget to '#include <cstdint>'?
  ```


### Detailed history

- MNT: Ignore messages from dlerror, as it can return a message even when dlopen successfully loaded a library
- BF: Clean up includes in threading.h, ensure that cstdint is inclued
