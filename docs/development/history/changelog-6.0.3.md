# FSl 6.0.3, 26th November 2019

FSL 6.0.3 contains minor fixes and updates across a number of FSL programs.

## `armawrap`

- upgraded to armadillo 9.700
- suppressed cholesky convergence failures

## avwutils

- `fslcreatehd`: better string handling for descrip field
- `fslcreatehd`: origin uses FSL coordinate

## `basisfield`

- limited same size checks to first three dimensions

## `bet2`

- switched to using error trap in shell script

## `cudabasisfield`

- limited same size checks to first three dimensions

## `distbase`

- separated LIBS and PROJECTS and sort by required build order

# `etc`

- MATLAB code: fixed intermittent fread bug

# `feat5`

- changed b0 unwarping placeholder values to 0.0

# `fsl_deface`

- minor bug fixes

# `libvis`

- fixed normpdf namespace collision

# `melodic`

- fixed namespace collisions
- fixed crash for large number of components

# `mm`

- fixed namespace collision

# `NewNifti`

- fixed issue with byte-swapping ANALYZE files

# `post_install`

- added `awscli`
- updated FSLeyes to 0.31.2
- updated pyfeeds, funpack and fslpy

# `ptx2`

- fixed bugs related to HR data and several stop/avoid masks in file

# `swe`

- enabled voxelwise design options
- added output of edof if requested
- save dof if requested
- bug fixes and enhancements

# `xtract`

- fixed GPU queue bug
- added `xtract_viewer`
- added `ptx_options`
- added option to pass probtrackx options as a text file
