# FSl 6.0.2, 29th September 2019

## General

- When a 4D image is provided to an input intended for 3D images, behaviour has reverted to "FSL5" usage in that the input will be truncated to a 3D volume, with an alert to highlight this has taken place.

## New tools

- `swe`: a tool for the analysis of longitudinal and repeated measures neuroimaging data based on the sandwich estimator.
- XTRACT: a command-line tool for automated tractography in humans and macaques.

## avwutils

- Fixed ANALYZE support in `fslchfiletype`, plus fully report all known formats ( including NIFTI2 ).
- Better `fslstats` output formatting with atlas masks.
- Allow use of non-continuous atlas mask labels in `fslstats`.

## `basil`

- Performance improvements.

## `bet`

- Exit gracefully (with relevant error messages) if expected ouput file is not written
- Fixed `-B` option where skull images were being generated even though not requested

## `bianca`

- Fixed nibabel deprecation issue.

## `config`

- Unified all machine configurations (auto detects most machine differences now).
- Updates for multi-format eddy build.

## `distbase`

- FSL build improvements

## `fabber`

- Fix compiler warnings.
- Update documentation.
- Update builds for FSL 6.0.x.
- General performance improvements.

## `fdt`

- Update fdt GUI to match multi-shell/single-shell defaults.

## `feat`

- Fix model setup wizard for randomise groups.

## `flirt`

- Removed interactive coordinate input for `img2imgcoord`, and `img2stdcoord`.

## `fsl_deface`

- Performance improvements.
- Removed deprecated option in `fslreorient2std` call.

## `lesion_filling`

- Updated help text.
- Added check for matching input image sizes.
- Added check to make sure lesion masks are within white matter mask.
- Added `-c` (`--components`) option to save each lesion as a separate image.

## `melodic`

- New version of `dual_regression`.

## `miscvis`

- New option for choosing optimal slice in `slices_summary`.

## `mist`

- Only run brainstem training if given correct modalities.

## MSM

- Fixed build for FSL 6.0.x.

## `oxford_asl`

- General performance improvements.
- Added unit tests.

## `post_install`

- FSLeyes is now installed via the main `fslpython` environment.
- Eddy configuration script for Linux (`$FSLDIR/etc/fslconf/configure_eddy.sh`). Automatically sets symlinks to `eddy_openmp` > `eddy` and `eddy_cudaX.X` > `eddy_cuda`.
- Added `fmrib-unpack` to fslpython.
- Added `pyfeeds` to fslpython.

## `sgeutils`

- General improvements.

## `utils`

- Updated boolean option handling for all FSL commandline programs.

## `verbena`

- Updates for reordering of parameters.
