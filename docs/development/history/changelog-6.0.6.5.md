## 6.0.6.5, 12th May 2023

### fslpy 3.11.2 --> 3.11.3

- BF: Don't check fsldir/bin/dcm2niix if $FSLDIR is not set


### fsl-eddy_qc v1.2.1 --> v1.2.3

- MNT: replace use of get_data with get_fdata, as the former raises an error in nibabel 5


### fsl-newimage 2203.6 --> 2203.8

- ENH: conditional define for boost
- TEST: Basic tests for volume<->mask shape compatibility
- ENH: begin/end methods for maskedIterator
- BF: Working maskedIterator
- BF: simplified, non-recursive increment
- BF: recursive solution to maskedIterator
- BF: mean, variance to work with subset masks


### fsl-pyfeeds 0.11.0 --> 0.12.1

- RF: Don't use nibabel Nifti1Image.get_fdata, as it causes RAM blow-out. Store image data in its native type instead.
- BF: get_data() raises error in nibabel >=5
