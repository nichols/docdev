## 6.0.7.1, 18th July 2023

### fsl-fugue           	2201.2    	-->	2201.3

- MNT: Update GE to GEHC
- ENH: GE fieldmap processing, kindly provided by Brice Fernandez
