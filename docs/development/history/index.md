# FSL release history


## 6.0.7.4, 29th September 2023

[Full changelog](development/history/changelog-6.0.7.4.md)

 - FSLeyes 1.9.0 - Updates for compatbility with newer versions of matplotlib; new "FSLView" mode for viewing legacy data sets.
 - FSL-MRS 2.1.13 - Add group level f-tests to the `fmrs_stats` tool.
 - New `open_fsl_report` command for opening PNM HTML report files.


## 6.0.7.3, 5th September 2023

[Full changelog](development/history/changelog-6.0.7.3.md)

- Fixed some issues with the `fslchfiletype` command
- `immv`/`imcp` honour `$FSLOUTPUTTYPE` more closely
- FSLeyes - minor bug fixes and new `--cmapCycle` command-line option


## 6.0.7.2, 22nd August 2023

[Full changelog](development/history/changelog-6.0.7.2.md)

- **New** FSLeyes MRS plugin - for working with MRS data from within FSLeyes
- **New** `spec2nii` command-line tool - for generating MRS NIfTI images
- `fsl_mrs` 2.1.12 - A range of bug fixes and improvements
- Fixed threading initialisation issue in some FSL tools
- FSLeyes 1.8.0 - New lightox options to overlap slices, and reverse slice order. A range of other bug fixes and improvements.
- `fsl-nets` - fixed bug in the `plot_spectra` function.
- `newimage` - Suppress some debug messages, fixes to `.hdr`/`.img` file extension handling
- `fslstats` - new `-json` option for JSON-formatted output


## 6.0.7.1, 18th July 2023

[Full changelog](development/history/changelog-6.0.7.1.md)

- `fsl_prepare_fieldmap` - now supports field maps obtained from GE scanners.


## 6.0.7, 14th July 2023

[Full changelog](development/history/changelog-6.0.7.md)

- **New** `mmorf` - The MultiMOdal Registration Framework. For simultaneous, nonlinear alignment of multimodal image data.
- **New** `fsl-nets` - New Python-based re-implementation of the MATLAB FSLNets scripts, for FMRI network modelling
- **New** `file-tree` - Framework to represent structured directories in Python
- **New** `fsl-pipe` - Declarative pipelines based on file-tree
- **New** `bet4animal` - species-tuned BET, kindly provided by Takuya Hayashi
- FSLeyes 1.7.0 - New colour range option for vector overlays. Ability to show slice locations in lightbox view. Improved _clip by_ and _modulate by_ options when a negative colour map is in use.
- `fsl_prepare_fieldmap` - Now supports GEHC fieldmaps, kindly provided by Brice Fernandez
- `fsl_mrs` - various improvements
- `topup` - New config file that works a little better on data with large and sharp distortions
- `xtract` - Added `xtract_qc`, a script to calculate a range of tract metrics


## 6.0.6.5, 12th May 2023

[Full changelog](development/history/changelog-6.0.6.5.md)

- Nibabel compatibility fixes
- Bug fixes related to mask-related operations on 4D images


## 6.0.6.4, 9th March 2023

[Full changelog](development/history/changelog-6.0.6.4.md)

- Bug fixes in `${FSLDIR}/etc/fslconf/fsl.csh`


## 6.0.6.3, 1st March 2023

[Full changelog](development/history/changelog-6.0.6.3.md)

- Compatibility fixes regarding use of the `dc` command on macOS
- FSLeyes 1.6.1 - FSLeyes can now read "default" command-line arguments from a file called `default_arguments.txt`, stored in the FSLeyes settings directory.
- `eddy_qc` - minor improvements to plots and layout.
- `xtract` - Added `HUMAN_BABY` option for xtract with neonate xtract protocols.


## 6.0.6.2, 4th January 2023

- `oxford_asl` - Compatibility fix for newer versions of wxpython with type annotations


## 6.0.6.1, 5th December 2022

[Full changelog](development/history/changelog-6.0.6.1.md)

- FSL installations now default to using `mamba` instead of `conda`, as `mamba` allows for much quicker installations.
- `gps`- Major rewrite to enable simultaneous optimisation of multiple shells
- `fsl_mrs` - Minor improvements


## 6.0.6, 22nd November 2022

[Full changelog](development/history/changelog-6.0.6.md)

- **New** FSL is now built and distributed using the [conda](https://conda.io) package and environment manager - more details at https://git.fmrib.ox.ac.uk/fsl/conda/docs.
  - Wrappers/entry points for all FSL commands are added into `${FSLDIR}/share/fsl/bin/` - shell configurations should add this directory to the `${PATH}`, instead of `${FSLDIR}/bin/`, to avoid other non-FSL commands in `${FSLDIR}/bin/` from polluting the user's shell environment. The new `fsinstaller.py` script should automatically update your shell configuration.
  - The `cluster` command has been renamed to `fsl-cluster` to avoid conflict with the Graphviz `cluster` command. An entry point for the FSL command, called `cluster`, is installed into `${FSLDIR}/share/fsl/bin/`.
  - FSL CUDA tools (`eddy`, `bedpostx_gpu`, `probtrackx2_gpu`) are now statically linked the CUDA 10.2 toolkit, and so should work with any GPU that supports CUDA 10.2 or newer - it should no longer be necessary to install the CUDA toolkit - only a GPU driver is required. For eddy, the `eddy` command will run the CUDA version if a GPU is present, or the OpenMP version otherwise.
- New `FSL_LOAD_NIFTI_EXTENSIONS` environment variable to control whether NIfTI extensions are loaded and preserved.
- New `FSL_SKIP_GLOBAL` environment variable to restrict BLAS threading.
- FSLeyes 1.5.0 - Redesigned the lightbox view to simplify behaviour and interaction.
- `topup` - performance improvements gained by parallelising some core operations.
- `fslmaths` - new `-dog_edge` option, which applies a difference of gaussians edge filter.
- `distancemap` - now uses superior Euclidean Distance Transform algorithm for improved performance.
- The `pnm_stage1` command now opens the PNM HTML report page using a web server, to avoid web browser security errors.


## 6.0.5.2, 19th May 2022

- FSLeyes 1.4.0 - support for TrackVis `.trk` and Mrtrix3 `.tck` tractogram files.


## 6.0.5.1, 8th November 2021

- `oxford_asl` Fix bug introduced in FSL 6.0.5 - can cause second registration to fail


## FSL 6.0.5, 20th July 2021

[Full changelog](development/history/changelog-6.0.5.md)

- **New** Python-based rewrite of `fsl_sub` supporting both SGE and SLURM.
- Fixes to `fslcreatehd` setting dimensions and intent fields.
- Improvements to `bet` for non-human images.
- New `bianca_perivent_deep` command, for subclassification of periventricular and deep lesions
- Slice-to-volume motion correction has been implemented for the OpenMP version of `eddy`.
- Major improvements to `fabber` and `oxford_asl`.
- FSLeyes 1.0.13 - New lighting effect on volume overlays in 3D view. New _annotation_ panel,  allowing simple shapes and text to be overlaid on an ortho view. New _sample along line_ tool, allowing data from an image to be sampled along a line and plotted. New _logarithmic scaling_ option, which allow voxel intensities to be mapped to the colour map logarithmically, rather than linearly.
- New options in `verbena`.
- Generalise `xtract` to allow it to be used with any species.


## FSL 6.0.4, 11th August 2020

[Full changelog](development/history/changelog-6.0.4.md)

- **New** `FSL_MRS`: an end-to-end tool for MRS data analysis, including data conversion, basis simulations, FID processing, fitting, quantitation, and visualisation
- **New** TIRL (Tensor Image Registration Library) v2.1 (beta): a Python-based general-purpose image registration framework with prebaked scripts for registering stand-alone histological sections to volumetric MRI data.
- `eddy` - major performance improvements in the slice-to-volume motion correction and the movement-by-susceptibility estimatio in the GPU version of `eddy`.
- `fabber` - New options, fixes, and substantial performance imporvements
- `dtifit` - Improvements to kurtosis fitting.
- `oxford_asl` - New wxPython-based BASIL graphical interface.
- FSLeyes 0.34.2 - Metadata from BIDS JSON sidecar files is now shown in the overlay information panel. New _modulate alpha_ option which allows opacity to be modulated by voxel/vertex intensity.
- Replaced bundled zlib with cloudflare-zlib, for improved `.nii.gz` load/save performance


## FSl 6.0.3, 26th November 2019

[Full changelog](development/history/changelog-6.0.3.md)

- `melodic` - fixed crash which could occur for a large number of components
- `ptx2`- fixed bugs related to HR data and several stop/avoid masks in file
- `swe` - enabled voxelwise design options
- `xtract` - added `xtract_viewer`, and ability to pass options through to `ptx2`.
- `avwutils` - fixes and improvements to the `fslcreatehd` command
- FSLeyes 0.31.2 - new _File tree_ panel, for viewing data in structured directories. Added first class support for NIfTI images with complex data type.


## FSl 6.0.2, 29th September 2019

[Full changelog](development/history/changelog-6.0.2.md)

- **New** `swe`: a tool for the analysis of longitudinal and repeated measures neuroimaging data based on the sandwich estimator.
- **New** `xtract`: a command-line tool for automated tractography in humans and macaques.
- **New** `fmrib-unpack`: A processing tool for working with non-imaging UK BioBank data.
- FSLeyes 0.30.1 - now with support for RGB(A) NIfTI images, and for 2D images (`.jpg`, `.png`, etc).
- Fixed ANALYZE support in fslchfiletype, plus fully report all known formats ( including NIFTI2 ).
- When a 4D image is provided to an input intended for 3D images, behaviour has reverted to "FSL5" usage in that the input will be truncated to a 3D volume, with an alert to highlight this has taken place.
- Better fslstats output formatting with atlas masks.
- Allow use of non-continuous atlas mask labels in `fslstats` .
- `bet`- Fixed `-B` option where skull images were being generated even though not requested
- `flirt` - Removed interactive coordinate input for `img2imgcoord`, and `img2stdcoord`.
- `fsl_deface` - Removed deprecated option in `fslreorient2std` call.
- `lesion_filling` - Added `-c` (`--components`) option to save each lesion as a separate image.
- `melodic` - new version of `dual_regression`.


## FSL 6.0.1, 11th March 2019

- `eddy` has had major features added, along with minor bug-fixes and improvements.
  - `eddy` now models how the susceptibility field changes with subject movement. This is described in NeuroImage 171 (2018) 277-295.
  - The `--slspec` parameter can now directly read `.JSON` output from `dcm2niix`.
  - Two new files are generated, `<my_output>.eddy_command.txt` and `<my_output.eddy_values_of_all_input_parameters`. These are used by `eddyqc`, and can be used as a "documentation" of exactly how eddy was run on a given data set.
- `fsl_deface`, a structural anonymisation script used by the UK Biobank scripts has been added.
- `probtrackx2` has been updated ( including `probtrackx2_gpu` )
- `bedpostx_gpu` has been restored (after being unavailable in 6.0.0)
- `cudimot`: a new front-end for implementing GPU-based model-fitting routines has been added.
- BASIL/`oxford_asl` (for perfusion quantification using ASL) - new version incorporating new GUI, distortion correction, partial volume correction (for all ASL data), improved registration to structural image, revised inference process, additional kinetic models, 'white paper' compliant mode.
- FSLeyes supports loading of complex images, and has more options controlling colour bar display.
- FSLeyes now comes bundled with a copy of dcm2niix
- The latest version of `eddy_qc` is included.
- An interaction between Armadillo and CUDA causing GPU tools to crash.
- `fslcpgeom` correctly updates q-form header information.
- `fslswapdim` now functions as per pre-FSL6.0.0 changes.
- An issue with `topup` that meant that the estimation of the field was suboptimal
- A rare crash during peristimulus-plot generation has been fixed.
- Images with non-unity `scl_slope` will be treated as floating-point type.
- Images that incorrectly report the existence of an extension field in post-header bytes ( e.g. created by some Matlab IO routines ) will be read normally rather than reporting an error.
- A standard set of OpenBLAS libraries are included in the linux binary downloads. If compiling the sources yourself, you will need to obtain these for your specific system.
- Performance issues with `vecreg` and `mcflirt` have been resolved.
- A bug with the `-ing` option in `fslmaths` has been fixed.


## FSL 6.0.0, October 2018

- This version has major changes to the lower-level image and matrix libraries to improve capabilities and performance for larger data sets. Specifically, the [NEWMAT](http://robertnz.net/nm_intro.htm) linear algebra library has been replaced with the more modern [armadillo](https://arma.sourceforge.net/).
- **New** `eddy_qc` - performs automated dMRI data assessment, both at the single subject and group levels
- FSLeyes: Now includes Jupyter notebook integration, maximum-intensity projection and a new plugin architecture.
- FSLpy includes wrappers for certain FSL commands.
- The HCP1065 standard space DTI templates have been added
- `fsl_ents` is a tool for extracting component timeseries from an ICA directory ( e.g using the output from FIX )
- The version of eddy is the same as in 5.0.11. A new version of eddy will be released with FSL 6.0.1
- Tools are now NIFTI-2 capable, although this should currently be regarded as beta-functionality.
- This also includes the latest versions of tools introduced since the last major update:
  - MIST: a tool for multi-model subcortical segmentation.
  - MSM: a tool for cortical surface registration.
  - BIANCA: A tool for automatic ( trained ) segmentation of white-matter hyperintensities.


##  FSL 5.0.11, 23rd March 2018


- eddy has been updated
- A bug in FEAT noise-estimation has been fixed
- A subthalamic nucleus atlas has been added, kindly provided by Birte U. Forstmann
- The fslpython environment has been updated, more scripts now use this environment
- Nudge now uses FSLeyes


## FSL 5.0.10, 25th April 2017

- FSL now installs its own Python environment in `$FSLDIR/fslpython`.
- FSLeyes - an advanced image viewer with many new features. FSLview has been deprecated, but is still included in this release for legacy purposes.
- ( Beta ) Higher level analyses in FEAT can use randomise instead of FLAME to generate statistics.
- ( Beta ) MIST - A new and more flexible tool for multimodal subcortical segmentation. The beta release supports segmentation of the putamen, globus pallidus, caudate nucleus and thalamus using T1-, T2- and diffusion-weighted data. For more information, see http://dx.doi.org/10.1016/j.neuroimage.2015.10.013.
- ( Beta ) MSM - A new tool for cortical surface registration. Registration can use a wide variety of univariate, multivariate and multimodal feature sets. For more information, see http://dx.doi.org/10.1016/j.neuroimage.2014.05.069.
- ( Beta ) BIANCA - A new tool for automatic segmentation of white matter hyperintensities based on manual training data. The tool is very - flexible with respect to input MRI modalities. For more information, see http://dx.doi.org/10.1016/j.neuroimage.2016.07.018.
- ( Beta ) FILM, FLAMEO and MELODIC can run on CIFTI files.
- Minor changes have been made to FEAT reporting to make results complaint with NIDM-Results ( http://dx.doi.org/10.1038/sdata.2016.102 )
- The default cluster-forming threshold in FEAT has been changed to 3.1.
- A Fornix template image, kindly provided by Brian Gold, has been added to the atlases.
- A 18-connectivity bug in cluster has been fixed.
- Temporal concatenation in MELODIC now defaults to using MIGP.
- POSSUM has a number of improvements and fixes.


## FSL 5.0.9, 29th September 2015

- Verbena: Initial release of model-based analysis of perfusion from DSC-MRI based on the vascular model, including the option to separately estimate the arterial/macrovascular component.
- BASIL:
  - `asl_calib` bug fix for default T2 values
  - `oxford_asl` - tidy up of usage, option for voxelwise calibration and improved compliance with consensus 'white' paper quantification approach, option to specify the noise prior (particularly for single TI data)
  - `asl_file` - includes the option for surround subtraction
  - `quasil` - fix to dispersion option;
- `randomise` has much improved handling of voxelwise regressors
- `fsl_anat` now has separate options for weak and strong bias field correction, with a change in the default to weak bias
- `dtifit` has now the option for correcting for gradient non-linearities unhidden
- `eddy_correct` has an option to choose the type of interpolation used (trilinear, spline)
- `select_dwi_vols` allows quick extraction of a certain b-shell from DWI data
- `bedpostx` has a new model (model=3), in both CPU & GPU version. Instead of using a stick kernel to represent a fibre compartment, a "zeppelin" axially symmetric tensor can be used.
- Updates on ptx2:
  - Inclusion of wt_stop masks. Allows streamlines to enter a volume, propagate within, but terminate upon exit. Or for surfaces allow streamlines to cross once, but terminate upon crossing twice.
  - Inclusion of `--omatrix1 --network` option for constructing ROI x ROI connectomes
  - Option of outputting Path length as well as Path distribution
  - Correction of bugs for surface counting


## FSL 5.0.8, 4th December 2014

- a bug in the header information of topup output files has been fixed
- FEAT has improved handling of non-fatal error conditions
- An error in the asl_calib script has been fixed
- An issue with missing images in the VIENA report has been resolved.
- All FSL scripts now internally set their locale to C
- A potential race condition with multiple calls to maskdyads or slices has been corrected.
- FDR now saves adjusted values as 1-p if the input is 1-p.


## FSL 5.0.7, 14th July 2014

- The analysis options for lower-level FEAT have been simplified to match changes in the processing pipeline
- FSL can now be compiled on the latest version of OSX ( using LLVM )
- Improvements have been made to the accuracy of POSSUM for a range of smaller voxel sizes and large motions.
- oxford_asl: Corrected the values for inversion efficiency for pASL and cASL data, correct assignment of default BAT value for cASL (now matches with usage).
- Connectivity-based parcellation atlases have been added, courtesy of Rogier B. Mars.
- The highpass temporal filter in fslmaths now removes the mean component


## FSL 5.0.6, 9th December 2013

- An error with voxelwise regressors in lower-level FEAT introduced in FSL 5.0.5 has been fixed. We would strongly recommend re-running any lower-level FEAT analysis that used voxelwise regressors ( e.g. PNM ) generated with FSL 5.0.5
- An issue with POSSUM has been resolved.
- FSL enviroment scripts have been updated for OSX Mavericks.
- The ASL GUI has been updated.


## FSL 5.0.5, 15th October 2013

- Bedpostx is now CUDA capable for GPU-equipped clusters, please see the wiki for information on usage and configuration.
- Bedpostx now has a gradient non-linearity option.
- Probtrackx2 can now output an ROIxROI network matrix.
- A bug in concat mode in the FSL 5.0.4 version of MELODIC has been fixed, it is recommended to re-run any temporal-concatenation analyses run with that version.
- An issue with some MELODIC analyses not converging has been resolved.
- Bug fixed for multishell qboot.


## FSL 5.0.4, 10th May 2013

- Fixed an issue with MELODIC Group ICA introduced in 5.0.3
- Changes for HCP pipeline compliance
- Updates to some atlas definitions


## FSL 5.0.3, 7th May 2013

- Minor bug fixes and feature enhancements
- Timeseries masking fixed in fslstats
- Precision fix in smoothest
- Build environment variables for OSX Mountain Lion
- Documentation updated to latest online wiki
- improvements to robustfov


## FSL 5.0.2.2, 15th February 2013

- FSLview patch for HCP
- Harvard-Oxford subcortical atlas fix


## FSL 5.0.3, 3rd January 2013

- fsl_sub patch


## FSL 5.0.2, 11th December 2012

- Numerous minor bug fixes
- Added Subthalamic Nucleus Atlas, courtesy of Birte Forstmann


## FSL 5.0.1, 1st October 2012

- Fixed critical scripting issues with FEAT
- Fixed post-hoc output timecourses in MELODIC
- Minor fixes to a small number of source files


## FSL 5.0.0, September 2012

- New live Wiki-based documentation
- Dual Regression - maps a set of group-ICA spatial maps back into individual subjects' datasets
- BASIL - a new suite of tools for the quantification of resting perfusion from Arterial Spin Labeling MRI, including correction for variable bolus arrival time, macro vascular contamination and the effects of partial voluming.
- BBR - a new option for EPI to structural registration, which is an implementation of Doug Greve's Boundary-Based-Registration method that includes built-in fieldmap-based distortion correction. This is now the default option for all FMRI to structural registrations as it has been found to be substantially more accurate, and is now built into FEAT and MELODIC registration.
- PNM - Physiological Noise Model is a tool for correcting physiological noise, based on cardiac and respiratory recordings (typically, but not restricted to, pulse-ox and bellows). It provides the standard RETROICOR-style regressors as well as additional ones including interactions (cardiac * respiratory), RVT, heart rate. This is strongly recommended to be used in any FMRI studies focussing on the brainstem, spinal cord, or other structures in the inferior part of the brain.
- Atlases - Oxford-GSK-Imanova structural and connectivity striatal atlases - two main atlases provided that sub-divide the striatum based on (i) the white matter connectivity to major functional regions of the cortex and; (ii) a structural atlas derived from literature-based rules that subdivides striatum into caudate, putamen and ventral striatum.
- FIRST - a new vertex analysis method, based on projection to the average surface normal, that provides (i) a general GLM setup, including arbitrary contrasts; (ii) input images for randomise (not surfaces); and (iii) a new selection of multiple-comparison methods to be employed for vertex analysis. In addition, the old vertex analysis method, with surfaces, remains available.
- Fieldmaps - fsl_prepare_fieldmap is a tool designed to make the initial processing of fieldmaps easier (designed for only Siemens scanners currently). It creates the necessary inputs (rad/s map and magnitude) for FEAT processing.
- Randomise - Lesion masking - a new script, setup_masks, is supplied to assist in using lesion masks (user-supplied) in randomise to exclude (inconsistently located) lesions from group studies.
- Lesion Filling - a new tool for taking structural images and filling lesion areas (specified by user-defined masks, e.g., drawn by hand) in order to improve segmentation and registration performance.
- SIENA - a separate ventricle-based analysis is now possible
- Probtrackx2 - A new version of the probabilistic tracking tool that includes many more options such as: handling surfaces (from Caret, FreeSurfer and FIRST), mixtures of images and surfaces, matrices output, and more...
- Various updates to the FDT GUI. It allows the user to set all the available command-line options
- BedpostX - A new model for estimating fibre orientations using multi-shell data is available. Also new options for modelling different noise distributions.
- Qboot - A tool for estimating fibre orientations non-parametrically using ODFs (orientation distribution functions). Uncertainty is estimated using residual bootstrap.
- Tools for processing surface files, such as surf2volume, surf2surf, surf_proj
- fsl_anat (BETA version) - Anatomical Processing Script (BETA version) - a flexible new tool that combines the existing features of brain extraction (BET+FNIRT-based masking), registration (FLIRT + FNIRT), tissue-type segmentation (FAST), subcortical segmentation (FIRST), with substantially enhanced bias-field correction and automatic reorientation and cropping.
- topup (BETA version) - A tool for estimating and correcting susceptibility induced distortions
- eddy (BETA version) - An advanced, highly tailored and accurate tool for correcting eddy-current-distortions
- A large array of minor improvements and additions to tools (e.g. massive speedup to invwarp, fill holes in fslmaths, spline interpolation in flirt, DVARS option in fsl_motion_outliers, ...)


## Release 4.1, June 2008

- FNIRT - FMRIB's Nonlinear Image Registration Tool. The first release of an accurate and yet fast nonlinear registration tool. This replaces IRTK, and is based on a similar warp field representation, but a) includes relative bias field modelling for improved accuracy and b) is very fast because of explicit computation of the cost function Hessian. FEAT, MELODIC, TBSS and FSL-VBM have all been updated to use FNIRT.
- FEAT v5.98. Various improvements, including FNIRT nonlinear registration, outlier modelling in higher-level analysis and higher-level voxelwise covariates (e.g., for structural confound modelling). A new script feat_gm_prepare makes it easy to process structural data for use as a higher-level voxelwise confound covariate.
- randomise v2.1. Various improvements, including improved handling of confounds, TFCE optimisation for TBSS and parallelisation over multiple computers.
- FAST v4.1 is now the default tissue-type segmentation program, and FAST v3 has been removed from FSL. Hence all programs including SIENA/SIENAX now use FAST v4.
- FIRST subcortical segmentation is now considerably faster, has improved boundary correction and now includes a cerebellum model.
- The outputting of ANALYZE files is no longer supported as the format is now strongly discouraged.


## Release 4.0, August 2007

- MELODIC v3.0 - now contains multi-subject ICA, including Tensor-ICA spaceXtimeXsubjects decomposition.
- FIRST - FMRIB's Integrated Registration and Segmentation Tool. The first release of a new tool which uses mesh models trained with a large amount of rich hand-segmented training data to segment subcortical brain structures.
- FDT - FMRIB's Diffusion Toolbox now has Bayesian crossing-fibre modelling with automatic estimation of the number of crossing fibres.
- POSSUM - Physics-Oriented Simulated Scanner for Understanding MRI. An FMRI data simulator that produces realistic simulated images and FMRI time series given a gradient echo pulse sequence, a segmented object with known tissue parameters, and a motion sequence.
- Standard Space Atlases - several complementary brain atlases, integrated into FSLView and Featquery, allowing viewing of structural and cytoarchitectonic standard space labels and probability maps for cortical and subcortical structures and white matter tracts.
- FSLView - in addition to the new atlases, new functionality includes 3D surface rendering and crossing-fibre diffusion direction viewing.
- FSL-VBM - an implementation of VBM-style analysis of grey matter density using FSL tools.
- TBSS v1.1 - improved registration efficiency in conjunction with the release of a new standard space FA template, the FMRIB58_FA. Also it is now possible to back-transform skeleton-space voxels into subjects' native DTI space, for example to allow the automated seeding of tractography in FDT.
- FABBER - Fast ASL & BOLD Bayesian Estimation Routine. Efficient nonlinear modelling and estimation of BOLD and CBF from dual-echo ASL data, nnusing Variational Bayes.
- The mechanism for running FSL under windows has changed. Instead of using Cygwin we now use VMware Player (freely available) with a Linux virtual machine inside it.
- FAST4 - a beta release of a new version of the FAST generic segmentation tool.
- Randomise v2.0 permutation testing - new features including: automatically correctly dealing with general contrasts and confounds, f-tests, TFCE (threshold-free cluster enhancement, a new method for cluster-based statistics).
- Several of the more compute-intensive tools can now take advantage of cluster computing, via SunGridEngine (SGE). For example, FEAT, MELODIC, TBSS, BEDPOST (FDT), POSSUM and FSL-VBM can automatically parallelise their processing to reduce total analysis time.
- New standard space templates. We have moved to a better version of the MNI152 standard-space image. This is based on the same dataset that was used to create the avg152T1 but using nonlinear registration instead of linear (many thanks to Andrew Janke and MNI for this). MNI152_T1_2mm is the new default standard image, also available at 1mm and 0.5mm. MNI152lin_T1_2mm is what we are calling the old standard image, based on linear registration.
- FEAT - v5.90, various miscellaneous updates including partial integration of the new atlases into Featquery.
- BET - old C code now removed and replaced with equivalent "bet2" C++ program; new wrapper script "bet" which adds various functionality such as improved eye and neck removal.
- FSLUTILS : all C++ utils ending with "++" now have had the "++" stripped off, as all the old C code with a similar name has been removed. Also, all programs starting with "avw" are now renamed to start with "fsl", e.g. "fslmaths". A script "avw2fsl" has been provided to help convert users' shell and TCL scripts to use the new command names.
- NIFTI i/o updated to use the sourceforge library.
- Warp field inversion is now possible with "invwarp".
- Removed FSL dependency on Tix, Gnuplot and ImageMagick; any generic recent TCL/TK can now be used for the scripts and GUIs.

## Release 3.3, April 2006

- TBSS - first release of Tract-Based Spatial Statistics, for voxel-based multi-subject statistics on diffusion (e.g. FA) data. TBSS uses the IRTK nonlinear registration software, binaries of which are bundled with this release of FSL.
- FEAT v5.6 new features:
  - Perfusion FMRI modelling/subtraction
  - Contrast estimability (meanginful rank deficiency and efficiency estimation)
  - FUGUE EPI unwarping now an option in pre-stats, to be applied simultaneously with motion correction; slice timing correction moved to be run after both
  - Motion parameters can now be automatically added as confound EVs into the model
  - New wizard for a few simple higher-level design setups
  - Empty EVs are now allowed at first level (for consistent naming across multiple subjects)
- FSLView v2.4 new features:
  - Timeseries plotting of model vs. data for FEAT output
  - FEAT cluster table interaction
  - Various other smaller improvements, plus full compilable source code now distributed
- Featquery changes:
  - % signal change calculations are now fully automated for both complex contrasts and higher-level FEAT output
  - Coordinate of max voxel also now reported in standard space mm
  - Raw text file output added
- Glm: A new GUI (a stand-alone version of the FEAT design matrix sub-GUI) for creating design matrix and contrast files, for example for use in randomise.
- SIENA v2.4 - the siena and sienax scripts can now pass on all relevant options to BET and FAST. Also the text output from sienax now includes unnormalised volume reporting as well as the head-size-normalised volumes. (Note also that small changes in FLIRT and FAST will mean that SIENA/SIENAX results may be very slightly different from the previous version.)
- FDR command-line program (fdr) is updated with a new definition of valid-voxel mask generation.

## Release 3.2β, September 2004

- FDT (FMRIB's Diffusion Toolbox) - first release of this complete toolkit for analysis of diffusion data, including probabilistic tractography.
- FLOBS (FMRIB's Linear Optimal Basis Functions) - a new method for finding an optimal set of HRF convolution basis functions, plus a Bayesian estimation method for improving signal/noise separation using these basis functions.
- SMM (Spatial Mixture Modelling) - a new tool for alternative hypothesis testing on statistic images using histogram mixture modelling with spatial regularisation of the voxel classification into activation and non-activation.
- Randomise - a randomisation-based inference tool for nonparametric statistical thresholding.
- BET v2 - can now estimate mesh representations of the inner and outer skull surfaces, and outer scalp surface, for example for use in EEG/MEG source modelling.
- NIFTI - FSL now uses the NIFTI-1 data format by default (though can still read and write old Analyze files). Single-file NIFTI-1 images can be read and written already compressed, saving much disk space.
- FEAT v5.4 new features:
  - Use of FLOBS (optimised HRF basis sets).
  - Fixed-effects higher-level analysis (e.g. for within-subject cross-session analysis).
  - Option to prevent cleanup of standard-space first-level stats.
  - User preferences file (and network-wide preferences file) which can set FEAT preferences for loading on startup.
  - Inclusion of MELODIC data exploration at the end of Pre-stats.
  - Time-series (data vs model) plotting inside Featquery.
- SIENA - can now do voxelwise cross-subject statistical atrophy analysis. Also, there is a small change to the partial volume estimation in - FAST, causing slight differences in volume estimates; any multi-subject analysis using FAST/SIENAX estimated volumes must only use the previous version or only use this new version.
- fslerrorreport is a new script which should always be run before emailing the FSL email list with a problem concerning running programs - the output from this will give us useful information about your system.

## Release 3.1, July 2003

- FLAME (Bayesian group stats modelling and estimation in FEAT5): updated to give increased estimation accuracy at the MCMC stage, and with added option for "FLAME stage 1 only" (nearly as fast as OLS and nearly as accurate as full FLAME).
- Beta-release of FSL for WindowsXP using Cygwin.
- Beta-release of FSLView interactive 3D/4D viewer (currently compiled for Linux/MacosX/Windows i.e. not yet IRIX/SunOS).
- Various minor additions (such as progress viewer for FEAT) and bug-fixes.


## Release 3.0, October 2002

- FEAT v5:
  - Bayesian multi-subject FMRI analysis: General GLM for higher-level analysis (e.g. analysis across sessions or across subjects; paired t-tests, three-level groupings etc): FLAME (FMRIB's Local Analysis of Mixed Effects). FLAME uses very sophisticated methods for modelling and estimating the random-effects component of the measured inter-session mixed-effects variance, using MCMC to get an accurate estimation of the true random-effects variance and degrees of freedom at each voxel.
  - Slice timing correction
  - Basis function HRF convolutions
  - Pre-threshold masking
  - Contrast masking after thresholding
  - Optionally registration can use two structural images as well as standard space image.
  - Peristimulus timing plots in the web report.
  - Featquery - a program which allows you to interrogate FEAT results by defining a mask or co-ordinates (in standard-space, highres-space or lowres-space) and get mean stats values and time-series.
  - Although IRVA (semi-model-free analysis) is still bundled with FSL, it is no longer an option within FEAT. This is because this kind of analysis is effectively covered as a subset of the functionality of FIR basis function analysis in FEAT.
- MELODIC v2:
  - Probabilistic ICA (PICA) for FMRI. By adding a Gaussian noise model to the ICA model, and automatically estimating the dimensionality (the number of non-Gaussian components), it is now possible to create meaningful Z-statistic maps for each spatial component, allowing inference ("thresholding").
  - Inference on the Z-statistic images is carried out with a Gaussian/Gamma mixture model (fitted using EM), fitting central noise and activation classes; thus alternative hypothesis testing is used.
- Interoperation with FreeSurfer - it is now very easy to display FEAT results onto FreeSurfer-generated inflated or flattened surfaces, and also easy to carry out group FEAT analysis in standard-spherical-surface space instead of 3D standard space.
- Interoperation with AFNI - AFNI can now easily read FSL (particularly FEAT) output for easy visualisation.
- SIENA v2: increased accuracy by forcing all steps to be symmetric; added masking functionality including standard-space-based Z limits.
- FLIRT/MCFLIRT; now run on single-slice (or few-slice) data; the flirt -applyxfm option now allows the processing of 4D data; new GUIs - ApplyXFM, ConcatXFM.
- BET: new command-line options for surface initialisation; "bet" now runs on binarised and on 2D images; the "betpremask" script uses FLIRT registration to do standard-space masking instead of running "bet"; bet now works on 4D images in two possible ways (the "betfunc" script is suited to motion-corrected FMRI data, applying over-inclusive masking, whilst just running "bet" on 4D data estimates the brain mask from the first image and applies this to the rest);


## Releases 2.x, internal development versions


## Release 1.3 for Mac OS X, November 2001


## Release 1.3, June 2001

- New: MELODIC - ICA-based model-free analysis of FMRI (and other 4D) data.
- New: FAST - Brain segmentation (into different tissue types) and bias field correction.
- New: FUGUE - Unwarps geometric distortion in EPI images using B0 field maps.
- New: SIENA - Structural brain change analysis, for estimating brain atrophy.
- FEAT 4.03: new features include F-tests and improved organisation of GLM-setup GUI.
- FLIRT: new features include image masking/weighting, different output interpolations, and allowing 2D registration.


## Release 1.2a, May 2001

> not an FSL release but the version bundled with MEDx 3.40)

- FEAT 4.02: new features include F-tests and improved organisation of GLM-setup GUI.

## Release 1.2, December 2000

- All GUIs can now run standalone - i.e., nothing in FSL needs MEDx in order to run.
- FSL now includes MCFLIRT motion correction, which is more accurate, and faster, than the other tested MC methods. FEAT uses MCFLIRT only.
- FILM (the GLM method inside FEAT) now uses general least-squares, prewhitening the data before the final fitting. This gives more efficient activation estimation than the other methods tested, particularly for dense single-event experiments. It includes autocorrelation correction, using smoothing of autocorrelation coefficients and nonlinear spatial filtering to achieve robust estimation.
- A new GUI `fsl` which calls the other main FSL tools.
- The "model-free ANOVA" method has been renamed "IRVA" (Inter-Repetition Variance Analysis) to avoid later confusion with ICA truly model-free analysis (to be released shortly).

## Release 1.1, August 2000

- FEAT (FMRI analysis) will now use FLIRT (registration) to automatically register your FMRI data to a high-resolution structural and/or standard image for you, without having to use the FLIRT GUI separately.
- FEAT can now run group statistics (fixed-effects and random-effects) across groups of FEAT directories, either in a single long FEAT run (consisting of multiple first-level analyses followed by group-stats) or on previously created FEAT directories.
- You can now explicitly demand that certain EVs in FEAT be orthogonalised with respect to other EVs in the design matrix.
- FSL now works with both the latest MEDx release (3.30) and the previous version (3.20).

## Release 1.0, June 2000

First ever release, containing:

- BET
- SUSAN
- FLIRT
- FEAT
- IRVA (then referred to as "Model-free ANOVA")
