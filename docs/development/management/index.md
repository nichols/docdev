# FSL development and release management procedures


This section hosts information and documentation regarding software development and management procedures implemented for the FMRIB Software Library (FSL).


## Quick summary


FSL is organised into a number of repositories hosted at https://git.fmrib.ox.ac.uk.

 - [`fsl/conda/manifest`](https://git.fmrib.ox.ac.uk/fsl/conda/manifest/): Metadata for managing public and internal FSL releases
 - [`fsl/conda/manifest-rules`](https://git.fmrib.ox.ac.uk/fsl/conda/manifest-rules/): CI configuration used by the `fsl/conda/manifest` repository
 - [`fsl/conda/installer`](https://git.fmrib.ox.ac.uk/fsl/conda/installer/): Home of the `fslinstaller.py` script, for installing FSL
 - [`fsl/conda/fsl-ci-rules`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules/) CI rules for automated building and publishing of FSL conda packages
 - [`fsl/<project>`](https://git.fmrib.ox.ac.uk/fsl/): Source code repository for FSL project `<project>` (e.g. [`fsl/avwutils`](https://git.fmrib.ox.ac.uk/fsl/avwutils/))
 - [`fsl/conda/fsl-<project>`](https://git.fmrib.ox.ac.uk/fsl/conda/): Conda recipe repository for FSL project `<project>` (e.g. [`fsl/conda/fsl-avwutils`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-avwutils/))


More detailed information on a range of topics can be found in the following sub-pages:

 - [FSL releases](development/management/fsl_releases.md): Details on workflows and processes for FSL releases.
 - [FSL project releases](development/management/project_releases.md): Details on development and release workflows and processes for individual FSL projects.
 - [FSL conda packages](development/management/conda_packages.md):  Details on cretaing FSL conda package recipes, and on buildingFSL conda packages.
 - [CI configuration](development/management/ci_configuration.md): Details on the AWS/CI configuration.


## FSL releases


Public and internal FSL release are defined by a set of conda [`environment.yml`
files](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#create-env-file-manually), one for each supported platform.  Each `environment.yml` file contains a list of the packages which are included in the FSL release.


FSL releases are described by a `manifest.json` file, which is an index of all available `environment.yml` files, and which contains all of the information needed to install a particular FSL release.


The [`fsl/conda/manifest`](https://git.fmrib.ox.ac.uk/fsl/conda/manifest/) repository is where new FSL releases, both public and internal, are created. The `environment.yml` and `manifest.json` files for a new release are automatically generated from information stored in the `fsl-release.yml` file in that repository.


The `manifest.json` and `environment.yml` files for public and internal release are currently published to https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/.


## Full FSL installations

FSL releases are installed via a script called `fslinstaller.py`, which is maintained at the [`fsl/conda/installer`](https://git.fmrib.ox.ac.uk/fsl/conda/installer/) repository.

The `fslinstaller.py` script performs the following tasks:

1. Downloads the FSL release `manifest.json` file, and the appropriate `environment.yml` file for the platform.
2. Downloads a `miniconda` installer.
3. Installs a base `miniconda` environment into `$FSLDIR` (where `$FSLDIR` defaults to `$HOME/fsl`, or is explicitly specified by the user).
4. Installs all FSL packages into the `$FSLDIR` base conda environment.
5. Sets up the user shell profile so that FSL is configured in their shell environment by default.


## Partial FSL installations


Users may choose to install specific FSL packages by installing the desired FSL packages directly from the FSL conda channel into their own conda environment.


> **Note:** All FSL conda packages list the dependencies required for their execution, so when a particular package is installed, all of its run-time dependencies will also be installed. However, as we transition away from monolithic installations and towards conda-based installations, there are likely to be omissions and errors in the dependency hierarchy. These issues will be resolved on a case-by-case basis.


## FSL conda packages


All FSL projects and their dependencies are published as conda packages. Most FSL conda packages are published to an internally maintained channel, but some packages are published to the `conda-forge` channel at https://anaconda.org.


All FSL projects, and any dependencies which are not available on https://anaconda.org, are built according to a conda *recipe*, which describes how the project can be built as a conda package. All FSL conda recipes are hosted as git repositories on the FMRIB gitlab server, under the `fsl/conda/` group.


To clarify, each FSL project, and internally managed dependency, comprises **two** git repositories<sup>*</sup>:

* The **project** repository contains the project source code/resources, and is typically hosted at `https://git.fmrib.ox.ac.uk/fsl/<project>`
* The **recipe** repository contains a conda recipe for the project, and is hosted at `https://git.fmrib.ox.ac.uk/fsl/conda/fsl-<project>`.


FSL conda packages are built automatically using Gitlab CI - the CI infrastructure for automatically building conda packages is hosted in the [`fsl/conda/fsl-ci-rules`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules/) repository.


> <sup>*</sup>A small number of FSL projects have more than one recipe repository associated with them - for example, the [`fsl/fdt`](https://git.fmrib.ox.ac.uk/fsl/fdt) project has two recipes - the [`fsl/conda/fsl-fdt`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt) recipe provides CPU executables, and the [`fsl/conda/fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda) recipe provides GPU/CUDA executables.


## Public, development, and internal channels


FSL projects are published to one of three internally managed conda channels:

  - The **public** channel contains stable packages for most FSL projects. Every time a new tag is added to a project repository, a stable conda package is built and published to the public channel.

  - The **development** channel may contain unstable/pre-release packages for some FSL projects. These packages may be built from any branch or commit of the project repository. This channel is intended for use by developers who wish to publish experimental changes for testing, evaluation, and/or feedback.

  - The **internal** channel contains stable packages for FSL projects which are not part of official FSL releases, and are only part of internal releases.

These channels are currently hosted online at the following URLs:

 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal/
