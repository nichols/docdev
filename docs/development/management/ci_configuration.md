# Automated CI configuration


FSL conda packages are automatically built and published using GitLab CI pipelines implemented in the [`fsl/conda/fsl-ci-rules`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules) repository. FSL releases are built and published using CI rules implemented in the [`fsl/conda/manifest-rules`](https://git.fmrib.ox.ac.uk/fsl/conda/manifest-rules) repository. The jobs which comprise these pipelines are executed on the following infrastructure:

 * Intel macOS jobs are executed on gitlab-runners installed on old macbooks managed by Paul and Taylor.
 * M1 macOS jobs are executed on a gitlab-runner installed on a mac mini, managed by Duncan.
 * Linux and platform-agnostic jobs are executed on AWS EC2 infrastructure, using a GitLab Runner [configured for auto-scaling](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/).


When new Linux/platform-agnostic jobs are scheduled, the auto-scaling runner uses [docker+machine](https://gitlab.com/gitlab-org/ci-cd/docker-machine) to create one or more `m4.large` EC2 instances (provisioned with docker), and dispatches the jobs to those instances.  When there are no more jobs to execute, the EC2 instances are destroyed.


Docker images used for these jobs are hosted at https://hub.docker.com/orgs/fsldevelopment/repositories/, which is managed by Paul. These images are built from `Dockerfiles`, and using CI jobs, in the `fsl/conda/fsl-ci-rules` repository.


The auto-scaling runner (the *GitLab CI runner manager*) resides on a `t3.medium` EC2 instance running 24/7. This instance is currently available at IP 18.133.213.73. With an appropriate private key, it is possible to SSH to this instance like so:


```bash
ssh -i <ec2-key.pem> ec2-user@18.133.213.73
```


This runner manager instance is roughly configured as follows:

 * Elastic IP assigned to instance (so it persists across reboots)
 * Security group allowing inbound port 22 (SSH)
 * Initial setup resembles the following:
   * `yum install docker httpd amazon-efs-utils` (`httpd` is no longer used)
   * `chkconfig systemctl docker on` (start at boot)
   * Install gitlab-runner: https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner
   * Download docker-machine binary from https://gitlab.com/gitlab-org/ci-cd/docker-machine, copy it to `/usr/local/bin/`, `chmod a+rwx`
   * Register the gitlab runners, one with `docker+machine` executor, tags `fsl-ci,docker`, name `FSL CI GitLab Runner Manager`
   * Edit `/etc/gitlab-runner/config.toml` as described below
   * Reboot


A second security group, applied to the auto-scale instances, must be configured to allow inbound TCP ports 22 and 2376. This group is specified in the gitlab runner `confiug.toml` file.


The `/etc/gitlab-runner/config.toml` configuration looks something like this:

```toml
concurrent = 20
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "FSL CI GitLab Runner Manager"
  limit = 20
  url = "https://git.fmrib.ox.ac.uk/"
  token = "<runner-token>"
  executor = "docker+machine"
  environment = ["DOCKER_AUTH_CONFIG={\"auths\":{\"https://index.docker.io/v1/\":{\"auth\":\"<docker-auth>\"}}}"]
  [runners.cache]
    Type = "s3"
    Shared = true
    [runners.cache.s3]
      ServerAddress = "s3.amazonaws.com"
      AccessKey = "<aws-access-key>"
      SecretKey = "<aws-secret-key>"
      BucketName = "fsl-ci-gitlab-runner-cache"
      BucketLocation = "eu-west-2"
  [runners.docker]
    tls_verify = false
    image = "alpine"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
  [runners.machine]
    IdleCount = 0
    IdleTime = 1800
    MaxBuilds = 20
    MachineDriver = "amazonec2"
    MachineName = "gitlab-docker-machine-%s"
    MachineOptions = ["amazonec2-access-key=<aws-access-key>",
                      "amazonec2-secret-key=<aws-secret-key>",
                      "amazonec2-region=eu-west-2",
                      "amazonec2-ami=ami-06cc1e8bd6513949d",
                      "amazonec2-vpc-id=<vpc-id>",
                      "amazonec2-subnet-id=<subnet-id>",
                      "amazonec2-tags=fsldev,true,runner-manager-name,gitlab-aws-autoscaler,gitlab,true,gitlab-runner-autoscale,true",
                      "amazonec2-security-group=<security-group-id>",
                      "amazonec2-instance-type=m4.large",
                      "amazonec2-root-size=32"]
```


where:
 * `<runner-token>` is replaced with the GitLab runner tokens (generated when registering a new runner via the GitLab web UI)
 * `<docker-auth>` is Paul's personal Docker Hub login details, used for uploading docker images to Docker Hub.
 * `<aws-access-key>` is the AWS access key
 * `<aws-secret-key>` is the AWS secret key
 * `<vpc-id>` is the EC2 vpc ID
 * `<subnet-id>` is the EC2 subnet ID
 * `<security-group-id>` is the EC2 security group ID

The `amazonec2-ami` entry specifies that Ubuntu 18.04 instances are used - this was added in response to https://gitlab.com/gitlab-org/ci-cd/docker-machine/-/issues/69.
