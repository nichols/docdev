# FSL wiki

This is the markdown source for the FSL documentation. These markdown files are
rendered as html via our Gitlab Pages site, currently at:

https://open.win.ox.ac.uk/pages/fsl/docdev/

# Adding wiki content

You can edit any markdown file in `docs/`.

If you need to create a new wiki section

- create the new file in `docs/`
- if you want the file to appear in the sidebar for navigation then add the
  link to `docs/_sidebar.md`. You can see other links in that file as a guide.

# Viewing content locally

## 1. Without a server

If you want to view the docs locally you can simply open `docs/index.html` in
a web browser.

For example, on macOS you could run this command in a terminal:

`open docs/index.html # to open with your default browser`

## 2. With a server

You can also serve them with a locally running server. If you have python3.7.x
or greater installed, or fslpython (part of FSL) you can do:

`fslpython -m http.server --directory ./docs 8080 # or some other port if 8080 is taken`

or

`python -m http.server --directory ./docs 8080 # or some other port if 8080 is taken`

## 3. With a hot-reload enabled server

Lastly, you can use the docsify hot-reload development server to view updates to your documentation live as you work on files and save them.

You must install nodejs/npm and then install the docsify package globally.

for example (assuming you have node and npm on your `$PATH`):

`npm i docsify-cli -g`

Then, to serve from the docdev directory:

`docsify serve docs`

# Markdown/docsify cheat sheet


The official Markdown syntax reference can be found at https://daringfireball.net/projects/markdown/syntax

There are many markdown renderers, all of which may support slightly different features - docsify uses the [marked](https://github.com/markedjs/marked) parser.

Some docsify-specific extensions are detailed at https://docsify.js.org/#/helpers



## Custom block quotes

Docsify provides a few custom markdown extensions for nicer block quotes. The contents for these block quotes must all be on a single line (or with newline
characters escaped with a back slash):

* Regular block quotes (standard Markdown)
  ```
  > **Some important information**
  > This will be rendered in a separate box.
  ```

* Warnings (note that newlines are escaped)
  ```
  !> WARNING: **Do not press the red button under any circumstances**\
  The last person who pushed it hasn't been seen since.
  ```

* Informative
  ```
  ?> Note that if you do inadvertently push the red button, \
  your warranty will be voided immediately.
  ```

## Linking to other pages and content

When link to other markdown files and attachments, you must always specify the path as being relative to the root of the `docs` directory. For example, to link to `registration/flirt.md`:


```
Check out the [FLIRT documentation](registration/flirt.md).
```

### Linking to images

In contrast to the above, when displaying an image using markdown syntax, you must specify the image file as being relative to the _markdown_ file. For example, if you want to display `structural/t1.png` in `structural/bet.md`, you would use:

```
![T1 image](t1.png)
```

You can also use raw HTML, in which case you need to refer to the image file using a path relative to the root of `docs`. This option gives you more control over layout, e.g.:

```
<img src="structural/t1.png" align="right" width="30%"/>
```


### Linking to attachments

If you are trying to use markdown syntax to link to a data file, e.g.:

    [Download this file](resting_state/dual_regression.pdf)

Docsify may create the URL incorrectly. You can work around this by telling Docsify to use the link as-is, e.g.:

    [Download this file](resting_state/dual_regression.pdf ":ignore")


### Linking to sections within a page

Every Markdown heading will be turned into a linkable anchor - for example, this Markdown header:

```
## Some section
```

can be linked to with the following syntax:

```
Refer to [some section](#some-section) for more details.
```

The link (a.k.a _slug_) for a header is the same as the header, converted to lower-case, and with all non-alpha-numeric characters replaced with hyphens (e.g. the slug for `Some section` is `some-section`).

It is also possible to manually create and link to anchors anywhere in your file - for example, you can create an anchor for a paragraph like so:

```
<a id="some-paragraph" href="#some-paragraph">This paragraph</a> is a very
good paragraph
```

And link to it elsewhere like so:

```
Check out [this paragraph](#some-paragraph) as it is a very good paragraph.
```


## Inline latex

You can insert equations into your markdown files like so:

```
Einstein's most famous equation is:

$$
E=mc^2
$$

Unfortunately nobody knows what it means.
```


## Code highlighting

If you are displaying code snippets, you can apply language-specific syntax highlighting by beginning the snippet with the language name:

    ```bash
    ls -l
    ```

    ```python
    def some_function():
        print('hello')
    ```

Only a few languages are supported - get in touch if it isn't working for you.


## Latex

We have enabled [mathjax](https://docs.mathjax.org/en/latest/) for rendering LaTeX mathematical equations. For an inline equation, use:

```
The equation $e=mc^2$ is a very good equation.
```

Or for a block equation, use:

```
The equation:

$$
e=mc^2
$$

is a very good equation.
```

## NIfTI preview with NiiVue

You can display one or more NIfTI images with NiiVue like so:


```
<div class="niivue-canvas" thumbnail="thumbnail.png">
<vol url="T1.nii.gz"/>
<vol url="T1_brain.nii.gz" colormap="red" opacity="0.5"/>
</div>
```

Paths to all files must be relative to the `docs/` directory. The thumbnail image is used as a preview before the NIfTI images are loaded.
