#!/usr/bin/env bash

set -e

basedir=$(cd $(dirname $0) && pwd)/../
thisscript=${basedir}/.ci/deploy-docs.sh

if [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]; then
  destdir="${basedir}/public/"
else
  branch=$(echo "${CI_COMMIT_REF_NAME}" | sed -e 's/[^A-Za-z0-9._-]/-/g')
  destdir="${basedir}/public/${branch}/"

  
  echo "Sanitised branch name: ${CI_COMMIT_REF_NAME} -> ${branch}"

  # publish main docs too
  mkdir tmp
  cd tmp
  tarball=${CI_PROJECT_URL}/-/archive/main/${CI_PROJECT_NAME}-main.tar.gz
  wget ${tarball}
  tar xf *tar.gz
  cd *-main
  CI_COMMIT_REF_NAME=${CI_DEFAULT_BRANCH} sh ${thisscript}
  cd ../../
fi

echo "Publishing $(pwd)/docs/* to ${destdir}/"

mkdir -p ${destdir}
mv ./docs/* ${destdir}
